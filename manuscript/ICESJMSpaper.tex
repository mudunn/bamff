\documentclass[11pt,halfline,a4paper,numbib]{ouparticle}
\usepackage{graphicx}


\begin{document}

\title{One fish, two fish: broadband acoustic Doppler current profilers can count fish}

\author{%
\name{Muriel Dunn}
\address{Memorial University of Newfoundland, Department of Physics and Physical Oceanography, St John's, Newfoundland and Labrador\\
A1C 3T5, Canada}
\email{mbdunn@mun.ca}
\and
\name{Len Zedel}
\address{Memorial University of Newfoundland, Department of Physics and Physical Oceanography, St John's, Newfoundland and Labrador\\
A1C 3T5, Canada}
\email{zedel@mun.ca}}

\abstract{Acoustic Doppler current profilers (ADCPs) are the standard instrument used to monitor ocean currents. These instruments also detect signals scattered by fish and other organisms, but these signals are normally treated as noise and rejected by ADCP data processing techniques. Those rejected signals do however contain information on fish movement providing an opportunity to extend the application of ADCP technology. The added capability of the ADCP offers a monitoring tool for fish activity. We explore this capability through a 37-day deployment of a self-contained bottom-mounted frame equipped with a 600 kHz RD Instruments Workhorse ADCP alongside a 120 kHz BioSonics DTX Submersible Split-Beam echosounder system. It is shown that fish can be differentiated from entrained air using a high signal correlation threshold with the broadband ADCP. The high correlation threshold is also used to count fish in the ADCP with agreement with the split-beam echosounder fish tracking counts.}

\date{\today}

\keywords{Fish detection; split-beam echosounder; broadband Doppler sonar; pulse-pair correlation}

\maketitle


\section{Introduction}

The industry standard instrument for fish detection is split-beam echosounders. In ideal survey conditions, their echograms provide a clear image of fish as they travel through the beam of ensonified water. For turbulent waters, the conventional processing approach is to limit the depth range by excluding the surface as it is difficult to distinguish bubbles from fish in areas of entrained air \cite{Staines2015}. When fish form dense aggregations, it can be difficult to acoustically identify an individual fish from its neighbour because the signal is over-saturated. To resolve the over-saturation of the signal in fish schools, a standard approach is to omit fish schools from the analysis \cite{Viehman2017}. Specialized processing methods have been developed in response to these gaps in data, but they require an in-depth prior knowledge of the site dynamics and the targeted species behaviour \cite{Fraser2017}. \\

Signals received by acoustic Doppler current profilers (ADCPs) contain backscatter from a variety of objects in the water column, including fish, zooplankton, sediments, bubbles and the surface or bottom. As the instrument is used for water velocity measurements, typically all signals that differ significantly in echo intensity between the four beams are rejected to reduce fish bias in velocity data \cite{Freitag1993}. The significant difference in echo intensity suggests that one beam is detecting something different from the other beams (i.e. a fish). The basis of fish detection with ADCP is to utilize the rejected fish signals as a data source of fish presence. Previously, researchers have suggested using ADCP's for fisheries studies, but they have encountered processing and technology advancement issues with algorithm developments and calibration (\cite{Holliday1977}, \cite{Olsen1983} \cite{Demer2000}). Demer (2000) identifies a need to modify data collection parameters and data processing as well as  to select survey sites carefully with fish schools matched to the size of the processing bins. \\

The latest generation of Doppler sonars simprove the spatial resolution and speed accuracy by using the phase difference of the received pulse-pair. Incidentally, the correlation processing inherent to the broadband Doppler system allows discrimination of discrete targets in addition to volume backscatter thresholds \cite{Tollefsen2003}. A perfect single discrete target without noise would return a correlation value of 1 or 256 counts, as recorded by the RDI systems, in contrast to correlation coefficient values of 0.5 or 128 counts, a characteristic expected from a cloud of bubbles, a more extensive school of fish or an absence of discrete targets. The correlation threshold aims to isolate the high peaks in the autocorrelation signal that is indicative of a discrete target, such as a single fish, when it coincides with a high volume backscatter signal \cite{Tollefsen2003}.\\

Here, we present a new approach to monitor fish presence through the use of long-term bottom-mounted upward-looking ADCP observations. The new approach is intended to complement existing acoustic survey methods. The results show that broadband ADCPs can count fish in agreement with the industry standard and the methods used to detect fish can be used to differentiate fish from entrained air.



\section{Experimental Methods}

\subsection{Experimental Site}
We deployed a bottom-mounted self-contained frame deployed in Grand Passage in the Bay of Fundy (Figure \ref{fig:GPdeploymentlocation}) at 25 m depth to compare fish counts from a split-beam sonar and an ADCP. The bottom-mounted frame was equipped with an RD Instruments 600 kHz acoustic Doppler current profiler and a BioSonics DTX Submersible 120 kHz split-beam echosounder, as shown in Figure \ref{fig:GPframe}. The deployment took place from September 21st, 2018 14:30 to October 29th, 2018 13:54. The acoustic instruments were installed as close as possible to facilitate direct comparison between both datasets. \\


\begin{figure}[!htbp]
\centering
\includegraphics[width=4in]{figures/GPdeploymentlocation.jpg}
\caption{Location of the frame in Nova Scotia, Canada.}% Fix perspective.}
\label{fig:GPdeploymentlocation}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=5in]{figures/GPframe.jpg}
\caption{Left: A picture of the frame before the deployment with the ADCP in the middle. The split-beam echosounder system includes the echosounder on the shelf, the black horizontal cylinder on the left is the DT-X Submersible and the battery pack is the blue horizontal cylinder on the right. Right: Sketch of the frame. The frame is 3 by 4 feet.}
\label{fig:GPframe}
\end{figure}


\subsection{Data collection settings}
The ADCP was set to collect at one ping per second with 1 m bins with no averaging of the profile data. It is crucial for the fish detection algorithm that the pings are not averaged. If multiple pings are averaged together the discrete targets, such as fish, will be either averaged out or combined. The time per ensemble command, TE, dictates how much time will be averaged in each ensemble and time between pings command, TP, sets the ping rate, see Table \ref{tab:ADCPconfig} for RDI configuration software commands. Both the TE and TP must be the same value to have one ping per ensemble, hence no averaging in the time domain. The split-beam sonar was configured to transmit four pings per seconds with a 0.1 ms pulse duration. \\
 
\subsection{Duty Cycle}
A common problem when operating two nearby acoustic instruments is crosstalk. We chose widely separated frequencies to mitigate this problem (120 kHz for split-beam and 600 kHz for the ADCP). Nevertheless, contamination in the split-beam sonar data was observed during lab tests. Therefore, in addition to the separate frequencies, the duty cycles were staggered to ensure the collection of individual uncontaminated data from both instruments as well as simultaneous measurements. To achieve an offset in the timing of the instruments, the split-beam data acquisition cycle started 10 minutes after the ADCP data acquisition cycle.\\

The ADCP data acquisition duty cycle was 20 minutes on and 20 minutes off. The Workhorse ADCP time commands from the RDI configuration software were used to set the duty cycle, the chosen settings are summarized in Table \ref{tab:ADCPconfig}. The time per burst command, TB, determines the length of one whole duty cycle. The ensemble per burst command, TC, dictates the number of ensembles within the burst time. The split-beam data acquisition duty cycle was 20 minutes on and 40 minutes off. \\


\subsection{ADCP settings, calibration and analysis}
\subsubsection{Data collection settings}
We configured the ADCP to collect at one ping per second with 1 m bins with no averaging of the profile data. It is crucial for the fish detection algorithm that the pings are not averaged. If multiple pings are averaged together the discrete targets, such as fish, will be either averaged out or combined. Both the time per ensemble, TE, and the time between pings, TP, must be the same value to have one ping per ensemble, hence no averaging in the time domain, see Table \ref{tab:ADCPconfig}  \\
 

\begin{table}[!htbp]
\centering
\caption{ADCP RDI Workhorse Time and Water Profiling Commands.}
\label{tab:ADCPconfig}
\centering
\begin{tabular}{|c|l|r|}
\hline
Command &  Description & Setting \\ 
\hline
\hline
 TB & Time per burst & 00:40:00.00 \\
\hline
  TC & Ensemble per burst & 1200 \\
\hline
 TE & Time per ensemble & 00:00:01.00\\
\hline
 TP & Time between pings & 00:00:01.00\\
\hline
\hline
 WE & Error Velocity (mm/s) & 0\\
\hline
  WC & Correlation Threshold (counts) & 0 \\
 \hline
 WN & Depth Cells & 30\\
\hline
 WS & Depth Cell Size (cm) & 50\\
 \hline
 WV & Ambiguity Velocity (cm/s) & 175\\
 \hline
\hline
\end{tabular}
\end{table}

\subsection{Calibration}
There are three components of the instrumentation that needed to be calibrated: the ADCP compass, the ADCP backscatter and split-beam backscatter. The ADCP compass was calibrated the day before both deployments by rotating the mounted frame in a constant magnetic field environment \cite{RDI2001}. The whole frame must be included in the compass calibration to account for any compass offset that may be present due to the magnetic materials of the instrument frame, such as the batteries and any iron material. \\

The ADCP backscatter calibration coefficients were taken from a January 2018 laboratory calibration. The backscatter calibration was performed following the details described in \cite{Dunn2019}. The split-beam was calibrated using the standard on-axis calibration proceedure \cite{Foote1987}, in the days immediately following the retrieval of the frame. We perform the calibration with a 33.2 mm diameter tungsten carbide sphere using a 0.4 ms pulse length. We applied the calculated offset of 1.1 dB to the data.

\subsubsection{Analysis}
\subsection{Broadband Acoustic Monitoring For Fish (BAMFF)}
We created the Broadband Acoustic Monitoring For Fish (BAMFF) package and used it to process the ADCP data. It is a MATLAB toolbox that processes raw ADCP data into depth and time-averaged fish and water velocities. The toolbox converts the RDI files into user-modifiable MATLAB structures, then it calibrates and corrects for spherical spreading and absorption loss using the procedure outlined by Deines (1999) \cite{Deines1999, Mullison2017}. Using the combination of correlation and volume backscatter thresholds, we are able to determine whether signals are from fish or water targets in each beam individually. The targets for all the beams are sorted by time and depth to calculate the average fish velocity and count. We also bin the non-fish target in the same way to extract the water velocity profiles.

\subsubsection{Thresholds}
As with any echosounder system, the presence of fish in the Doppler sonar data can be initially assessed by volume backscatter levels exceeding a specified threshold. Properties unique to the broadband Doppler system allow discrimination of discrete targets in addition to volume backscatter. The correlation processing inherent to broadband sonar provides an alternative way to detect discrete targets. A correlation coefficient values of 0.5 or 128 counts is a characteristic expected from a cloud of bubbles, more extensive school of fish or an absence of discrete targets. A perfect single discrete target without noise would return a correlation value of 1 or 256 counts, as recorded by the RDI systems \cite{Tollefsen2003}. The correlation threshold aims to isolate the high peaks in the autocorrelation signal that is indicative of a discrete target, such as a single fish target (if it coincides with high volume backscatter signal). \\ 

We determined the threshold values by starting with low values and slowly increasing them while assessing their accuracy on a characteristic single fish school and a bubble plume. The thresholds were increased until the bubbles plume was not considered to include discrete targets, but the fish in the aggregation were still detected. The balance between the intensity and correlation thresholds was adjusted further so that in general bubble plumes and fish schools were correctly identified. Figure \ref{fig:thresholdhist}a shows the correlation has a sharp upper limit where no targets are detected with a threshold above 159 counts (or 0.62). The intensity counts sharply decrease and taper off at volume backscatter values above -65 dB (Figure \ref{fig:thresholdhist}b).\\

\begin{figure}[!htbp]
\centering
\includegraphics[width=5in]{figures/thresholdhist.jpg}
\caption{Histograms of the a) correlation and b) volume backscatter values in the ADCP beam 1.}
\label{fig:thresholdhist}
\end{figure}



\begin{figure}[!htbp]
\centering
\includegraphics[width=5in]{figures/FishschoolADCPsplit.jpg}
\caption{a) A volume backscatter sonogram for a fish school from the ADCP with the identified fish targets (black dots, $\cdot$) from all four beams b) identifies the remaining data after the backscatter threshold (-45 dB)  is applied, c) the corresponding split-beam signal and d) identifies the remaining data after the backscatter and correlation thresholds (135 counts).}
\label{fig:FishschoolADCPsplit}
\end{figure}


The volume backscatter and correlation thresholds are applied to each beam individually for fish detection. An example of the effect of the thresholding is shown in Figure \ref{fig:FishschoolADCPsplit}. The top left panel, Figure \ref{fig:FishschoolADCPsplit}a is the sonogram image based on the calibrated volume backscatter, $S_{v}$ from beam 1. Figure \ref{fig:FishschoolADCPsplit}b is the calibrated volume backscatter, $S_{v}$, with a threshold of -45 dB applied to isolate all possible fish target candidates. The volume backscatter threshold results in bubbles, surface and fish targets remaining. The correlation threshold of 153 counts (or 0.60) is applied to finalize the fish detection by removing the low quality, incoherent targets. \\

In addition to both thresholds, the near-field and the surface must be removed from the analysis. Near the transducer face, the signal is complex because it has areas of constructive and destructive interference \cite{Medwin1997}. Past a critical range, determined as $ R = \pi \frac{a^{2}}{\lambda} $, the pressure is safely far-field and the phase is uniform across the beam. With a transducer radius, $a$, of 0.04 m and a wavelength, $\lambda$, of  0.0025 m, the near-field range is calculated to be 2.0 m. We double the near-field range to ensure we are only using the far-field signal; therefore we exclude first 4 m from the analysis. As for the surface, the exclusion line range was selected with a running mean of the maximum volume backscatter. This exclusion line removes the strong surface backscatter and anything beyond the surface from the analysis. \\

The final targets remaining after the volume backscatter threshold, the correlation threshold, and the surface and near-field exclusion lines are accepted as fish target; these are shown in Figure \ref{fig:FishschoolADCPsplit}d and as well, these remaining detected targets are indicated by black dots in Figure \ref{fig:FishschoolADCPsplit}a. Note in particular the clear agreement in backscatter structure when comparing the Doppler sonar data (Fig. \ref{fig:FishschoolADCPsplit}a) with the split-beam data (Fig. \ref{fig:FishschoolADCPsplit}c). Conversely; water targets are identified as signals below either the intensity or correlation thresholds; and these water target data are used to calculate the water velocity.


\subsection{Fish tracking with Sonar5-Pro}
Sonar5-Pro \cite{Balk2018}, is a sonar post-processing program developed to improve single echo detection methods for fish target detection in sonar data. Balk and Lindem (2002) created a split-beam sonar target tracking algorithm to improve ``traditional'' methods that would frequently miss echoes from fish and create tracks solely from noise. This program, like many others of its kind, requires the user to establish the zones that will not be evaluated for fish detection, such as the near-field and the surface or bottom. The Biosonics 120kHz split-beam echosounder near-field is 0.73 m; we set the near-field exclusion zone to 1.46 m. In our case, the ocean surface also needs to be excluded by a surface exclusion line. This problem is analogous to difficulties near the bottom for conventional hull-mounted (downward looking) system operations. In the present application it is possible to use the bottom detection algorithm to identify the location of the surface. Generally, the bottom detection algorithm requires manual adjustment when fish are near the surface and when the fish and bubbles are mixed. For this purpose, the software provides facility to adjust the exclusion lines. The manual adjustment process is a visual judgment of the sonogram image to delineate the extent of the spreading bubble plume. Manual adjustments can introduce bias by either keeping too much of the bubble plume which results in false targets from bubbles or by eliminating too much which leads to an inaccurate fish count estimate. In addition, it is time-consuming for the user and results in blind spots for the system.\\

We used the Cross-filter Detector algorithm, developed by Balk and Lindem (2002), to identify the SED and to combine them into fish tracks. There are four components to the Cross-filter Detector for tracking: the detector, the evaluator, the SED and the tracking. The parameters for each component were tuned by carefully following the guidelines described in the Sonar5-Pro manual \cite{Balk2018}. We focused on the same fish school to choose the settings for fish detection and tracking as for tuning the ADCP parameters.\\


\section{Results}

A total of 37 hours of simultaneous data were collected in 10 minute intervals during the first deployment between September 21st and November 1st, 2018.  Results show that the detected fish targets of the ADCP and split-beam datasets generally coincide in time and space. The ADCP has a slightly lower count in the dense fish schools but seems to detect more isolated targets.

\subsection{Fish Detection}

\begin{figure}[!htbp]
\centering
\includegraphics[width=\textwidth]{figures/ADCPsplitcount.jpg}
\caption{Fish counts of the simultaneous data collected averaged over 2 h time bins and 2 m depth bins. a) ADCP fish targets, b) split-beam fish track count.}
\label{fig:ADCPsplitcount}
\end{figure}

The accepted fish detections for both instruments were averaged over 2 h time bins and 2 m depth bins. The temporal bin size was chosen to have enough data points to average (20 minutes of data in 2 hours) without averaging over tidal cycles. We chose 2 m depth bins to have two bins to average in the ADCP data while maintaining the depth structure of the fish aggregations. Smaller depth bins fragmented the results. The resulting data summarized in Figure \ref{fig:ADCPsplitcount} shows an agreement in the intervals of higher fish detections. The split-beam generally has a couple of bins with a higher density of fish than the ADCP within each fish school. The more dense areas in the split-beam fish school detections are most noticeable with the fish schools on September 22nd and September 29th, 2018. However, fish counts for both instruments are similar in other aggregations and span the same depth range.\\
 
We corrected the fish detections from Figure \ref{fig:ADCPsplitcount} for the difference in beam patterns between the instruments and the increase of detection volume with depth. The ADCP and the split-beam echosounder have different transducer properties. The inherent differences between the transducers, frequency and size, are summarized in the equivalent beam angle, $\psi$, which indicates the solid angle of an idealized acoustic beam:

\begin{equation}
\label{eq:psi}
\psi = \int_{\theta=0}^{\pi} \int_{\phi=0}^{2\pi} b^4(\theta, \phi) \mathrm{d}\theta \mathrm{d}\phi.\\
\end{equation}
where for an ideal cylindrical transducer, the directivity is expressed as:

\begin{equation}
\label{eq:Directivity}
b = \frac{2 J_{1}(ka\sin(\theta))}{ka\sin(\theta)}\\
\end{equation}
where the transducer radius, $a$, wavenumber, $k$ and $J_{1}$ is a Bessel function of the first kind \cite{Medwin1997}.

The sample volume is a measure of the space that contributes echoes to the transducer for any signal \cite{Maclennan2013}. It accounts for the acoustic beam pattern, the pulse length and the increase in detection volume with depth. We normalize the fish counts per bin with the sample volume, $V$:

\begin{equation}
\label{eq:V}
V = \frac{c \tau \psi R^{2}}{2} \\
\end{equation}
where the range, $R$, is the center of each depth bin, the pulse length is $\tau$ and the sound speed velocity is $c$. For the ADCP, the sample volume is multiplied by four to account for the 4 transducers.

The detections from Figure \ref{fig:ADCPsplitcount} were normalize by the sample volume and depth-integrated (Figure \ref{fig:DepthIntTS}a). The peaks, representing the fish schools, occur at the same time in both the ADCP and split-beam. Notable differences in between the datasets (Figure \ref{fig:ADCPsplitcount}b) are seen in the fish schools on September 22nd and 23rd, which show a greater number of ADCP fish detections, and the fish school on September 29th and 30th, and October 1st, 2018, where more split-beam fish targets are detected. Both of these occurrences are relatively small schools with less than 100 fish detections.

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.75\textwidth]{figures/DepthIntTS.jpg}
\caption{Fish counts of the simultaneous data collected depth-integrated and averaged over 2 h time bins. a) The ADCP fish targets counts are plotted in blue and the split-beam fish tracks are plotted in orange. b) The difference between the split-beam and ADCP fish counts, positive values is when there is more fish counts in split-beam, negative values when there is more ADCP fish count.}
\label{fig:DepthIntTS}
\end{figure}

The data of the depth-integrated time series (Figure \ref{fig:DepthIntTS}) were used to calculate the linear regression between split-beam fish tracks and ADCP fish counts. Although the analysis was carried out using observed values, a scatter plot (ADCP vs. split-beam) is presented using a log-log plot in Figure \ref{fig:linreg} in order to highlight agreement at both high and low fish concentrations. When compared against the fish targets identified by the ADCP, a slope of 1.2 $\pm$ 0.1 was calculated for the linear regression. Overall, a cross-correlation coefficient of 0.90 was calculated between the fish tracks from the split-beam and the fish counts from the ADCP. 

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.65\textwidth]{figures/linearregression.jpg}
\caption{Linear regression analysis for comparison of overlapping ADCP fish targets dataset to split-beam fish tracks count, both datasets were averaged in 2 h time bins, normalize by the sample volume of each bin and depth-integrated for the whole water column.}
\label{fig:linreg}
\end{figure}

\subsection{Entrained air}
A particular challenge in strong tidal flows is the occurrence of near-surface bubble plumes \cite{Melvin2014} which are often hard to distinguish from fish (or plankton) targets. By using a bottom-mounted frame rather than a hull-mounted survey, the data in the present study extends right up to the surface. The zone just below the surface must be clear of entrained air to be analyzed with the split-beam echosounder, because the bubbles are notorious for false detections with fish detection and tracking methods. \\

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.65\textwidth]{figures/entrainedaircompare.jpg}
\caption{Fish detections in entrained air: a) shows the split-beam fish tracks (colorful lines) below the surface exclusion line (red curvy line) and b) shows ADCP fish detections intermingled with the bubbles. The surface is at 25 m: a) the dark red horizontal line, b) the orange horizontal line. }
\label{fig:entrainedaircompare}
\end{figure}

The surface exclusion line, red line in Figure \ref{fig:entrainedaircompare}a, determined the upper limit for the analysis area and had to be adjusted manually for this area because bubbles and fish are mixed together in the sonogram. Arguably the whole zone above 15 m depth should have been removed from the analysis, but inspection of the echosounder image suggested clear fish targets within the bubble plumes. For the data shown in Figure \ref{fig:entrainedaircompare}b, the Doppler sonar volume backscatter shows the fish targets identified below and within the bubbles. The requirement of coincident high signal correlations and high intensity was used to distinguish discrete targets from bubble clouds. In this case, the ability of the ADCP to differentiate between bubbles and through the use of the correlation values fish target is a promising result that demonstrated that ADCP fish detections could complement traditional survey methods in areas of high turbulence.

\section{Discussion}

We presented the acoustic data and results of a comparison between a broadband ADCP and a split-beam echosounder from a 37-day deployment in a high-energy tidal channel. This research assessed the ability of a broadband ADCP to detect fish. Depth-averaged fish targets from the collocated instruments show a strong agreement for the 370 minutes of simultaneous data. This result demonstrates that broadband ADCP results agrees with split-beam echosounder results for fish detection. The agreement of the datasets suggests ADCPs can be a promising alternative fish detection method. \\

Holliday (1977), Olsen et al. (1983), and Demer (2000) saw the potential for velocity measurements of fish with Doppler shift instruments, and identified technology advancements and data processing methods as outstanding challenges. Broadband technology produces a more precise measurement by using autocorrelation to calculate velocity. Conveniently, the autocorrelation provides a measure of the quality of the backscattered signal \cite{Tollefsen2003}. This development is a critical turning point in developing the capabilities of fish detection with ADCP because it can differentiate fish targets from regions of continuous volume backscatter, such as water or bubble plumes. Additionally, Zedel and Cyr-Racine (2009) remove the dependence on assuming the bins have homogeneous flow and have developed a data processing method that uses an optimization algorithm to extract both the fish and water velocities. Both of these developments to Doppler shift measurements enable the detection of fish using broadband ADCPs. The effect of the development of the correlation threshold has been shown in Figure \ref{fig:FishschoolADCPsplit}, through the identification of fish targets in the fish school using intensity and correlation thresholds. The advancement in fish and water velocity extraction through a least-squares algorithm can be used in tandem with the fish detection method to extract fish velocities from ADCP data.\\

Single fish detection and target tracking is difficult in fish schools for split-beam sonars. When aggregations are too dense, fish cannot be individually isolated and their tracks, or velocities, cannot be extracted. The ADCP detected fewer fish in denser areas of fish schools. The bin size difference, 1 m for ADCP and ~0.15 m for split-beam is suspected to contribute to the discrepancy. With a smaller depth bin size the broadband ADCP should be capable of detecting discrete targets in denser areas. By simply relying on the combination of high correlation and high intensity values, the ADCP avoids the target tracking challenges that come with high concentrations of fish. \\

Comparisons between the presented ADCP and split-beam data are also complicated by discrepancies in the collection parameters, frequency and ping rate. We corrected for the beam pattern and pulse length difference by normalizing by the sample volume. Considering the ping rates discrepancy, the ping rate for the split-beam must be higher to detect each fish target at least twice for fish tracking, a requirement established in the Sonar5-Pro tracking parameters. The ADCP only detects each fish once because of the single detection framework of the ADCP target detection method. However, for flow rates greater than 0.7 m/s, the ADCP could miss fish detections at 25 m range from the instrument that the split-beam would be able to detect twice and successfully track. \\

Bubble plumes caused by entrained air were excluded from analysis in split-beam data to reduce the number of false detections. For single echo detections and target tracking, a fish cannot consistently be distinguished from a bubble plume. With the correlation threshold, the ADCP was capable of detecting fish in zones with entrained air by differentiating bubbles clouds from fish targets, as shown in Figure \ref{fig:entrainedaircompare}. The added capabilities of the ADCP showed it isolated fish targets in areas unaccessible for fish detection to the split-beam. However, this comparison is biased due the effect of the difference in frequencies on the backscattered signal of bubble plumes because higher frequencies are less affected by bubble clouds.\\

We show three main results in this paper. We present through a fish detection comparison that broadband ADCPs can detect fish in providing agreement with the industry standard, split-beam echosounder, for the conditions sampled in Grand Passage, NS. Then, we show the ADCP provides an alternative processing approach for dealing with near-surface bubbles that eliminates the need for the more operator intensive process of determining an entrained air exclusion line. Finally, we suggest that if the broadband ADCP data is unaveraged, i.e. one ping per ensemble, then the BAMFF package can be used to extract fish counts. With proper data collection paramerters BAMFF can also be used to extract both fish school velocities and water velocities.


\begin{notes}[Acknowledgements]
Greg Trowse at Luna Ocean Ltd. has provided an unparalleled expertise of the research site and its communities. Mark Downey, from Memorial University of Newfoundland, provided instrumentation knowledge for the deployments and the lab tests. Richard Cheel from Dalhousie University has been invaluable help in the field.
This work was funded by the Nova Scotia Offshore Energy Research Association.
\end{notes}



\begin{thebibliography}{0}
\bibitem{Freitag1993}
Freitag, H. P., Plimpton, P. E., \& McPhaden, M. J. (1993, October). Evaluation of an ADCP fish-bias rejection algorithm. In \textit{Proceedings of OCEANS'93} (pp. II394-II397). IEEE.
\bibitem{Holliday1977}
Holliday, D. V. (1977). Two applications of the Doppler effect in the study of fish schools.  \textit{ICES Journal of Marine Science}, 170, 21-30.
\bibitem{Olsen1983}
Olsen, K., Angel, J., Pettersen, F., Løvik, A., Nakken, O., \& Venema, S. C. (1983). Observed fish reactions to a surveying vessel with special reference to herring, cod, capelin and polar cod. \textit{FAO Fisheries Report}, 300, 139-149.
\bibitem{Demer2000}
Demer, D. A., Barange, M., \& Boyd, A. J. (2000). Measurements of three-dimensional fish school velocities with an acoustic Doppler current profiler. \textit{Fisheries Research}, 47(2-3), 201-214.
\bibitem{Tollefsen2003}
Tollefsen, C. D., \& Zedel, L. (2003). Evaluation of a Doppler sonar system for fisheries applications. \textit{ICES Journal of Marine Science}, 60(3), 692-699.
\bibitem{Zedel2009}
Zedel, L., \& Cyr-Racine, F. Y. (2009). Extracting fish and water velocity from Doppler profiler data. \textit{ICES Journal of Marine Science}, 66(9), 1846-1852.
\bibitem{Melvin2014}
Melvin, G. D. \& Cochrane, N. A. (2014). Investigation of the Vertical Distribution, Movement and Abundance of Fish in the Vicinity of Proposed Tidal Power Energy Conversion Devices. \textit{OEERA/OETR Final Report}. December 2014.
\bibitem{Ona2007}
Ona, E., Godø, O. R., Handegard, N. O., Hjellvik, V., Patel, R., \& Pedersen, G. (2007). Silent research vessels are not quiet. \textit{The Journal of the Acoustical Society of America}, 121(4), EL145-EL150.
\bibitem{Foote1986}
Foote, K. G., Aglen, A., \& Nakken, O. (1986). Measurement of fish target strength with a split‐beam echo sounder. \textit{The Journal of the Acoustical Society of America}, 80(2), 612-621.
\bibitem{Broadhurst2014}
Broadhurst, M., Barr, S., \& Orme, C. D. L. (2014). In-situ ecological interactions with a deployed tidal energy device; an observational pilot study. \textit{Ocean \& coastal management}, 99, 31-38.
\bibitem{Viehman2015b}
Viehman, H. A., \& Zydlewski, G. B. (2015). Fish interactions with a commercial-scale tidal energy device in the natural environment. \textit{Estuaries and Coasts}, 38(1), 241-252.
\bibitem{Viehman2017}
Viehman, H. A., \& Zydlewski, G. B. (2017). Multi-scale temporal patterns in fish presence in a high-velocity tidal channel. \textit{PloS one}, 12(5), e0176405.
\bibitem{Staines2015}
Staines, G., Zydlewski, G., Viehman, H., Shen, H., \& McCleave, J. (2015, September). Changes in vertical fish distributions near a hydrokinetic device in Cobscook Bay, Maine, USA. In \textit{Proceedings of the 11th European Wave and Tidal Energy Conference (EWTEC2015)}, Nantes, France (pp. 6-11).
\bibitem{Fraser2017}
Fraser, S., Nikora, V., Williamson, B. J., \& Scott, B. E. (2017). Automatic active acoustic target detection in turbulent aquatic environments. \textit{Limnology and Oceanography: Methods}, 15(2), 184-199.
\bibitem{Deines1999}
Deines, K. L. (1999). Backscatter estimation using broadband acoustic Doppler current profilers. \textit{In Proceedings of the IEEE Sixth Working Conference on Current Measurement} (pp. 249-253). IEEE.
\bibitem{Mullison2017}
Mullison, J. (2017). Backscatter Estimation Using Broadband Acoustic Doppler Current Profilers-Updated. In \textit{Proceedings of the ASCE Hydraulic Measurements \& Experimental Methods Conference}, Durham, NH, USA (pp. 9-12).
\bibitem{Tollefsen2003}
Tollefsen, C. D., \& Zedel, L. (2003). Evaluation of a Doppler sonar system for fisheries applications. \textit{ICES Journal of Marine Science}, 60(3), 692-699.
\bibitem{RDI2001}
RDI Instruments (2001). Workhorse sentinel adcp user’s guide.
\bibitem{Dunn2019}
Dunn, M. (2019). Developments in Fish Detection using Broadband Acoustic Doppler Current Profiler. Thesis submitted to Memorial University of Newfoundland. 89 pp.
\bibitem{Medwin1997}
Medwin, H., \& Clay, C. S. (1997). \textit{Fundamentals of acoustical oceanography}. Academic press.
\bibitem{Foote1987}
Foote, K. H., \& Knutsen, G. V. maclennan D, Simmonds E. 1987. Calibration of acoustic instruments for fish density estimation: A practical guide. \textit{ICES, Cooperative research Report}, (144), 69.
\bibitem{Balk2018}
Balk, H., \& Lindem, T., Sonar 4 and Sonar 5-Pro post-processing systems. Operator manual 6.0.4, 2018 
\bibitem{Balk2002}
Balk, H., \& Lindem, T. (2002). A new method for single target detection in sonar data characterised with low signal-to-noise ratio. \textit{In Proceedings of 6th ICES Symposium on Acoustics in Fisheries and Aquatic Ecology}. Montpellier, France.
\bibitem{Maclennan2013}
MacLennan, D. N., \& Simmonds, E. J. (2013). \textit{Fisheries acoustics} (Vol. 5). Springer Science \& Business Media.
\end{thebibliography}


\appendix
\section{Sonar5-Pro parameters}


\begin{table}[!htbp]
\centering
\label{tab:ParametersS5}
\caption{Parameters for each component of Cross-Filter Detector for tracking algorithm Sonar5-Pro.}
\begin{tabular}{|c||c|c|r|}
\hline
Component & \multicolumn{2}{c|}{Parameter} & Values\\ 
\hline
\hline
Detector & Foreground Filter & Height & 15 bins \\ 
\cline{3-4}
	&	& Width  & 5 pings\\
\cline{2-4}
	& Background Filter & Method & Mean filter  \\
\cline{3-4}
	&	& Height & 53 bins \\
\cline{3-4}
	&	& Width  & 5 pings \\ 
\cline{3-4}
	&	& Offset  & 10 dB\\
\hline
Evatuator & Track Length & Min. & 4 pings  \\ 
\cline{3-4}
	&	& Max. & 32 pings \\
\cline{2-4}
	& Mean Intensity & Min & -50 dB  \\
\cline{3-4}
	&	 & Max & -28 dB  \\
\cline{2-4}
	& Std. dev. Alo & Min. & 0.97  \\
\cline{3-4}
	&	& Max.  & 5.87   \\ 
\cline{2-4}
	& Std. dev. Ath & Min. & 0.51 \\
\cline{3-4}
	&	& Max.  & 6.73  \\
\hline
SED	&  \multicolumn{2}{c|}{Max gap between detections} &  3 pings  \\
\cline{2-4}
	&  \multicolumn{2}{c|}{Range detection}  &  Center of gravity \\
\hline 
Tracking & \multicolumn{2}{c|}{Min. Track Length} & 5 pings \\ 
\cline{2-4}
	& \multicolumn{2}{c|}{Max. Ping Gap} & 2 pings  \\ 
\cline{2-4}
	& \multicolumn{2}{c|}{Gating Range} & 0.05 m \\ 
\hline
\end{tabular}
\end{table}


\end{document}
