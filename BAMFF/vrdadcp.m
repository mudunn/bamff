function [adcp,cfg]=vrdadcp(ifile,interval)
% function [adcp,cfg]=vrdadcp(ifile,[ens_1 ens_end]);
%
% Function to read in a binary RDI file (ifile) into two structures, adcp and cfg. 
% Adapted from both Rich's and Jody's code by Dewey for VENUS.
% Optional input: [ens_1 ens_end] to read a limited interval of ensembles
%                 i.e. [38000 40000], [40001 inf]: default [1 inf]
%
% This routine does no averaging or screening.
% Output structures:
%   adcp.range      (range to bins in m)
%       .corr       (correlation counts)
%       .intens     (backscatter counts)
%       .vel  [4,nbins,nens] (velocities: u,v,w,e | b1,b2,b3,b4)
%       .pgood      (percent good)
%       .ens        (ensemble number)
%       .heading    (internal compass heading, relative to beam 3)
%       .pitch      (tilt 1, + beam 3 up)
%       .roll       (tilt 2, + beam 2 up)
%       .mtime      (matlab time of ensembles, assumes century 2000)
%       .temperature (internal temperature of head, C)
%       .salinity   (fixed/set or measured salinity psu)
%       .pressure   (fixed/set or measured in m)
%       .depth      (fixed/set or calcualted from pressure)
%       .units      (units of all fields)
%       .bt.*       (bottom track structure/variables)
%       .vm.*       (VMDAS structure/variables)
%    cfg.fw_ver     (firmware version #, i.e. 16)
%       .fw_rev     (firmware revision #, i.e. 28 --> v16.28)
%       .sys_cfg    (2 bit strings coding the hardware configuration)
%       .freq       (the carrier frequency of the ADCP, i.e. 300000 Hz)
%       .beam_pat   ('Concave' or 'Convex')
%       .orient     (vertical orientation, 'Down' or 'Up')
%       .beam_angle (in degrees, i.e. 20)
%       .janus_cfg  (transducer config, i.e. '4-Beam Janus')
%       .xmit_lag   (transmit lag between pulses, units?)
%       .n_beams    (number of transducers, i.e. 4)
%       .nbins      (number of cells/bins)
%       .npings     (number of pings per ensemble)
%       .cell_size  (cell/bin size in m)
%       .blank      (blank after transmit, in m);
%       .profiling_mode (mode WM command, i.e. 1)
%       .corr_thresh (counts, correlation minumum threshold before data is set to bad (-32768)
%       .codereps   (number of code reps per transmission)
%       .pgood_min  (counts, percent good minimum before data set to bad)
%       .errvel_max (error velocity max mm/s)
%       .time_ping  (TPP, time per ping within ensemble)
%       .coord      (binary string from EX command)
%       .dtime_ens  (effective time between ensembles in seconds)
%       .head_align (Compass (beam 3) heading alignment offset, degrees, EA command)
%       .mag_dev    (Magnetic Deviation for location, EB command)
%       .sensor_src (sensor source index for sensors to use, bit string EZ command)
%       .sensor_avail (sensors that are available, bit string)
%       .bin1_dist  (distance in m to center of first bin)
%       .xmit_length (transmission pulse length in m, WT command)
%       .false_trgt (false target threshold in counts, default 50, WA command)
%       .CPU-sn     (CPU serial number)
%       .sys_bndwdth (Bandwidth setting, either broad (0) or narrow (1), WB command)
%       .sys_pwr    (transmit power, counts (0-255), default 255, CQ command)
%       .inst_sn    (instrument serial number, if available)
%       .sos        (fixed/average speed of sound value)
%
% Assumes 21st century (RTC plus year 2000)

% RKD 06/09

% RKD 07/10: Fixed two bugs.
%     1) pitch and roll range between +/- 20 degrees, not correctly convert LSB & MSB (near line 580)
%     2) fixed the indexing (mtime0) to fill in gaps larger than 1.9*dt, (near line 298)


if nargin<2
    interval=[1 inf];
end
indx_1=1;
indx_end=interval(2)-interval(1)+1;
century=2000;
fileinfo=dir(ifile);
if isempty(fileinfo)
    fprintf(' Error* Can''t find file %s\n',ifile);
    return
end
if (interval(1)~=1 && interval(2)==inf) || interval(2)~=inf
   fprintf(' Opening and reading a portion of file: %s\n',ifile);
   % estimate the ensemble size if we have a limited number of ensembles to read
   fid=fopen(ifile,'r','ieee-le');
   ch=uint8(fread(fid,100000,'uint8'));
   fclose(fid);
   in0 = find(ch(1:end-1)==127 & ch(2:end)==127); % find the "7F7F" header start
   din0=diff(in0);
   bpens=max(din0);  % estimate of ensemble size
   disp([' Estimated ensemble size: ',num2str(bpens)]);
   fileinfo=dir(ifile);
   nens=fix(fileinfo.bytes/bpens);
   fid=fopen(ifile,'r','ieee-le');
   if interval(1) > 1
       skip=interval(1)-1;
       fseek(fid,skip*bpens,'bof');
   end
   rens=interval(2) - interval(1) + 2; % get a little extra
   ch=uint8(fread(fid,rens*bpens,'uint8'));
   fclose(fid);
else % read entire file
   fprintf(' Opening and loading entire file: %s\n',ifile);
   fid=fopen(ifile,'r','ieee-le');
   ch = uint8(fread(fid,inf,'uint8')); % read the entire file into RAM
   fclose(fid);
end

nbytes=length(ch);
disp([num2str(nbytes/(1024*1024)),' MB of data read into RAM.']);
in0 = find(ch(1:end-1)==127 & ch(2:end)==127); % find the "7F7F" header start
din0=diff(in0);
bpens=max(din0)-2;  % estimate of ensemble size
nens=fix(nbytes/bpens);  % projected number of ensembles
% these are all the header candidates.  Some are data, some are actual
% headers.... (i.e. "7F7F" might occur in the data blocks)
bytesens0 = double(ch(in0+2))+double(ch(in0+3))*256; % bytes in each block between "7F7F"
i=1;j=0;
in=NaN*ones(size(in0)); % memory allocation
iprt=ceil(nens/500)*100;

bytesens=in;
fprintf('Parsing into good records. \n');
while i<length(in0) % Identify the header blocks
  if (in0(i+1)-in0(i)) == (bpens+2) % then in0(i) is the start of a header block
    j=j+1;
    in(j)=in0(i);
    bytesens(j)=bytesens0(i);
  else
    ii=i+1;
    while (in0(ii)-in0(i)) < (bpens+2) && ii < length(in0)
        ii=ii+1; % skip this false "7F7F"
    end
    j=j+1;
    in(j)=in0(i);
    bytesens(j)=bytesens0(i);
    i=ii-1; % next good
  end
  
  if mod(j,iprt)==0
    fprintf('%d.',j);
  end
  i=i+1;
end
% hope the last record is complete
j=j+1;
in(j)=in0(i);
bytesens(j)=bytesens0(i);
%
fprintf('\n');
indx=find(~isnan(in));
in=in(indx);
bytesens=bytesens(indx);
% make an array

if in(end)+bytesens(end)>length(ch), % check last record still long enough
  in=in(1:end-1);
  bytesens=bytesens(1:end-1);
end;
dat = double(ch(in(1)+[1:bytesens(1)]-1));  % pre-allocate

% preallocate arrays...
hdr = gethdr(dat,0);
ldr = getleader(dat,hdr.offsets(1));
cfg=ldr; % save cfg for return
% get some things from the leader:
adcp.range = [ldr.bin1_dist+((1:ldr.nbins)-1)*ldr.cell_size]';

adcp.corr = zeros(ldr.nbins,length(in),4)+NaN;
adcp.intens = zeros(ldr.nbins,length(in),4)+NaN;
adcp.vel = zeros(ldr.nbins,length(in),4)+NaN;
adcp.pgood = zeros(ldr.nbins,length(in),4)+NaN;

varl = initvarl(length(in));
istart=1; %max([1 indx_1]);
iend=min([indx_end length(in)]);

fprintf('Decoding %d records...\n',[iend-istart+1]);
i=0;iii=0;
for ii=istart:iend
  i=i+1;
  if mod(i,1000)==0
    fprintf('%d of %d\n',i,[iend-istart+1]);
  end
  dat = double(ch(in(ii)+[0:bytesens(ii)-1]));
  % check the checksum
  hexrec=dec2hex(ch(in(ii)+[0:bytesens(ii)+1]))';
  cs=checksum(hexrec); % checksum as read
  ocs=hexrec(end-3:end); % checksum in record
  if cs~=ocs
      disp(['Ensemble: ',ii,' Checksums do not match: ',ocs,' and ',cs,'  Record may be corrupt, skipping.']);
  else
     %
     hdr = gethdr(dat,0);
     % get the ids...
     ids = dat(hdr.offsets+1)+ 256*dat(hdr.offsets+2);
     varl = getvar(dat,hdr.offsets(2),varl,i);
     offsets = hdr.offsets;
     nbins = ldr.nbins;
     for j=3:length(hdr.offsets)
       switch ids(j)
         case 256 
           vel = getvel(dat,offsets(j),nbins);
           adcp.vel(:,i,:)=reshape(vel,4,nbins)';
         case 512
           corr = getcor(dat,offsets(j),nbins);
           adcp.corr(:,i,:)=reshape(corr,4,nbins)';
         case 768
           intens = getint(dat,offsets(j),nbins);
           adcp.intens(:,i,:)=reshape(intens,4,nbins)';
         case 1024
           percgood = getpercgood(dat,offsets(j),nbins);
           adcp.pgood(:,i,:)=reshape(percgood,4,nbins)';
         case 1536
           if ~isfield(adcp,'bt')
               adcp = initbot(length(in),adcp); 
           end
           adcp = getbot(dat,offsets(j),adcp,i);
         case 8192
           % vmdas file with nav info in it...  From vmdas manual
           if ~isfield(adcp,'slat')
             adcp = initvmhd(length(in),adcp);
           end
           adcp = getvmhd(dat,offsets(j),adcp,i);
         otherwise % not sure either what this data type is or where it's pointing
           disp(['Ens: ',num2str(i),' Data type: ',num2str(j-2),' [vel,corr,intens,pgood] Bad Offset:',dec2hex(ids(j))]);
           % fill these profiles with NaNs
           adcp.vel(:,i,:)=NaN;  % :( this may not be absolutely necessary, but this record is corrupt somehow.
           adcp.corr(:,i,:)=NaN;  % :(
           adcp.intens(:,i,:)=NaN;  % :(
           adcp.pgood(:,i,:)=NaN;  % :(
           break % out of case/switch loop
       end   
     end
  end
end

adcp.ens=varl.ens;
adcp.heading=varl.heading;
adcp.pitch=varl.pitch;
adcp.roll=varl.roll;
adcp.mtime=varl.mtime;
adcp.temperature=varl.temperature;
adcp.salinity=varl.salinity;
adcp.pressure=varl.pressure;
adcp.depth=varl.depth;
cfg.sos=mean(varl.sos(~isnan(varl.sos)));


% fix order of vels
todo = {'corr','intens','vel','pgood'};
for ii=1:length(todo)
  adcp.(todo{ii}) = permute(adcp.(todo{ii}),[3 1 2]);
end
% Note that beam2uvw will also need the order re-re-done again to [3 2 1]

% set flagged values to NaNs
in=find(adcp.vel==-32768);adcp.vel(in)=NaN;
in=find(adcp.corr==-32768);adcp.corr(in)=NaN;
in=find(adcp.intens==-32768);adcp.intens(in)=NaN;
in=find(adcp.pgood==-32768);adcp.pgood(in)=NaN;

% apply scaling, mm/s to m/s
adcp.vel=adcp.vel*0.001;

%
if indx_end~=inf % then the user asked for a reduced set, truncate end NaNs
    iend=max(find(~isnan(adcp.mtime)));
    adcp.vel=adcp.vel(:,:,1:iend);
    adcp.corr=adcp.corr(:,:,1:iend);
    adcp.intens=adcp.intens(:,:,1:iend);
    adcp.pgood=adcp.pgood(:,:,1:iend);
    adcp.mtime=adcp.mtime(1:iend);
    adcp.ens=adcp.ens(1:iend);
    adcp.heading=adcp.heading(1:iend);
    adcp.pitch=adcp.pitch(1:iend);
    adcp.roll=adcp.roll(1:iend);
    adcp.temperature=adcp.temperature(1:iend);
    adcp.salinity=adcp.salinity(1:iend);
    adcp.pressure=adcp.pressure(1:iend);
    adcp.depth=adcp.depth(1:iend);
end



% now look for gaps in the data and fill with NaNs
% there should be the right number of ensembles given a uniform ensemble interval
dtens=diff(adcp.mtime);
dt=round(median(dtens)*24*36000)/(24*36000);  % rounded to nearest 10th of a second
cfg.dtime_ens=dt*24*3600;  % ensemble interval in decimal seconds
indx=find(dtens>(1.9*dt)) + 1;  % these are the indecices to the gaps nearly 2dt or greater
Ndt=0;N=0;
if ~isempty(indx)
    % Speed this up by pre-allocating mamory based on first and last mtime and dt
    DT=adcp.mtime(end)-adcp.mtime(1);
    N=DT/dt + 1; % this should be about the new length of the matrices
    [m,n,o]=size(adcp.vel);
    adcp.vel(:,:,[o+1:N])=0;
    adcp.corr(:,:,[o+1:N])=0;
    adcp.intens(:,:,[o+1:N])=0;
    adcp.pgood(:,:,[o+1:N])=0;
    adcp.heading([o+1:N])=0;adcp.pitch([o+1:N])=0;adcp.roll([o+1:N])=0;
    adcp.temperature([o+1:N])=0;adcp.salinity([o+1:N])=0;adcp.pressure([o+1:N])=0;adcp.depth([o+1:N])=0;
    disp(['Filling in ',num2str(length(indx)),' temporal gaps/missing ensembles with NaN profiles.']);
    mtime0=adcp.mtime; % save the original time base with gaps
    for i=length(indx):-1:1, % start from the back and insert NaN Ensembles in gaps.
        j=indx(i);
        dti=mtime0(j)-mtime0(j-1); % this is the duration of the gap to fill
        ndt=round(dti/dt)-1; % number of ensembles to insert
        if ndt>0
            Ndt=Ndt+ndt;
            o=length(adcp.mtime); % check the current size/dimensions/length of the time base
            jj=j+ndt-1;
        % for each gap, from back to front, shift all data towards the end, making room for NaN profiles
            adcp.vel(:,:,[j:o]+ndt)=adcp.vel(:,:,[j:o]);adcp.vel(:,:,[j:jj])=NaN;
            adcp.corr(:,:,[j:o]+ndt)=adcp.corr(:,:,[j:o]);adcp.corr(:,:,[j:jj])=NaN;
            adcp.intens(:,:,[j:o]+ndt)=adcp.intens(:,:,[j:o]);adcp.intens(:,:,[j:jj])=NaN;
            adcp.pgood(:,:,[j:o]+ndt)=adcp.pgood(:,:,[j:o]);adcp.pgood(:,:,[j:jj])=NaN;
            adcp.heading([j:o]+ndt)=adcp.heading([j:o]);adcp.heading([j:jj])=NaN;
            adcp.pitch([j:o]+ndt)=adcp.pitch([j:o]);adcp.pitch([j:jj])=NaN;
            adcp.roll([j:o]+ndt)=adcp.roll([j:o]);adcp.roll([j:jj])=NaN;
            adcp.temperature([j:o]+ndt)=adcp.temperature([j:o]);adcp.temperature([j:jj])=NaN;
            adcp.salinity([j:o]+ndt)=adcp.salinity([j:o]);adcp.salinity([j:jj])=NaN;
            adcp.pressure([j:o]+ndt)=adcp.pressure([j:o]);adcp.pressure([j:jj])=NaN;
            adcp.depth([j:o]+ndt)=adcp.depth([j:o]);adcp.depth([j:jj])=NaN;
            adcp.mtime([j:o]+ndt)=adcp.mtime([j:o]); % the mtimes and ens should remain continuous/non-NaN
            adcp.mtime([j:jj])=adcp.mtime(j-1)+[1:ndt]*dt; % these are the approximate times of the NaN profiles
        end
    end
    adcp.ens=[adcp.ens(1):(adcp.ens(1)+length(adcp.mtime)-1)];  % re-set the entire ensemble count
end
disp(['Numebr of NaN profiles inserted: ',num2str(Ndt)]);
N1=length(adcp.ens);
if N>N1 % some excess memory allocation, truncate
   N2=[N1+1:N];
   adcp.vel(:,:,N2)=[];
   adcp.corr(:,:,N2)=[];
   adcp.intens(:,:,N2)=[];
   adcp.pgood(:,:,N2)=[];
   adcp.heading(N2)=[];adcp.pitch(N2)=[];adcp.roll(N2)=[];adcp.temperature(N2)=[];
   adcp.salinity(N2)=[];adcp.pressure(N2)=[];adcp.depth(N2)=[];
end
disp(['Final number of consecutive ensembles returned: ',num2str(length(adcp.mtime))]);
adcp.units.temperature='C';
adcp.units.salinity='psu';
adcp.units.pressure='db';
adcp.units.depth='m';
adcp.units.range='m';
adcp.units.corr='counts';
adcp.units.intens='counts';
adcp.units.pgood='%';
adcp.units.velocity='m/s';
adcp.units.mtime='Matlab time in UTC';
adcp.units.heading='degrees';
adcp.units.pitch='degrees';
adcp.units.roll='degrees';
if cfg.coord(1:2)=='00'
    adcp.units.Reference_Frame='Beam Coordinates';
elseif cfg.coord(1:2)=='01'
    adcp.units.Reference_Frame='Instrument Coordinates';
elseif cfg.coord(1:2)=='01'
    adcp.units.Reference_frame='Instrument Coordinates';
elseif cfg.coord(1:2)=='10'
    adcp.units.Reference_Frame='Ship Coordinates';
elseif cfg.coord(1:2)=='11'
    adcp.units.Reference_Frame='Earth Coordinates';
end
if cfg.coord(1:2)~='00' & cfg.coord(3)=='1'
    adcp.units.Tilts_Applied='Yes';
else
    adcp.units.Tilts_Applied='No';
end
if cfg.coord(1:2)~='00' & cfg.coord(4)=='0'
    adcp.units.Three_Beam_Solution='No';
else
    adcp.units.Three_Beam_Solution='Yes';
end
if cfg.coord(1:2)~='00' & cfg.coord(5)=='0'
    adcp.units.Bin_Mapping_Applied='No';
else
    adcp.units.Bin_Mapping_Applied='Yes';
end
% fini


%%
function hdr=gethdr(dat,offset)
%
hdr.id = dat(1);
hdr.source= dat(2);
hdr.nbyte = dat(3)+256*dat(4);
hdr.ndat = dat(6);
hdr.offsets=dat(7+[0:1:hdr.ndat-1]*2) + 256*dat(8+[0:1:hdr.ndat-1]*2);


%%
function ldr=getleader(dat,off,ldr,i)
% parse fixed leader/header 
ldr = [];
off = off+1;
dd = double(dat(off:end));
ldr.fw_ver=dd(3);
ldr.fw_rev=dd(4);
ldr.sys_cfg(1,:)=dec2bin(dd(5),8);
ldr.sys_cfg(2,:)=dec2bin(dd(6),8);
if ldr.sys_cfg(1,6:8)=='000', ldr.freq='75 kHz'; end
if ldr.sys_cfg(1,6:8)=='001', ldr.freq='150 kHz'; end
if ldr.sys_cfg(1,6:8)=='010', ldr.freq='300 kHz'; end
if ldr.sys_cfg(1,6:8)=='011', ldr.freq='600 kHz'; end
if ldr.sys_cfg(1,6:8)=='100', ldr.freq='1200 kHz'; end
if ldr.sys_cfg(1,6:8)=='101', ldr.freq='2400 kHz'; end
if ldr.sys_cfg(1,5)=='0', ldr.beam_pat='Concave'; end
if ldr.sys_cfg(1,5)=='1', ldr.beam_pat='Convex'; end
if ldr.sys_cfg(1,1)=='0', ldr.orient='Down'; end
if ldr.sys_cfg(1,1)=='1', ldr.orient='Up'; end
if ldr.sys_cfg(2,7:8)=='00', ldr.beam_angle=15; end
if ldr.sys_cfg(2,7:8)=='01', ldr.beam_angle=20; end
if ldr.sys_cfg(2,7:8)=='10', ldr.beam_angle=30; end
if ldr.sys_cfg(2,7:8)=='11', ldr.beam_angle=dd(59); end
if ldr.sys_cfg(2,1:4)=='0100', ldr.janus_cfg='4-Beam Janus'; end
if ldr.sys_cfg(2,1:4)=='0101', ldr.janus_cfg='5-Beam Janus v1'; end
if ldr.sys_cfg(2,1:4)=='1111', ldr.janus_cfg='5-Beam Janus v2'; end
%ldr.simflag=dd(7); % always hard set to zero?
ldr.xmit_lag=dd(8);
ldr.n_beams=dd(9);
ldr.nbins = dd(10);
ldr.npings = dd(11)+256*dd(12);
ldr.cell_size = (dd(13)+256*dd(14))*0.01;
ldr.blank = (dd(15)+256*dd(16))*0.01;
ldr.profiling_mode = dd(17);
ldr.corr_thresh=dd(18);
ldr.codereps= dd(19);
ldr.pgood_min=dd(20);
ldr.errvel_max=dd(21)+256*dd(22);
ldr.time_ping=dd(23)*60 + dd(24) + dd(25)/100;
ldr.coord=dec2bin(dd(26),5);
ldr.head_align = dd(27)+256*dd(28);
if ldr.head_align > 32768
  ldr.head_align = -2*32768+ldr.head_align;
end
ldr.head_align=ldr.head_align*0.01;
ldr.mag_dev = dd(29) + 256*dd(30);
if ldr.mag_dev > 32768
  ldr.mag_dev = -2*32768+ldr.mag_dev;
end
ldr.mag_dev=ldr.mag_dev*0.01;
ldr.sensor_src = dec2bin(dd(31),7);
ldr.sensor_avail = dec2bin(dd(32),7);
ldr.bin1_dist = (dd(33)+256*dd(34))*0.01;
ldr.xmit_len= dd(35)+256*dd(36);
ldr.false_trgt=dd(39);
ldr.xmit_lag=dd(41) + 256*dd(42);
ldr.CPU_sn=dec2hex(dd(43:50));
ldr.sys_bndwdth=dd(51) + 256*dd(52);
ldr.sys_pwr=dd(53);
ldr.inst_sn=dd(55:58)';


%% 
function adcp = initvmhd(nens,adcp)
% initialize VMDAS Variables
x=ones(1,nens)*NaN;
adcp.stime = x;
adcp.etime = x;
adcp.slat = x;
adcp.slon = x;
adcp.elat = x;
adcp.elon = x;
adcp.spd= x;
adcp.headtrue = x;
adcp.headmag = x;
adcp.smg = x;
adcp.hmg = x;


%% 
function adcp = getvmhd(dat,off,adcp,i)
% parsing VMDAS Header
dd = dat((off+1):end);
time = dd(7)+dd(8)*256+dd(9)*256^2+dd(10)*256^3;
time = mod(time*0.0001,24*3600); % seconds...
year = 2000 + dd(5)+256*dd(6);
adcp.vm.stime(i) = datenum(year,dd(4),dd(3),0,0,time);
if i>1 && adcp.vm.stime(i)>adcp.vm.stime(i-1)+0.5
  adcp.vm.stime(i) = adcp.vm.stime(i)-1;
end
time = dd(23)+dd(24)*256+dd(25)*256^2+dd(26)*256^3;
time = mod(time*0.0001,24*3600); % seconds...
adcp.vm.etime(i) = datenum(year,dd(4),dd(3),0,0,time);
% believe it or not the date clicks over before the time (sigh)...

if i>1 && adcp.vm.etime(i)>adcp.vm.etime(i-1)+0.5
  adcp.vm.etime(i) = adcp.vm.etime(i)-1;
end

lat = dd(15)+dd(16)*256+dd(17)*256^2+dd(18)*256^3;
if lat>2^32/2
  lat = -2^32+lat;
end
adcp.vm.slat(i) = lat*180/2^31;
lon = dd(19)+dd(20)*256+dd(21)*256^2+dd(22)*256^3;
if lon>2^32/2
  lon = -2^32+lon;
end
adcp.vm.slon(i) = lon*180/2^31;

lat = dd(27)+dd(28)*256+dd(29)*256^2+dd(30)*256^3;
if lat>2^32/2
  lat = -2^32+lat;
end
adcp.vm.elat(i) = lat*180/2^31;

lon = dd(31)+dd(32)*256+dd(33)*256^2+dd(34)*256^3;
if lon>2^32/2
  lon = -2^32+lon;
end
adcp.vm.elon(i) = lon*180/2^31;
% the rest of this is crap for some reason...
x = dat(35)+256*dat(36);
if x > 2^16/2
  x=-2^16+x;
end
adcp.vm.spd(i) = x*1e-3;

adcp.vm.headtrue(i) = dat(37)+256*dat(38);
adcp.vm.headmag(i) = dat(39)+256*dat(40);
x = dat(41)+dat(42)*256;
if x > 32768
  x=-2*32768+x;
end
adcp.vm.smg(i) = x*1e-3;
adcp.vm.hmg(i) = dat(43)+256*dat(44);

%% 
function adcp = initbot(nens,adcp)
x=ones(4,nens)*NaN;
adcp.bt.range = x;
adcp.bt.vel = x;
adcp.bt.corr = x;
adcp.bt.amp = x;
adcp.bt.wrlon= NaN*zeros(1,nens);
adcp.bt.wrlat= NaN*zeros(1,nens);

%%
function adcp = getbot(dat,off,adcp,i)

dd = dat(off+(1:90));
x = getshort(dd(25:32));
x(x==-32768)=NaN;
adcp.bt.vel(:,i)=x/1000;
adcp.bt.range(:,i) = getushort(dd(17:24))/100;
bad = find(adcp.bt.range(:,i)==0);
adcp.bt.range(bad,i) = NaN;
adcp.bt.corr(:,i) = dd(33:36);
adcp.bt.amp(:,i) = dd(37:40);

% some odd winriver stuff that may or may not be here....
lat=dd(5)+dd(6)*256+dd(47)*256^2+dd(48)*256^3;
if lat>2^32/2
  lat = -2^32+lat;
end
adcp.bt.wrlon(i)=lat*180/2^31;

lat=dd(13)+dd(14)*256+dd(15)*256^2+dd(16)*256^3;
if lat>2^32/2
  lat = -2^32+lat;
end
adcp.bt.wrlat(i)=lat*180/2^31;

%% function
function x = getshort(dd)
x = dd(1:2:end)+256*dd(2:2:end);
in = find(x>32767);
x(in)=-2*32768+x(in);

%% function
function x = getushort(dd)
x = dd(1:2:end)+256*dd(2:2:end);

%% 
function varl = initvarl(nens)
x=ones(1,nens)*NaN;
varl.mtime = x;
varl.ens = x;
varl.sos = x;
varl.heading = x;
varl.pitch = x;
varl.roll = x;
varl.depth = x;
varl.temperature = x;
varl.salinity = x;
varl.pressure = x;
varl.heading_std = x;
varl.pitch_std = x;
varl.roll_std = x;

%%
function varl=getvar(dat,off,varl,i)
%
ldr = [];
off = off+1;
dd = dat(off:end);
varl.ens(i)=dd(3) + dd(4)*2^8 + dd(12)*2^16;
dd(5)=dd(5)+2000; % force to the 21 century
varl.mtime(i)=datenum(dd(5:10)') + dd(11)/(24*3600*100);
varl.sos(i)=dd(15) + 256*dd(16);
varl.depth(i)=(dd(17) + 256*dd(18))*0.1;
varl.heading(i)=(dd(19)+256*dd(20))*0.01;
varl.pitch(i)=(dd(21)+256*dd(22))*0.01;
% pitch and roll can be negative +/- 20 degrees, subtract (255+255*256)/0.01
if varl.pitch(i)>327, varl.pitch(i)=varl.pitch(i)-655.35; end
varl.roll(i)=(dd(23)+256*dd(24))*0.01;
if varl.roll(i)>327, varl.roll(i)=varl.roll(i)-655.35; end
varl.salinity(i)=(dd(25) + 256*dd(26));
varl.temperature(i)=(dd(27) + 256*dd(28))*0.01;
varl.heading_std(i)=dd(32);
varl.pitch_std(i)=dd(33)*0.1;
varl.roll_std(i)=dd(34)*0.1;
varl.pressure(i)=(dd(49) + dd(50)*2^8 + dd(51)*2^16 + dd(52)*2^24)/1000;

%%
function vel=getvel(dat,offset,nbins)
offset = offset+2;
vel = dat(offset+2*[1:4*nbins]-1)+256*(dat(offset+2*[1:4*nbins]));
vel(vel>=32768)=-2*32768+vel(vel>=32768);

%%
function vel=getcor(dat,offset,nbins)
offset = offset+2;
vel = dat(offset+[1:4*nbins]);

%%
function vel=getint(dat,offset,nbins)
offset = offset+2;
vel = dat(offset+[1:4*nbins]);

%%
function vel=getpercgood(dat,offset,nbins)
offset = offset+2;
vel = dat(offset+[1:4*nbins]);

%%
function hexcs=checksum(lineHEX)
% function checksum(lineHEX)
pline=lineHEX(1:end-4);
nhex=fix(length(pline)/2); % get the actual size of the data block read
plinebyte=reshape(pline(1:nhex*2),2,nhex); % resize HEX vector string into 2xN matrix
dec=hex2dec(plinebyte'); % use internal hex to decimal conversion for each HEX pair
ncs=mod(sum(dec),65536);
mlhexcs=dec2hex(ncs,4);
hexcs(1:2)=mlhexcs(3:4);
hexcs(3:4)=mlhexcs(1:2);

% fini