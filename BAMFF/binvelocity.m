function[] = binvelocity(params)
%**************************************************************************
%This program will analyse the velocity profile of the fish as a function
%of time and depth intervals
%**************************************************************************
%% Load timeseries
load([params.fileout 'timeseries.mat']);


%% Open first file
ifilenumber = 0;
number = sprintf('%03i',ifilenumber);
filename2 = [params.fileout number '.mat'];
fprintf('MATLAB is loading the data \n')
load(filename2);

while isempty(fish.ifish)
    ifilenumber = ifilenumber+1;
    number = sprintf('%03i',ifilenumber);
    filename2 = [params.fileout number '.mat'];
    fprintf('MATLAB is loading the data \n')
    load(filename2);
end

%% Display start and end time and depth
[ystart,mstart,dstart,hstart,mistart,sstart] = datevec(min(fish.timefish));
[yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish));
 
disp(['Start time: ' num2str(ystart) '/' num2str(mstart) '/' num2str(dstart) ...
       '  ' num2str(hstart) ':' num2str(mistart) ':' num2str(sstart)])
disp(['End time: ' num2str(yend) '/' num2str(mend) '/' num2str(dend) ...
       '  ' num2str(hend) ':' num2str(miend) ':' num2str(send)])
endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);

timestep = params.timebin;
disp(['Minimum depth of data: ' num2str(min(fish.depthoffish)) ' m'])
disp(['Maximum depth of data: ' num2str(max(fish.depthoffish)) ' m'])


%% Build final arrays
stepdepth = params.depthbin(3);
depths = params.depthbin(1):stepdepth:params.depthbin(2);


starttime = min(Time_series);
endtime = max(Time_series);
tt=starttime:timestep:endtime;

Vel_x = zeros(length(tt),length(depths));
Vel_y = zeros(length(tt),length(depths));
Vel_z = zeros(length(tt),length(depths));

Sig_vx = zeros(length(tt),length(depths));
Sig_vy = zeros(length(tt),length(depths));
Sig_vz = zeros(length(tt),length(depths));

Numfish = zeros(length(tt),length(depths));
Nfish = zeros(length(tt),length(depths));

Vel_xwater = zeros(length(tt),length(depths));
Vel_ywater = zeros(length(tt),length(depths));
Vel_zwater = zeros(length(tt),length(depths));
 
 
%define which interval we are averaging over and what is the minimum number
% of fish required to solve the equation
%***********************************************************************
thres=params.fishthres;    %the minimun number of fish necessary to find a meaningful velocity 
%***********************************************************************
fprintf('MATLAB is processing the data \n')
sampletime = [];
nfilesmax = countfiles(params.fileout)-1;

%% Cycle through time steps
for itimebin = 1:length(tt)
    ifilenumber = 0;
    number = sprintf('%03i',ifilenumber); 
    filename2 = [params.fileout number '.mat'];
    fprintf(['MATLAB is loading file ' number '\n'])
    load(filename2);
    if ~isempty(fish.timefish)
        [yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish));
        endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);
    end

    sampletime(itimebin) = tt(itimebin) + timestep/2;
    rtime = datevec(tt(itimebin));
    etime = datevec(endtime_thisfile);

     
    %Find data that fall into this time interval
    instime = find(fish.timefish > tt(itimebin)-timestep/2 & fish.timefish <= (tt(itimebin)+timestep/2));
    dep=fish.depthoffish(instime);
    vel=fish.vfish(instime);
    vec=fish.beamvector(:,instime);
    ints = fish.ifish(instime);
     
     
    %% Open the files necessary for the time step.
    for ifilenumber=1:nfilesmax
        %for ((tt(itimebin) + timestep) >= endtime_thisfile)
         
            %ifilenumber = ifilenumber + 1;
            number = sprintf('%03i',ifilenumber);     

            filename2 = [params.fileout number '.mat'];
            fprintf(['MATLAB is loading file ' number '\n'])
            load(filename2);
            
            if ~isempty(fish.timefish)
                [yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish));
                endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);
            end
            instime = find(fish.timefish >= tt(itimebin) & fish.timefish <= (tt(itimebin)+timestep));

            dep = [dep fish.depthoffish(instime)];
            vel = [vel fish.vfish(instime)];
            vec = [vec fish.beamvector(:,instime)];
            ints = [ints fish.ifish(instime)];
        
        %end
    end

    
     
     %% Calculates velocity in each depth bin
     for idepthbin = 1:length(depths)
         %insdepth = find(dep>(depths - stepdepth/2) & dep<=(depths+stepdepth/2) & (ints < highthresh) );
         insdepth = find(dep>(depths(idepthbin) - stepdepth/2) & dep<=(depths(idepthbin)+stepdepth/2));% & ~isnan(vel);
         
         % Set values to NaN if no fish (or <threshold) in that depth bin
         if(isempty(insdepth) || length(insdepth)<=thres || all(isnan(vel(insdepth))))
             
            Vel_x(itimebin,idepthbin) = NaN;
            Vel_y(itimebin,idepthbin) = NaN;
            Vel_z(itimebin,idepthbin) = NaN;

            Sig_vx(itimebin,idepthbin) = NaN;
            Sig_vy(itimebin,idepthbin) = NaN;
            Sig_vz(itimebin,idepthbin) = NaN;
            
            Nfish(itimebin, idepthbin) = length(insdepth);
            Numfish(itimebin,idepthbin) = length(insdepth);
         else
           kx = vec(1,insdepth);
           ky = vec(2,insdepth);
           kz = vec(3,insdepth);

           [v_x,v_y,v_z,sigma_vx,sigma_vy,sigma_vz,nfish] = resolve3d_sub(kx,ky,kz,-vel(insdepth));
% -ve on vel because adcp velocities are +ve toward transducer and therefore -ve in the beam direction
            Vel_x(itimebin,idepthbin) = v_x;
            Vel_y(itimebin,idepthbin) = v_y;
            Vel_z(itimebin,idepthbin) = v_z;

            Sig_vx(itimebin,idepthbin) = sigma_vx;
            Sig_vy(itimebin,idepthbin) = sigma_vy;
            Sig_vz(itimebin,idepthbin) = sigma_vz;
            
            %Number of fish used in velocity calculation
            Numfish(itimebin,idepthbin) = nfish;
            Nfish(itimebin, idepthbin) = length(insdepth);
         end
     end
end

%% Save final values  
Vel_x_fish = Vel_x;
Vel_y_fish = Vel_y;
save ([params.fileresults 'fish_results.mat'],'Vel_x_fish','Vel_y_fish', 'Nfish') 




%% notfish******************************************************************
if params.notfish
    ifilenumber = 0;
    number = sprintf('%03i',ifilenumber);        
    filename2 = [params.filenotfish number '.mat'];
    fprintf('MATLAB is loading the data \n')
    load(filename2);

    [yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish));
    endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);
    
    % Use same start and end time as fish.
    itimebin=0;
    sampletime=[];
    for itimebin = 1:length(tt)
         sampletime(itimebin) = tt(itimebin) + timestep/2;
         rtime = datevec(tt(itimebin));
         etime = datevec(endtime_thisfile);


         %Split the data into time intervals
         %vv=find(fish.timefish >= tt & fish.timefish <= (tt+timestep));
         instime = find(fish.timefish >= tt(itimebin) & fish.timefish <= (tt(itimebin)+timestep));

         dep = fish.depthoffish(instime);
         vel = fish.vfish(instime);
         vec = fish.beamvector(:,instime);
         ints = fish.ifish(instime);

         if ifilenumber < nfilesmax
            if ((tt(itimebin) + timestep) > datenum(etime))
                ifilenumber = ifilenumber + 1;
                number = sprintf('%03i',ifilenumber);        
                filename3 = [params.filenotfish number '.mat'];
                fprintf('MATLAB is loading the data \n')
                load(filename3)
                [yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish));
                endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);

                instime = find(fish.timefish >= tt(itimebin) & fish.timefish <= (tt(itimebin)+timestep));

                dep = [dep fish.depthoffish(instime)];
                vel = [vel fish.vfish(instime)];
                vec = [vec fish.beamvector(:,instime)];
                ints = [ints fish.ifish(instime)];
            end
         end



             %% Split the water column into depth bins
         for idepthbin = 1:length(depths)
             insdepth = find(dep>(depths(idepthbin) - stepdepth/2) & dep<=(depths(idepthbin)+stepdepth/2));

             if(isempty(insdepth) || length(insdepth)<=thres || all(isnan(vel(insdepth))))
                Vel_xwater(itimebin,idepthbin) = NaN;
                Vel_ywater(itimebin,idepthbin) = NaN;
                Vel_zwater(itimebin,idepthbin) = NaN;

                Sig_vx(itimebin,idepthbin) = NaN;
                Sig_vy(itimebin,idepthbin) = NaN;
                Sig_vz(itimebin,idepthbin) = NaN;

             else
                kx = vec(1,insdepth);
                ky = vec(2,insdepth);
                kz = vec(3,insdepth);

                [v_x,v_y,v_z,sigma_vx,sigma_vy,sigma_vz,~] = resolve3d_sub(kx,ky,kz,-vel(insdepth));
                % -ve on vel because adcp velocities are +ve toward transducer and therefore -ve in the beam direction
                Vel_xwater(itimebin,idepthbin) = v_x;
                Vel_ywater(itimebin,idepthbin) = v_y;
                Vel_zwater(itimebin,idepthbin) = v_z;

                Sig_vx(itimebin,idepthbin) = sigma_vx;
                Sig_vy(itimebin,idepthbin) = sigma_vy;
                Sig_vz(itimebin,idepthbin) = sigma_vz;

             end
         end
    end     
    
    %% Save results
    Vel_x_water = Vel_xwater;
    Vel_y_water = Vel_ywater;
    save ([params.fileresults 'water_results.mat'],'Vel_x_water','Vel_y_water') 
    
end




end
