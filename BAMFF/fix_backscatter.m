%------------------------------------------------------------------------------------------
% FIX_BACKSCATTER - Adjusts backscatter calibration to allow for
% spherical dispersion of signal
%
% INPUTS: backscatter 	- Raw backscatter intensity. A  matrix where columns represent samples 
%			  in time and rows represent depth (where row 1 = closest bin to 
%			  transducer).
%	  Tx 		- transducer temperature. This temperature is also used in calculate 
%	  		  alpha since a vertical profile is not available.
%	  D 		- depth cell length (metres)
%	  B 		- blank after transmit
%	  L		- transmit pulse length in metres 
%	  S		- S is the reference salinity level
%         C             - system scaling from RDI tables 
%                       - -143.5  for 300 kHz workhorse
%         kc            - calibration sensitivity ... about 0.45 for 300 kHz
%         Pdbw          - transmit power
%                       - 14 for 300 kHz workhorse
%         Er            - minimum value observed, should be something like 40;
%         nbins         - number of depth bins
%
%	  Note that some of the coefficients below may need to be changed if different
%	  instruments are used.
%
% OUTPUTS: Corrected backscatter intensity
%
% Sv = C + 10log10((Tx + 273.16) * R^2) - Ldbm - Pdbw + (2 * alpha * R) + Kc * (E - Er);
%
%
% Refer to Deines, K. L. (1999)
%
%
% Derek Tittensor, October 2002.
% 
% Modified to accept all parameters as input to the function
%
% Len Zedel        June 2003.
%-----------------------------------------------------------------------------------------

function Sv = fix_backscatter(backscatter, Tx, D, B, L, S, C, Kc, Pdbw, Er, nbins)


%%%%% SET UP COEFFICIENTS


% 10log10(Transmit pulse length)
Ldbm = 10 * log10(L);

% Beam angle. Twenty degrees
theta = pi * (20 / 180);


% Size of input array 
[rows, cols] = size(backscatter);

if (rows == nbins)
  profiles = cols;
else
  profiles = rows;
end


% Depth cell numbers
N = 1:nbins;
%N = N';

%%%%%



%%%%% CALCULATE EQUATION COMPONENTS

% Calculate slant range to a depth cell
% Assume that c'/c_1 = 1 i.e. that avg. sound speed from transducer to range
% cell is equal to the speed of sound used by the instrument. See Deines.
R = (B +  ((L + D) / 2) +  ((N - 1) * D)+ (D / 4)) * 1 / cos(theta);


if (cols == nbins)
  R2 = ones(rows,1)*R;
%
disp(['just executed an untested bit of code, better check things are ok'])

else
  R2 = (ones(cols,1)*R)';
end

%define the ambient sound matrix
for hh=1:1:rows
Err(hh,:)=Er';
end

% Calculate alpha. Uses the function alpha.m
% Assume constant temperature equal to that 
% of the transducer. Could change this if 
% you have a temperature profile. 307.2 KHz
alph = alpha(307200,Tx,S);

%%%%%



%%%%% CALCULATE CORRECTED BACKSCATTER

% Finally, calculate Sv
Sv = C + 10 * log10((Tx + 273.16) .* R2.^2) - Ldbm - Pdbw + (2 * alph .* R2) + Kc * (backscatter - Err);
end
%%%%%


