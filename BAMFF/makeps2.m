function[] = makeps2(params)
    II1_all = [];
    II2_all = [];
    II3_all = [];
    II4_all = [];
    ts_all = [];
    Ve_all = [];
    Vn_all = [];

    Velx_fish_all = [];
    Vely_fish_all = [];
    Velx_water_all = [];
    Vely_water_all = [];

    titlesize = 20;
    axissize = 16;
    
    load ([params.fileout 'timeseries.mat'])
    ifilenumber = 0;
     number = sprintf('%03i',ifilenumber);
    filename2 = [params.fileout number '.mat'];
    fprintf('MATLAB is loading the data \n')
    load(filename2)
    [ystart,mstart,dstart,hstart,mistart,sstart] = datevec(min(fish.timefish));
    [yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish));
    disp(['Start time: ' num2str(ystart) '/' num2str(mstart) '/' num2str(dstart) ...
           '  ' num2str(hstart) ':' num2str(mistart) ':' num2str(sstart)])
    disp(['End time: ' num2str(yend) '/' num2str(mend) '/' num2str(dend) ...
           '  ' num2str(hend) ':' num2str(miend) ':' num2str(send)])
    starttime = datenum(ystart,mstart,dstart,hstart,mistart,sstart);
    
    endtime = starttime+1;
    endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);
    timestep = 20/60/24;          % 20 minutes
    tt=starttime:timestep:(endtime-timestep);
    scale=0;
    %font_title = 20;
    %changesize_title=title('FontSize',font_title);

    % for ii = 01:30
    %tsname = ['timeseries' int2str(ii)]
    %     load(tsname)

    load ([params.fileout 'timeseries.mat'])
    load ([params.fileresults 'fish_results.mat'])
    load ([params.fileresults 'water_results.mat'])

        II1_all = [II1_all II1];
        II2_all = [II2_all II2];
        II3_all = [II3_all II3];
        II4_all = [II4_all II4];
        ts_all = [ts_all Time_series];

    % *** Magnetic to True North Conversion **********************************
    % Fish(true east): [East(mag)cos17 + North(mag)sin17] i 
    % Fish(true north):  [North(mag)sin17 - East(mag)cos17] j
    %
    % where East(mag) = Vel_x_fish and North(mag) = Vel_y_fish
    %
    %
    %
    % Water(true east): [East(mag)cos17 + North(mag)sin17] i 
    % Water(true north):  [North(mag)sin17 - East(mag)cos17] j
    %
    % where East(mag) = Vel_x_water and North(mag) = Vel_y_water
    %
    % Link to current declination in Vancouver:
    % http://geomag.nrcan.gc.ca/apps/mdcal-eng.php?Year=2009&Month=9&Day=24&Lat
    % =49&Min=15&LatSign=1&Long=123&Min2=7&LongSign=-1&Submit=Calculate+magnetic+declination&CityIndex=83
    % ***************************************************************
    % hh = Vel_x_fish;

    % % true N S E W
    % Vel_x_fish = [-sind(17) cosd(17) sind(17) -cosd(17)];
    % Vel_y_fish = [cosd(17) sind(17) -cosd(17) -sind(17)];
    dec=17; %declination angle(should be 17)

    hh=Vel_x_fish;
    jj=Vel_x_water;

    Vel_x_fish = (Vel_x_fish*(cosd(dec))) + (Vel_y_fish*(sind(dec)));
    Vel_y_fish = (Vel_y_fish*(cosd(dec)) - (hh)*(sind(dec)));

    Vel_x_water = ((Vel_x_water*(cosd(dec))) + (Vel_y_water)*(sind(dec)));
    Vel_y_water = (Vel_y_water*(cosd(dec)) - (jj)*(sind(dec)));




    %     disp('E Vel'); disp(round(Vel_true_east)/10)
    %     disp('N Vel'); disp(round(Vel_true_north)/10) 

%     fishname = [params.fileresults 'fish_results.mat'];
%     load(fishname)
    Velx_fish_all = [Velx_fish_all Vel_x_fish'];
    Vely_fish_all = [Vely_fish_all Vel_y_fish'];

%      watername = [params.fileresults 'water_results.mat'];
%     load(watername)
    Velx_water_all = [Velx_water_all Vel_x_water'];
    Vely_water_all = [Vely_water_all Vel_y_water'];


    [idim ilen] = size(II1_all);
    depth = params.depth_instrument - params.depth_blanking -  ((params.binlength) / 2) - params.binlength * (1:idim) - params.binlength/4;

    % a = input('Year: ');
    % b = input('Month (number): ');
    % c = input('Day: ');


    %starttime = datenum([2012 month2 day2 0 0 0]);

    fix axes
    figure(1)
    clf
    imagesc((ts_all-starttime)*24,depth,flipud(II1_all),[-80 -40])
    depths = [4 98 4];
    mindepth = depths(1);
    maxdepth = depths(2);
    stepdepth = depths(3);
    depth_all = mindepth:stepdepth:maxdepth;
    [adim alin] = size(Velx_water_all);
    trun = (20/60/24 * (1:alin) + dstart);

    set(gca,'FontSize',16)
    set(colorbar,'FontSize',16)
    title('Backscatter Beam 1','FontSize',20)
    xlabel('Hour','FontSize',axissize)
    ylabel('Depth (m)','FontSize',axissize)

    filename = [params.fileresults 'images.ps'];
    print('-f1', '-depsc2', filename);


    figure(2)
    clf
    %imlabel=imagesc(trun,depth_all,Velx_water_all/10,[-50 50]);
    imlabel=imagenans((ts_all-starttime)*24,depth_all,Velx_water_all/10,[-50 50],1);
    set(gca,'FontSize',16);
    set(colorbar,'YTick',[1 32 64],'YTicklabel',{-50,0,50},'FontSize',axissize)
    title('Eastward Water Velocity (cm/s)','FontSize',titlesize)
    xlabel('Hour','FontSize',axissize)
    ylabel('Depth (m)','FontSize',axissize)

    filename = [params.fileresults 'east_water_vel.ps'];
    print('-f2','-depsc2', filename);


    starttime=datenum(ystart,mstart,dstart,hstart,mistart,sstart);



%     figure(8)
%     subplot(211)
%     quiver(((tt-starttime)*24*100),zeros(size(tt)),Velx_water_all(10,:),Vely_water_all(10,:),scale)
%     set(gca,'XTickLabel',{[],'0','5','10','15','20','25'})
%     set(gca,'FontSize',12)
%     xlabel('Hour','FontSize',10)
%     ylabel('Velocity cm/s','FontSize',10)
%     subplot(212)
%     quiver(((tt-starttime)*24*100),zeros(size(tt)),Velx_water_all(20,:),Vely_water_all(20,:),scale)
%     set(gca,'XTickLabel',{[],'0','5','10','15','20','25'})
%     set(gca,'FontSize',12)
%     xlabel('Hour','FontSize',10)
%     ylabel('Velocity cm/s','FontSize',10)
%     filename = [params.fileresults 'Velxywater.ps'];
%     print('-f8','-depsc2',filename);
% 

    %starttime = datenum([2012 month2 day2]);

    figure(3)
    clf
    %imlabel=imagesc(trun,depth_all,Vely_water_all/10,[-50 50]);
    imlabel=imagenans((ts_all-starttime)*24,depth_all,Vely_water_all/10,[-50 50],1);
    set(gca,'FontSize',16);
    set(colorbar,'YTick',[1 32 64],'YTicklabel',{-50,0,50},'FontSize',axissize)
    title('Northward Water Velocity (cm/s)','FontSize',titlesize)
    xlabel('Hour','FontSize',axissize)
    ylabel('Depth (m)','FontSize',axissize)

    filename = [params.fileresults 'north_water_vel.ps'];
    print('-f3','-depsc2', filename);
    % 
    % 
    figure(4)
    clf
    %imagesc(trun,depth_all,Velx_fish_all/10,[-50 50],1)
    imagenans((ts_all-starttime)*24,depth_all,Velx_fish_all/10,[-50 50],1);

    set(gca,'FontSize',16)
    set(colorbar,'YTick',[1 32 64],'YTickLabel',{'-50','0','50'},'FontSize',axissize)
    title('Eastward Fish Velocity (cm/s)','FontSize',titlesize)
    xlabel('Hour','FontSize',axissize)
    ylabel('Depth (m)','FontSize',axissize)

    filename = [params.fileresults '_east_fish_vel.ps'];
    print('-f4','-depsc2', filename);



    figure(5)
    clf
    %imagesc(trun,depth_all,Vely_fish_all/10,[-50 50])
    imagenans((ts_all-starttime)*24,depth_all,Vely_fish_all/10,[-50 50],1);

    set(gca,'FontSize',16);
    set(colorbar,'YTick',[1 32 64],'YTickLabel',{'-50','0','50'},'FontSize',axissize)
    title('Northward Fish Velocity (cm/s)','FontSize',titlesize)
    xlabel('Hour','FontSize',axissize)
    ylabel('Depth (m)','FontSize',axissize)

    filename = [params.fileresults '_north_fish_vel.ps'];
    print('-f5','-depsc2', filename);



    figure(6)
    clf
    %imagesc(trun,depth_all,(Velx_fish_all - Velx_water_all)/10,[-30 30])
    imagenans((ts_all-starttime)*24,depth_all,(Velx_fish_all - Velx_water_all)/10,[-30 30],1);

    set(gca,'FontSize',16);
    set(colorbar,'YTick',[1 32 64],'YTickLabel',{'-30','0','30'},'FontSize',axissize)
    title('Relative Eastward Fish Velocity (cm/s)','FontSize',titlesize)
    xlabel('Hour','FontSize',axissize)
    ylabel('Depth (m)','FontSize',axissize)

    filename = [params.fileresults '_net_eastfish_vel.ps'];
    print('-f6','-depsc2', filename);



    figure(7)
    clf
    %imagesc(trun,depth_all,(Vely_fish_all - Vely_water_all)/10,[-30 30])
    imagenans((ts_all-starttime)*24,depth_all,(Vely_fish_all - Vely_water_all)/10,[-30 30],1);

    set(gca,'FontSize',16)
    set(colorbar,'YTick',[1 32 64],'YTickLabel',{'-30','0','30'},'FontSize',axissize)
    title('Relative Northward Fish Velocity (cm/s)','FontSize',titlesize)
    xlabel('Hour','FontSize',axissize)
    ylabel('Depth (m)','FontSize',axissize)

    filename = [params.fileresults '_net_northfish_vel.ps'];
    print('-f7','-depsc2', filename);

