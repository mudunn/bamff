function params = makeparams(fname)
%*********************************************************************************
% This function makes a structure called params that includes the
% parameters of the deployment. The structure will be saved with the data.
% Open this function by typing "edit makeparams" and enter your values for
% your deployment or survey.
%*********************************************************************************


%% Algorithm Parameters
params.notfish = false;
params.optionback = 1;
params.threshback = -45;
params.optioncorr = 1;
params.threshcorr = 185; 

params.fishthres = 3;
params.savefishfig = 1;              

%% Geometry
params.orientation = 'up';                                


%% Depth
params.lastgoodbin = 60;
params.depth_instrument = 30;
params.depth_blanking = 0.88;
params.binlength = 0.5 * cos(20/180*pi);
params.depthbin = [2 30 1];

params.cellrange = params.depth_blanking + ((params.binlength) / 2) + params.binlength * (1:params.lastgoodbin) + params.binlength/4;


%% Calibration
params.calibrations = [.37 .43 .39 .43];
params.E_min = [44 43 50 40];
params.instrument_cal = -133.5;
params.pdbw =  7;


%% Time
params.timedrift = 0;
params.zerotime = datenum(2019,7,11,14,00,00);
params.timebin = 120/60/24;
    

%% Compass                                       
params.frameangle = 16.5;
params.mag_dev = -16.54;
params.compass_true = params.frameangle+params.mag_dev;
params.compass_mag = params.compass_true; 


%% Filenames and locations             
params.fname = fname;                                   
% initial adcp data from bblist
params.asciidataroot = ['.\ADCPGPJuly2019\converted\raw_ascii\'];
% converts to matlab equivalent
params.rawdataroot = ['.\ADCPGPJuly2019\converted\uncalibrated\'];
% these data are then calibrated for backscatter into
params.fileroot = ['.\ADCPGPJuly2019\converted\calibrated\'];
% final resolved velocities go to
params.fileout = ['.\ADCPGPJuly2019\detected_fish\'];
% Where the not fish data
params.filenotfish = ['.\ADCPGPJuly2019\detected_notfish\'];
% final file be saved
params.fileresults = ['.\ADCPGPJuly2019\results\'];
% final file be saved
params.filefigures = ['.\ADCPGPJuly2019\figures\'];

mkdir(params.asciidataroot)
mkdir(params.rawdataroot)
mkdir(params.fileroot)
mkdir(params.fileout)
mkdir(params.filenotfish)
mkdir(params.fileresults)
mkdir(params.filefigures)

end
