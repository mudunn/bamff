function [xout] = despike(xin,thresh);

%  remove spikes from a data series

%  xin is the input
%  thresh is the maximum allowed jump between values


ddt_xin = diff(xin);

jumps = find(abs(ddt_xin) > thresh);


xout = xin;
xout(jumps+1) = NaN;