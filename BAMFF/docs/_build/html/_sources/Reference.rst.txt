Reference
==================

For more information of fish detection in BAMFF.
Dunn, M., Zedel, L., Trowse, G., "Development of acoustic Doppler fish monitoring for applications in high-energy tidal channels", UACE Proceedings 2019, Greece, 2019.

Zedel, L., & Cyr-Racine, F. Y. (2009). Extracting fish and water velocity from Doppler profiler data. ICES Journal of Marine Science, 66(9), 1846-1852.

Tollefsen, C. D., & Zedel, L. (2003). Evaluation of a Doppler sonar system for fisheries applications. ICES Journal of Marine Science, 60(3), 692-699.


If you found this toolbox useful please cite:
Zedel, L., Dunn, M., Cyr-Racine, F. Y., Cameron, H. BAMFF: Broadband Acoustic Monitoring For Fish",www.bamff.redthedocs.io.
