
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Literature Review &#8212; BAMFF  documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script type="text/javascript" id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/language_data.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="AlgorithmDocumentation" href="AlgorithmDocumentation.html" />
    <link rel="prev" title="Welcome to BAMFF’s documentation!" href="index.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="literature-review">
<h1>Literature Review<a class="headerlink" href="#literature-review" title="Permalink to this headline">¶</a></h1>
<div class="section" id="introduction">
<h2>Introduction<a class="headerlink" href="#introduction" title="Permalink to this headline">¶</a></h2>
<p>Split-beam echosounders are the research standard for fish detection,
they can track individual fish in the instruments ensonified volume.
Tracking the movement between each ping in space gives the velocity of
the tracked fish. This application is commonly used for fisheries
research, however, it has some limitations. The split-beam echosounder
encounters difficulties when it is measuring fish aggregations with a
high density, it does not provide the water velocity relative to the
fish velocity and it is not designed for long-term deployments, which is
crucial in understanding fish movement independently of external
factors.</p>
<p>The limitations above can be partially resolved by using Acoustic
Doppler Current Profilers (ADCPs) to extract fish velocity. If the
instrument and algorithms are properly modified, the ADCP, an instrument
designed for long-term deployments, can extract fish velocity in
reference to water velocity. It can be done in almost any fish
aggregation density regime. The algorithm proposed, Least Squares
algorithm <a class="reference internal" href="#zedel2009" id="id1">[zedel2009]</a>, has not been validated in the
field yet.</p>
<p>Fish velocity in the context of water velocity is especially important
in areas targeted for in-stream tidal energy extraction. In these
sites, ADCPs are already used to measure water velocities but we still
have no definitive research on the effect of tidal turbines on fish.
Therefore, it is a perfect candidate for comparing split-beam and ADCP
methods, advantages and disadvantages for fish velocity extraction.</p>
</div>
<div class="section" id="split-beam-echosounders">
<h2>Split-beam echosounders<a class="headerlink" href="#split-beam-echosounders" title="Permalink to this headline">¶</a></h2>
<p>Transducers transmit a sound pulse which propagates through the water
column away from the face of the transducer. Sound waves are scattered
in all directions by any object with an acoustic impedance different
than the medium the sound is travelling through. Fish swim bladders
are a great target because the acoustic impedance of air and water
differ greatly from each other. Some of the sound waves scattered by
swim bladders and any other target will be sent back in the direction
of the transducer. Most echosounders receive the scattered signal
through the same transducer that transmitted the original signal. In
the most basic single-beam echosounders, fish finders, the sent and
received signal intensities are compared to infer properties about the
target. Many types of echosounders have been developed with increased
complexities to improve on some of the drawbacks of single-beam
echosounding. Multibeam echosounders were designed to decrease the
beam angle while maintaining an efficient sampling volume
<a class="reference internal" href="#gerlotto1999" id="id2">[gerlotto1999]</a> but, despite having a wider beam
width and generally covering a larger volume, they are less sensitive
and tend to have a lower signal to noise ratio
<a class="reference internal" href="#melvin2014" id="id3">[melvin2014]</a>. Dual-beam and split-beam echosounders
were originally designed to improve the single target statistics
<a class="reference internal" href="#maclennan2013" id="id4">[maclennan2013]</a>. However, dual-beam echosounders are
not able to provide the angular location of an echo within the beam
<a class="reference internal" href="#ehrenberg1996" id="id5">[ehrenberg1996]</a>. The rest of this review will focus
on split-beam echosounders because they provide information on
swimming speeds, location in the water column and the direction of
travel of individual fish <a class="reference internal" href="#ehrenberg1996" id="id6">[ehrenberg1996]</a>, all of
which is useful to analyze fish behaviour. Split-beam echosounders
send a single ping through the water column, like a single beam
transducer, but receive the signal through four quadrants that are
analyzed independently. The phase difference between the signal
received by each quadrant locates the target in the beam, as shown in
Figure [fig:splitbeam]. This information is used in the “direct”
method to determine target strength if combined with the on-axis
sensitivity and the beam pattern of the echosounder. For split-beam
echosounders, best results are achieved when the mean density of fish
is low and the target strength distribution width is narrow, that is
when fish tracking is possible by post-processing the data. A low fish
density minimizes the chances of detecting echoes from two or more
fish as a single target, these must be discarded. When a multitude of
species are present, additional processing steps are required to
identify single targets due to the wide distribution of possible
target strength values <a class="reference internal" href="#maclennan2013" id="id7">[maclennan2013]</a>.</p>
<div class="figure">
<img alt="splitbeam" src="splitbeam" />
</div>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name" />
<col class="field-body" />
<tbody valign="top">
<tr class="field-odd field"><th class="field-name">alt:</th><td class="field-body">A split-beam echosounder diagram with its four quadrants and</td>
</tr>
</tbody>
</table>
<p>the phase difference analysis used to locate the fish in the beam.
Taken from: <a class="reference internal" href="#maclennan2013" id="id8">[maclennan2013]</a>.
:width: 50.0%</p>
<p>A split-beam echosounder diagram with its four quadrants and the
phase difference analysis used to locate the fish in the beam. Taken
from: <a class="reference internal" href="#maclennan2013" id="id9">[maclennan2013]</a>.</p>
<p>Ping-to ping target tracking significantly increases the amount of
information provided by a split beam echosounder. First, it identifies
single-target echoes from a single ping and isolates them with the same
technique used for target strength estimation. Second, it determines an
expected next echo for each of the previously accepted single-target
echoes. Third, it looks at the results from the next ping and extracts
single-target echoes, the newly measured single-target echoes that
satisfy an expected echo value are added to the track and used to form
the next expected echo. When an echo does not satisfy the expectation it
begins a new track. The tracks create multiple chains of similar echoes
through time and space. Target tracking provides swim speed data for
single fish targets while it is in the ensonified volume of the
split-beam. The most commonly used advantage of target tracking is that
it provides multiple estimates of the target strength of a single fish
which greatly reduces the variance in the averaged target strength, this
leads to better size estimates and species identification <a class="reference internal" href="#ehrenberg1996" id="id10">[ehrenberg1996]</a>.</p>
<p>Fish tracking requires an in-depth knowledge of fish behaviour <a class="reference internal" href="#xie1997" id="id11">[xie1997]</a> and can be labour intensive if the tracking
algorithm is not specifically designed for the species and location
being measured. When fish aggregation are too dense, single echoes
cannot be isolated and the fish tracking algorithm is unable to create
single fish tracks to extract swim speeds and averaged target strengths.</p>
<p>An experiment by Enzenhofer (1998) compared split-beam echosounder and
video recording to determine the bias of fish numbers from split beam
fish tracking. They found that split-beam fish tracking technology was
over saturated and could not track all the fish when migrating rates
were more than 2000 fish/h, thus having a bias on the echo estimates
during high echo density <a class="reference internal" href="#enzenhofer1998" id="id12">[enzenhofer1998]</a> . In
situations with a high density of fish, like fish school passing
through a strong current, the split beam system and fish tacking
algorithms are limited by the need to isolate individual fish into
single target echoes. But with the echo integration method, the
density of an aggregation of fish can be determined acoustically with
enough knowledge of the medium, the fish and the equipment <a class="reference internal" href="#foote1983" id="id13">[foote1983]</a>.
Other instruments such as the ADCP may be able to complement the limitations of split-beam echosounders.</p>
</div>
<div class="section" id="acoustic-doppler-current-profiler">
<h2>Acoustic Doppler Current Profiler<a class="headerlink" href="#acoustic-doppler-current-profiler" title="Permalink to this headline">¶</a></h2>
<p>Acoustic Doppler Current Profilers (ADCPs) are the standard instrument
for measuring ocean current velocities. As the name says it uses the
Doppler effect, the frequency of sound in water is shifted
proportionally to the speed and direction of the sound source. When
sound is scattered from suspended particles in the water column, they
become the source and allow the ADCP to measure the speed and the
direction of the scatterers. Traditionally, to extract water velocity,
four beams are used to each provide a radial velocity, the result is
average over all the beams within bins to get a 3D vector of the
velocity, as shown in Figure [fig:ADCP], the flow is assumed to be
homogeneous between the beams and within the whole bin. Even though the
principle is the same for all types of Doppler measurements, they vary
in the way they transmit the signal. Narrowband send a long pulse of
acoustic energy with a known frequency and listens for the frequency of
the echo, it compared the send and received frequencies to determine the
along-beam velocity. Broadband systems send two pulses back to back and
the phase difference between their echoes is proportional to the
velocity of the water. Coherent is similar to broadband in that it sends
two pulses but they are shorter and they wait to have received the echo
of the first pulse before sending the second pulse. Again, the phase
shift is analyzed to calculate the velocity. Doppler measurements are
most commonly used for water velocity and for the military, but there
has been some research done towards expanding its capabilities toward
also extracting fish velocity.</p>
<div class="figure">
<img alt="ADCP.png" src="ADCP.png" />
</div>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name" />
<col class="field-body" />
<tbody valign="top">
<tr class="field-odd field"><th class="field-name">alt:</th><td class="field-body">Ozturk, M. Sediment Size Effects in Acoustic Doppler</td>
</tr>
</tbody>
</table>
<p>Velocimeter-Derived Estimates of Suspended Sediment Concentration.
Water 2017, 9, 529.
:width: 80.0%</p>
<p>Ozturk, M. Sediment Size Effects in Acoustic Doppler
Velocimeter-Derived Estimates of Suspended Sediment Concentration.
Water 2017, 9, 529.</p>
<p>In 1977, Holliday said, “…the utilization of measurements involving
the Doppler phenomenon can play a valuable role in the study of fishery
resources” <a class="reference internal" href="#holliday1977" id="id14">[holliday1977]</a>. A lot of research has been
done to improve and optimize ADCP systems for fish detection. Vent
(1976) <a class="reference internal" href="#vent1976" id="id15">[vent1976]</a> has worked on using Doppler
measurement for determining the target strength of fish schools. They
could only resolve radial velocities, so conclude that the interaction
between acoustic energy and fish schools is very complex that is
dependent on the physical and acoustical properties of the observed
school. Then, Olsen and Lovik (1982) <a class="reference internal" href="#olsen1982" id="id16">[olsen1982]</a>
measured velocity estimates of herring and cod as a function of vessel
speed to quantify the fish reaction to a surveying vessel. They compare
video, echosounder and Doppler shift measurements. They find that the
speed of the vessel and the depth of the fish are important in
identifying the magnitude of the velocity of the fish schools. The
limitations of the echosounders are observed when recording groups of
fish that are not sufficiently dispersed to allow for single-target echo
detection. A promising method that could be used in these situations
would be the Doppler shift measurements <a class="reference internal" href="#olsen1982" id="id17">[olsen1982]</a>.</p>
<p>Most research methods before Demers (2000) used low frequency, long
pulses and narrow bandwidths. Though Doppler shift methods were said to,
theoretically, be promising in gathering information for fish behaviour,
the techniques and instruments weren’t developed enough. Demers (2000)
suggested they needed to achieve higher velocity and range resolution
and showed that this can be accomplished using a higher frequency and
autocorrelation for the detection of pulse shifts. Further modifications
are needed to adapt an ADCP system for fish velocity measurements such
as changing modifying parameters and data processing routines, carefully
selecting a survey site with fish schools at least as large as the
ensonifying volume of a bin and measuring velocities of surrounding
water from bins void of fish. This allows the ADCP to measure water
velocities and fish school velocities <a class="reference internal" href="#demer2000" id="id18">[demer2000]</a>.</p>
<p>Further work has been done toward optimizing ADCP measurements for fish
velocity extractions such as an algorithm suggested by Zedel and
Cyr-Racine (2009). Rather than using the data processing algorithm
normally used for extracting water velocity, which requires fish to be
present in at least three acoustic beams, they present an alternative
approach that analyzes each acoustic beam individually to extract both
fish and water velocities, even when fish are intermittently in the
beams and disperse <a class="reference internal" href="#zedel2009" id="id19">[zedel2009]</a>. The main difference is
that with Least Squares algorithm they utilize all the data collected by
the Doppler Profiler rather than only using homogeneous flow data, it is
more computationally involved but provides more information. When all
the data is used the Least Squares algorithm and the conventional
algorithm provide the same velocity results <a class="reference internal" href="#zedel2009" id="id20">[zedel2009]</a>, however, this method needs to be validated as a method to detect fish velocities with an industry
standard for fish detection, such as a split-beam echosounder.</p>
</div>
<div class="section" id="in-stream-tidal-industry">
<h2>In-stream tidal industry<a class="headerlink" href="#in-stream-tidal-industry" title="Permalink to this headline">¶</a></h2>
<p>Our goal is to validate the Least Squared algorithm developed by Dr.
Zedel with a split-beam echosounder to provide a new method and
instrument for fish velocity extraction. There is an opportunity to
contribute to the research of the in-stream tidal energy industry while
accomplishing our goal. Tidal energy converts power from the tides into
electricity by installing turbines in tidal currents that rotate like a
wind turbine in strong winds. In-stream turbine technologies do not
force migrating fish to pass through dams or enclosures so it is
possible that they would have a smaller impact of fish migrations,
however, there is more research required to understand the impact of
tidal turbines on fish migratory patterns and stock. In September 2010,
Melvin and Cochrane set a goal to monitor fish distribution near Minas
Passage at a location proposed for in-stream tidal power energy
conversion devices. To understand the behaviour (or reaction) of fish
when they encounter an underwater structure they completed 6 transects
over a full tidal cycle. The report in 2014 reports that fish are likely
to interact with turbines. Even though the mean density of fish is low,
there is a high density of transient fish in this area <a class="reference internal" href="#melvin2014" id="id21">[melvin2014]</a>.
They make valuable recommendations about instrumentation and deployment,
such as split-beam echosounders are tried and tested to quantify fish but
encounter difficulty with surface turbulence and fish aggregation. They also
recommend a deployment of a
stationary or autonomous bottom-mounted echosounder. First, it removes
vessel noise problems for interference with the acoustic instruments.
Second, it minimizes bubbles, they are a huge problem for acoustic
studied near the ocean surface. Lastly, it lowers cost, it enables a
deployment for several months, ideally a full year cycle without boat
costs, which would provide the complete dataset needed to draw
conclusions on the impact of tidal power development on behaviour and
mortality <a class="reference internal" href="#melvin2014" id="id22">[melvin2014]</a>. Though the recommendations are
very insightful they were unable to make quantified conclusions about
fish behaviour in relation with in-stream tidal turbines.</p>
<p>Viehman’s approach focused on researching how the fish used the water
column in areas with tidal currents strong enough for tidal power
generation. The difficulty of sampling a full tidal cycle in these
strong tidal currents conditions had formed a gap in the literature.
They focused on the vertical distribution of fish before the
installation of a proposed in-stream tidal turbine to find which
populations would be most affected and to eventually compare with
vertical densities after the installation <a class="reference internal" href="#viehman2015" id="id23">[viehman2015]</a>. The paper compares Fish Density Index
for different depths, however, they do not describe how they determined
their ensonified sampling volume required to calculate a density.</p>
<p>For her research Viehman (2015) collected twenty-four hour long
stationary hydroacoustic survey from a moored boat during each season
for two years in Cobscook Bay, Maine, USA
<a class="reference internal" href="#viehman2015" id="id24">[viehman2015]</a>. This data was used by Shen (2016) to
calculate 3 probabilities: the probability of a fish being at the depth
of the turbine when the turbine is absent, the probability a fish would
change its behaviour to avoid the turbine from far away and the
probability of the fish being near the turbine when it is present. They
use a Bayesian Generalized Linear Model to find that a fish is likely to
avoid a device by moving horizontally starting as far as 140m from the
in-stream tidal turbine <a class="reference internal" href="#shen2016" id="id25">[shen2016]</a>. Shen (2016) and
Viehman(2014) used split-beam data for fish detection and ADCP data for
current measurement. If they had used the ADCP capabilities to extract
fish velocities relative to water velocities, the results and analysis
could have been much more robust.</p>
</div>
<div class="section" id="conclusion">
<h2>Conclusion<a class="headerlink" href="#conclusion" title="Permalink to this headline">¶</a></h2>
<p>ADCPs are essential in any in-stream tidal current analysis due to the
need to measure the highly variable current velocities. These
instruments are designed to perform long-term deployments which is a key
factor missing in the literature described above on fish avoidance
analysis in tidal energy sites. With validation of the Least Squares
method proposed by Zedel (2009) <a class="reference internal" href="#zedel2009" id="id26">[zedel2009]</a> fish
behaviour studies could be consistently measured over various days,
tidal cycles and seasons. In the last 5 years, there have been some
research efforts focused towards understanding fish behaviour changes in
the presence of tidal turbines. However, research in the field of fish
velocity extraction using ADCP instruments does not seem to have kept
up, there is a gap in knowledge that could be filled by using the recent
tidal energy site research and ADCP’s capabilities to extract fish and
water velocities even when intermittently distributed in a bin.</p>
<table class="docutils citation" frame="void" id="maclennan2013" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label">[maclennan2013]</td><td><em>(<a class="fn-backref" href="#id4">1</a>, <a class="fn-backref" href="#id7">2</a>, <a class="fn-backref" href="#id8">3</a>, <a class="fn-backref" href="#id9">4</a>)</em> MacLennan, David N., and E. John Simmonds. Fisheries acoustics. Vol. 5. Springer Science &amp; Business Media, 2013.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="ehrenberg1996" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label">[ehrenberg1996]</td><td><em>(<a class="fn-backref" href="#id5">1</a>, <a class="fn-backref" href="#id6">2</a>, <a class="fn-backref" href="#id10">3</a>)</em> Ehrenberg, John E., and Thomas C. Torkelson. “Application of dual-beam and split-beam target tracking in fisheries acoustics.” ICES Journal of Marine Science 53.2 (1996): 329-334.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="melvin2014" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label">[melvin2014]</td><td><em>(<a class="fn-backref" href="#id3">1</a>, <a class="fn-backref" href="#id21">2</a>, <a class="fn-backref" href="#id22">3</a>)</em> Melvin, G. D., and N. A. Cochrane. “Investigation of the vertical distribution, movement and abundance of fish in the vicinity of proposed tidal power energy conversion devices.” Final Report, OEER/OETR Research Project (2013): 3000-170.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="enzenhofer1998" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id12">[enzenhofer1998]</a></td><td>Enzenhofer, Hermann J., Norm Olsen, and Timothy J. Mulligan. “Fixed-location riverine hydroacoustics as a method of enumerating migrating adult Pacific salmon: comparison of split-beam acoustics vs. visual counting.” Aquatic Living Resources 11.2 (1998): 61-74.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="holliday1977" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id14">[holliday1977]</a></td><td>Holliday, D. V. “Two applications of the Doppler effect in the study of fish schools.” ICES J. Mar. Sci 170 (1977): 21-30.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="demer2000" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id18">[demer2000]</a></td><td>Demer, David A., Manuel Barange, and Alan J. Boyd. “Measurements of three-dimensional fish school velocities with an acoustic Doppler current profiler.” Fisheries Research 47.2-3 (2000): 201-214.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="xie1997" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id11">[xie1997]</a></td><td>Xie, Yunbo, George Cronkite, and Timothy James Mulligan. A split-beam echosounder perspective on migratory salmon in the Fraser River: a progress report on the split-beam experiment at Mission, BC, in 1995. Vol. 11. Pacific Salmon Commission, 1997.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="vent1976" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id15">[vent1976]</a></td><td>Vent, R. J., et al. Fish school target strength and Doppler measurements. No. NUC-TP-521. NAVAL UNDERSEA CENTER SAN DIEGO CA, 1976.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="olsen1982" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label">[olsen1982]</td><td><em>(<a class="fn-backref" href="#id16">1</a>, <a class="fn-backref" href="#id17">2</a>)</em> Olsen, K., et al. “Observed fish reactions to a surveying vessel with special reference to herring, cod, capelin and polar cod.” (1983).</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="zedel2009" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label">[zedel2009]</td><td><em>(<a class="fn-backref" href="#id1">1</a>, <a class="fn-backref" href="#id19">2</a>, <a class="fn-backref" href="#id20">3</a>, <a class="fn-backref" href="#id26">4</a>)</em> Zedel, Len, and Francis-Yan Cyr-Racine. “Extracting fish and water velocity from Doppler profiler data.” ICES Journal of Marine Science 66.9 (2009): 1846-1852.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="shen2016" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id25">[shen2016]</a></td><td>Shen, Haixue, et al. “Estimating the probability of fish encountering a marine hydrokinetic device.” Renewable Energy 97 (2016): 746-756.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="viehman2015" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label">[viehman2015]</td><td><em>(<a class="fn-backref" href="#id23">1</a>, <a class="fn-backref" href="#id24">2</a>)</em> Viehman, Haley A., et al. “Using hydroacoustics to understand fish presence and vertical distribution in a tidally dynamic region targeted for energy extraction.” Estuaries and Coasts 38.1 (2015): 215-226.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="gerlotto1999" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id2">[gerlotto1999]</a></td><td>Gerlotto, Francois and Soria, Marc and Freon, Pierre, From two dimensions to three: the use of multibeam sonar for a new approach in fisheries acoustics,Canadian Journal of Fisheries and Aquatic Sciences,56,1,6-12,1999.</td></tr>
</tbody>
</table>
<table class="docutils citation" frame="void" id="foote1983" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id13">[foote1983]</a></td><td>Foote, Kenneth G, Linearity of fisheries acoustics, with addition theorery, The Journal of the Acoustical Society of America,73,6,1932-1940,1983</td></tr>
</tbody>
</table>
</div>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">BAMFF</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Literature Review</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#introduction">Introduction</a></li>
<li class="toctree-l2"><a class="reference internal" href="#split-beam-echosounders">Split-beam echosounders</a></li>
<li class="toctree-l2"><a class="reference internal" href="#acoustic-doppler-current-profiler">Acoustic Doppler Current Profiler</a></li>
<li class="toctree-l2"><a class="reference internal" href="#in-stream-tidal-industry">In-stream tidal industry</a></li>
<li class="toctree-l2"><a class="reference internal" href="#conclusion">Conclusion</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="AlgorithmDocumentation.html">AlgorithmDocumentation</a></li>
<li class="toctree-l1"><a class="reference internal" href="QuickStart.html">Quick Start Guide</a></li>
<li class="toctree-l1"><a class="reference internal" href="Reference.html">Reference</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="index.html" title="previous chapter">Welcome to BAMFF’s documentation!</a></li>
      <li>Next: <a href="AlgorithmDocumentation.html" title="next chapter">AlgorithmDocumentation</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    </div>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2019, Dunn, Zedel.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 1.8.5</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/LitRevtex.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>