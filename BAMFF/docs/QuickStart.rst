Quick Start Guide
=======================

To get started clone the code from the Bitbucket repository.

Open ``makeparams.m`` and fill in the information of your deployment\survey, calibrations and processing preferences.

Run the functions.::
  
    params = makeparams(fname);
    RDItoMAT(params);
    backscatter(params);
    findfish(params);
    binvelocity(params);

Fine tune your processing parameters by changing the values in ``makeparams.m`` and rerun.::
  
    params = makeparams(fname);

When you are happy with your parameters we suggest saving the params structure with you dataset, such as::

    params_GP2018 = makeparams(fname);
    save('parameters_GP2018.mat', '-struct', 'params_GP2018');

Then running the rest of the code with those parameters::
  
    RDItoMAT(params_GP2018);
    backscatter(params_GP2018);
    findfish(params_GP2018);
    binvelocity(params_GP2018);

You can save the fish detection images using::
  
    savefishfigs(params_GP2018);

Or select areas for fish and/or water velocity extraction with::
  
    pickfish(params_GP2018);
