Welcome to BAMFF's documentation!
============================================
Broadband Acoustic Monitoring For Fish (BAMFF) is a MATLAB package that processes raw ADCP data into depth and time-averaged fish and water velocities.

The toolbox converts the RDI files into usable MATLAB structures, then it calibrates and corrects for spherical spreading and absorption. Using a combination of correlation and volume backscatter thresholds, it determines whether signals are from fish or water targets in each beam individually. The targets for all the beams are binned to calculate the average fish velocity and count. It also bins the non-fish targets to extract the water velocity profiles.

The code is kept in a Bitbucket repository and is maintained under version control. Documentation for each user-facing functions and parameters is in this Readthedocs webpage.

We are working on adding plotting and data visualization functions. 
This page contains documentation for BAMFF package, a literature review, a quickstart guide and references.
    

    
Contents
^^^^^^^^
.. toctree::
   :maxdepth: 2
	      
   LitRevtex
   AlgorithmDocumentation
   QuickStart
   Reference



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
