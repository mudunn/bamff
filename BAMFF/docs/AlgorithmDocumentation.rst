AlgorithmDocumentation
=================================================================
makeparams.m
----------------
A function that will create a structure containing the filename, survey or deployment parameters and processing preferences. It is saved with the data for future acces and reproducibility.::
  
    edit makeparams
    % change entries in makeparams
    params = makeparams(filename);

Creating this structure is the first step for this toolbox, the "params" structure is a input to all the next processing steps.

Here we will go through each variable to establish what it means and suggested values.

Algorithm Parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
notfish
  true: the algorithm (findfish.m) will run a second time to isolate all "notfish" targets (generally water). false: only isolates fish targets.
  
optionback
  1, for backscatter detection; 0 for no backscatter detection. Backscatter detection uses backscatter as a threshold, set in "thresback", to isolate fish targets.

thresback
  Detection threshold value for volume backscatter (in dB). In the "findfish" function, returns greater than the thresback value will be candidates for fish targets, lower values will be candidates for notfish values.

optioncorr
  1, for corelation detection, 0 for no correlation detection. Correlation detection uses the correlation of each sample as a threshold, set in threscorr, to isolate targets.

threscorr
  Detection threshold value fish correlation (in counts). In the "findfish" function, returns that are greater than the threscorr value will be candidates for fish targets, lower values will be considered notfish.

fishthres
  Minimum amount of fish necessary to calculare a meaningful velocity.

Geometry
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
orientation
  "up", upward looking geometry. "down", downlooking geometry.


Depth
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
lastgoodbin
  Useful if you do not what to use all the recorded bin, for stripping the bottom or surface off.

depth_instrument
  Instrument depth, in meters.

depth_blanking
  The blanking depth translates the amount of time required for the instrument to stop transmitting and start listening into a depth. Can be found in adcp.config structure.

binlength
  The size of the bin in vertical depth(m). It uses the bin range value, can be found in the adcp.config structure, and multiplies by cos(20/180*pi).

bindepth
  Values for binning the depth in the "binvelocity" function. [mindepth maxdepth step]

cellrange
  Calculates the center of the range of each cell, distance from instrument, based on depth blanking and bin length


Calibration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
calibrations
  Values from the calibration performed on the instrument. Beams [1 2 3 4].

E_min
  The noise floor for each beam, minimum value observed.

instrument_cal
  System scaling for RDI table.

pdbw
  Transmit power.

Time
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
timedrift
  Drift over deployment(s)/totaltime(s)*180??

zerotime
  Time instrument was started, in MATLAB datenum.

timebin
  Values for binning the time in "binvelocity" function. [mintime maxtime step]

Compass
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
frameangle
  The frame angle during the deployment, can use mean "heading".

compass_true
  frameangle-135, where is 135 from?

compass_mag
  compass_true-17, where is 17 from?


Filenames
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
fname
  The filename containing the raw data from the RDI ADCP.

rawdataroot
  File location for the structures created by RDItoMAT.

fileroot
  Contains files that are calibrated for backscatter by "backscatter"

fileout
  Contains file "timeseries.mat" file with all beam data together and velocities of the isolated fish targets. As well as a file for each ensemble of fish detections.

filenotfish
  Contains the files related to the notfish data samples, all beam data together and velocities.

fileresults
  File file to be saved. 





RDItoMAT.m
------------
Originially names bblist_not, after the BBLIST function in the DOS program made available by RDI. The program takes the binary file created by the ADCP and converts it into to a ASCII-text format. The function "RDItoMAT", breaks up the binary file created by the ADCP into chunks and converts them to a .mat format table.:
Note: Fills missing temporal data with NaN to have a resulting data set with no temporal gaps






backscatter.m
--------------
Converts each file into backscatter values. Runs each file through fix_backscatter which adjusts for spherical dispersion of signal and attenuation. Uses the calibration coefficients set in params.calibrations.






findfish.m
-------------
Function to find fish, determines targets in the backscatter through correlation or backscatter threshold, set in "makeparams". If notfish is true it will also calcuate and isolate the targets that do not quality as fish targets.




binvelocity.m
--------------
Opens the files created by findfish in the fileout or filenotfish folders. It binsthe velocity of each folder by depth, "params.bindepth", and time, "params.bintime". It then appends all the binned velocities in each folder in chronological order and creates two files, if "params.notfish"=true, one for fish x and y velocities and one for notfish x and y velocities, in the results folder.


pickfish.m
---------------
