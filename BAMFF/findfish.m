function [] = findfish(params)
%Function to find fish, determines targets in the backscatter through
%correlation or backscatter threshold, set in makeparams. If notfish is
%true it will run again for "not fish" aka water.

%% Allocate scape      
Ve = [];
Vn = [];
Time_series = [];
II1 = [];
II2 = [];
II3 = [];
II4 = [];


%% Beam Geometry
switch params.orientation
    case 'up'
        beams = [sin(20/180*pi) 0 cos(20/180*pi); ...  % beam vectors
              -sin(20/180*pi) 0 cos(20/180*pi); ...
               0 sin(20/180*pi) cos(20/180*pi); ...
               0 -sin(20/180*pi) cos(20/180*pi)];

    case 'down'
        beams = [-sin(20/180*pi) 0 -cos(20/180*pi); ...  % beam vectors
              sin(20/180*pi) 0 -cos(20/180*pi); ...
               0 sin(20/180*pi) -cos(20/180*pi); ...
               0 -sin(20/180*pi) -cos(20/180*pi)];   
end

%% Parameters for reading file
filecount = countfiles(params.fileroot);
overallfiles = 0:filecount-1;

    
for in = overallfiles
%% Open file (runs through each file one at a time)
    number = sprintf('%03i',in);
    filename = [params.fileroot number '.mat'];
    load(filename);
    fprintf('MATLAB is now processing file number %g \n\n',in)

    % Allocate space to variables
    Ensefish = [];
    Direcfish = [];
    Timefish = [];
    Ifish = [];
    Vfish = [];
    Cfish = [];
    Depthfish = [];
    Rolllfish = [];
    Ptchfish = [];
    Compassfish = [];
    Beamnumfish = [];
    Beamvector = [];
    
    if params.notfish
        Ensenotfish = [];
        Direcnotfish = [];
        Timenotfish = [];
        Inotfish = [];
        Vnotfish = [];
        Cnotfish = [];
        Depthnotfish = [];
        Rolllnotfish = [];
        Ptchnotfish = [];
        Compassnotfish = [];
        Beamnumnotfish = [];
        Beamvectornot = [];
    end


    if strcmpi(params.orientation,'up')
        rolll = -1 * rolll;    % correction for adcp coordinate, see page
                     % 15 of RDI's manual on coordinate
    end

    time_matlab = time;       

    Time_series = [Time_series time];
    Ve = [Ve ve];
    Vn = [Vn vn];
    II1 = [II1 I1];
    II2 = [II2 I2];
    II3 = [II3 I3];
    II4 = [II4 I4];
    

    %Detect the fish using the intensity of the backscatter
    ensefish = []; 
    direcfish = []; 
    ifish = [];
    vfish = [];
    cfish = [];
    depthfish = [];
    rolllfish = [];
    ptchfish = [];
    compassfish = [];
    beamnumfish = [];
    timefish = [];
    beamvector = [];
    
    if params.notfish
        ensenotfish = []; 
        direcnotfish = []; 
        inotfish = [];
        vnotfish = [];
        cnotfish = [];
        depthnotfish = [];
        rolllnotfish = [];
        ptchnotfish = [];
        compassnotfish = [];
        beamnumnotfish = [];
        timenotfish = [];
        beamvectornot = [];
    end
            
    [ibin, ~] = size(II1);
    switch params.orientation
        case 'down'
            run = 1:params.lastgoodbin;
        case 'up'
            run = ((ibin - params.lastgoodbin)+1):ibin;
    end

    %% Find the surface/bottom based on backscatter levels in this file.
    meanamp = movmean(I4,500,2,'omitnan','endpoints','shrink');
    [~,surfacebin] = max(meanamp);
    %surfacebins = min(botts);  % surface is distance to surface in m.
                                               % watch out, fish depth is
                                               % just that, depth from an
                                               % imaginary surface 30 m
                                               % from instrument

    
%% Loop through each ADCP beam        
    for ibeam = 1:4      % loop through four adcp beams
        eval(['I = I' num2str(ibeam) '(run,:);']);
        eval(['c = c' num2str(ibeam) '(run,:);']);
        eval(['v = v' num2str(ibeam) '(run,:);']);


        if(params.optionback==1 && params.optioncorr==0)
            fish=find(I>params.threshback);
            notfish=find(I<params.threshback);
        elseif(params.optionback==0 && params.optioncorr==1)
            fish=find(c>params.threshcorr);
            notfish=find(c<params.threshcorr);
        elseif(params.optionback==1 && params.optioncorr==1)
            Io=I>params.threshback;
            co=c>params.threshcorr;
            fish=find(Io.*co);

            notfish=find(~(Io.*co));

        end
        
%% Check for fish above surface and below nearfield remove those hits.
        [fishr, fishc] = ind2sub(size(I),fish);
        fish = fish(fishr<(surfacebin(fishc)-2)' & fishr>2); %Fish must be between surface and nearfield exclusion lines
        

%% Find the values for the fish.
        ifish=[ifish I(fish)'];
        vfish=[vfish v(fish)'];
        cfish=[cfish c(fish)'];
        %Define the depth of the fish
        %I will need to know the orientation of the beams to find depth and subsequently to find speed
        if size(fish,1) ~= 0 & ~isempty(time)
            beamvector = [beamvector (ones(size(fish))*beams(ibeam,:))'];
        else
            beamvector = beamvector;
        end
        
        % find the corresponding row and column for this fish        
        [fishr, fishc] = ind2sub(size(I),fish);                                                                         
        depthfish = [depthfish fishr'];
        
        %Define the heading of the ADCP, roll, pitch, time ensemble number and beamnum 
        compassfish=[compassfish hdg(fishc)];
        rolllfish=[rolllfish rolll(fishc)];
        ptchfish=[ptchfish ptch(fishc)];
        timefish = [timefish time_matlab(fishc)];
        ensefish=[ensefish ensemble(fishc)];
        beamnumfish=[beamnumfish ibeam*ones(size(hdg(fishc)))];
        
        
        if params.notfish
            inotfish = [inotfish I(notfish)'];            
            vnotfish = [vnotfish v(notfish)'];
            cnotfish = [cnotfish c(notfish)'];
            beamvectornot = [beamvectornot (ones(size(notfish))*beams(ibeam,:))'];
            
            [notfishr, notfishc] = ind2sub(size(I),notfish);
            
            depthnotfish = [depthnotfish notfishr'];
            %Define the heading of the ADCP, roll, pitch, time ensemble number and beamnum 
            compassnotfish=[compassnotfish hdg(notfishc)];
            rolllnotfish=[rolllnotfish rolll(notfishc)];
            ptchnotfish=[ptchnotfish ptch(notfishc)];
            timenotfish = [timenotfish time_matlab(notfishc)];
            ensenotfish=[ensenotfish ensemble(notfishc)];
            beamnumnotfish=[beamnumnotfish ibeam*ones(size(hdg(notfishc)))];
        end
    end    % of loop through beams
    
    if params.savefishfig && ~isempty(time)
        savefishfig(params, surfacebin, timefish, params.cellrange(depthfish), time_matlab, I, c)
    end
    
%% Do some pre-processing of the data
%Sort them such that the final data will be an increasing time series. Use the ensemble number
    [ensefish,index]=sort(ensefish);
    ifish=ifish(index);
    vfish=vfish(index);
    cfish=cfish(index);
    depthfish=depthfish(index);
    compassfish=compassfish(index);
    rolllfish=rolllfish(index);
    ptchfish=ptchfish(index);
    beamnumfish=beamnumfish(index);
    timefish = timefish(index);
    beamvector = beamvector(:,index);
    
    if params.notfish
        [ensenotfish,notindex]=sort(ensenotfish);
        inotfish=inotfish(notindex);
        vnotfish=vnotfish(notindex);
        cnotfish=cnotfish(notindex);
        depthnotfish=depthnotfish(notindex);
        compassnotfish=compassnotfish(notindex);
        rolllnotfish=rolllnotfish(notindex);
        ptchnotfish=ptchnotfish(notindex);
        beamnumnotfish=beamnumnotfish(notindex);
        timenotfish = timenotfish(notindex);
        beamvectornot = beamvectornot(:,notindex);
    end

    %From the beam number and the heading, find the direction of the
    %beam.
    switch params.orientation
        case 'up'
            headfactor = [90 270 0 180];
        case 'down'
            headfactor = [270 90 0 180];
    end

%% Keep fish angles between 0 and 360.
    direcfish = compassfish + headfactor(beamnumfish);
     highs = find(direcfish >= 360);
     while ~isempty(highs)
         direcfish(highs)=direcfish(highs)-360; 
         highs = find(direcfish >= 360);
     end
     
     if params.notfish
         direcnotfish = compassnotfish + headfactor(beamnumnotfish);
         highsnot = find(direcnotfish >= 360);
         while ~isempty(highsnot)
             direcnotfish(highsnot)=direcnotfish(highsnot)-360; 
             highsnot = find(direcnotfish >= 360);
         end
     end


%% Append the data to the file that covers the whole deployment
    Direcfish=[Direcfish direcfish];
    Timefish = [Timefish timefish];
    Compassfish = [Compassfish compassfish];
    Beamnumfish = [Beamnumfish beamnumfish];
    Ifish=[Ifish ifish];
    Vfish=[Vfish vfish];
    Cfish=[Cfish cfish];
    Depthfish=[Depthfish depthfish];
    Rolllfish=[Rolllfish rolllfish];
    Ptchfish=[Ptchfish ptchfish];
    Ensefish=[Ensefish ensefish];
    Beamvector = [Beamvector beamvector];
    clear('ifish','vfish','cfish','depthfish','direcfish','ptchfish','ensefish','rolllfish','compassfish','beamnumfish')

    
    if params.notfish
        Direcnotfish=[Direcnotfish direcnotfish];
        Timenotfish = [Timenotfish timenotfish];
        Compassnotfish = [Compassnotfish compassnotfish];
        Beamnumnotfish = [Beamnumnotfish beamnumnotfish];
        Inotfish=[Inotfish inotfish];
        Vnotfish=[Vnotfish vnotfish];
        Cnotfish=[Cnotfish cnotfish];
        Depthnotfish=[Depthnotfish depthnotfish];
        Rolllnotfish=[Rolllnotfish rolllnotfish];
        Ptchnotfish=[Ptchnotfish ptchnotfish];
        Ensenotfish=[Ensenotfish ensenotfish];
        Beamvectornot = [Beamvectornot beamvectornot];
        clear('inotfish','vnotfish','cnotfish','depthnotfish','direcnotfish','ptchnotfish','ensenotfish','rolllnotfish','compassnotfish','beamnumnotfish')
    end

    
    
%% make Beamvector_rot to position fish at depth (should make a subfunction)
    Beamvector_rot = Beamvector;
    for i = 1:length(Ifish)
        D = [cos(Compassfish(i)/180*pi) sin(Compassfish(i)/180*pi) 0; ...
            -sin(Compassfish(i)/180*pi) cos(Compassfish(i)/180*pi) 0; ...
             0 0 1];

        C = [1 0 0;0 cos(Ptchfish(i)/180*pi) sin(Ptchfish(i)/180*pi); ...
                  0 -sin(Ptchfish(i)/180*pi) cos(Ptchfish(i)/180*pi)];

        B = [cos(Rolllfish(i)/180*pi) 0 sin(Rolllfish(i)/180*pi); 0 1 0; ...
            -sin(Rolllfish(i)/180*pi) 0 cos(Rolllfish(i)/180*pi)];

        A = D * C * B;

        Beamvector_rot(:,i) = A * Beamvector(:,i);
    end

    
    if params.notfish
        Beamvectornot_rot = Beamvectornot;
        for i = 1:length(Inotfish)
            Dnot = [cos(Compassnotfish(i)/180*pi) sin(Compassnotfish(i)/180*pi) 0; ...
                -sin(Compassnotfish(i)/180*pi) cos(Compassnotfish(i)/180*pi) 0; ...
                 0 0 1];

            Cnot = [1 0 0;0 cos(Ptchnotfish(i)/180*pi) sin(Ptchnotfish(i)/180*pi); ...
                      0 -sin(Ptchnotfish(i)/180*pi) cos(Ptchnotfish(i)/180*pi)];

            Bnot = [cos(Rolllnotfish(i)/180*pi) 0 sin(Rolllnotfish(i)/180*pi); 0 1 0; ...
                -sin(Rolllnotfish(i)/180*pi) 0 cos(Rolllnotfish(i)/180*pi)];

            Anot = Dnot * Cnot * Bnot;

            Beamvectornot_rot(:,i) = Anot * Beamvectornot(:,i);
        end
    end


    %
    %  next step is to use Beamvector_rot to position fish at depth
    %


    L = params.binlength / cos(20*pi/180);

    %  flipped this over ie changed 'do' to 'up' for Rock channel ...
    % for some reason the adcp stored the data in backward sort ....
    %  not happy about this. 
    %  in data I'd processed, rdi would stack data top to bottom in depth
    %  no matter the orientation of the instrument ....
    %


    %disp(['keep an eye, changed code here but seemed strange'])
    switch params.orientation
        case 'up'
            range = (params.depth_blanking + ((L + params.binlength) / 2) + ...
                 ((Depthfish - 1) * params.binlength) + (params.binlength/4)) / cos(20*pi/180);
            if params.notfish
               rangenot = (params.depth_blanking + ((L + params.binlength) / 2) + ...
                    ((Depthnotfish - 1) * params.binlength) + (params.binlength/4)) / cos(20*pi/180);
                Depthofnotfish = - Beamvectornot_rot(3,:) .* rangenot + params.depth_instrument;
            end
        case 'down'
            range = (params.depth_blanking + ((L + params.binlength) / 2) + ...
                 ((params.lastgoodbin - Depthfish) * params.binlength) + (params.binlength/4)) / cos(20*pi/180);
            if params.notfish
                rangenot = (params.depth_blanking + ((L + params.binlength) / 2) + ...
                 ((params.lastgoodbin - Depthnotfish) * params.binlength) + (params.binlength/4)) / cos(20*pi/180);
                Depthofnotfish = - Beamvectornot_rot(3,:) .* rangenot + params.depth_instrument;
            end
    end
    
     if isempty(Beamvector_rot)
         Depthoffish=Depthfish;
     else
        Depthoffish =  Beamvector_rot(3,:) .* range;% + params.depth_instrument;
     end
    
   
%% Save in a structure
    
    fprintf('MATLAB is building the structure containing the data \n')
    fprintf('Number of fish found %g \n',length(Ifish))
    number = sprintf('%03i',in);
    fish=struct('ensefish',Ensefish,'direcfish',Direcfish,...
                'ifish',Ifish,'vfish',Vfish,...
                'cfish',Cfish,'depthoffish',Depthoffish,...
                'rolllfish',Rolllfish,'ptchfish',Ptchfish,'compassfish',Compassfish,...
                'beamnumfish',Beamnumfish,'timefish',Timefish,'beamvector',Beamvector_rot,'surface',surfacebin);
    save([params.fileout number],'fish')
    clear fish
    
    if params.notfish
        fprintf('Number of notfish found %g \n',length(Inotfish))
        fish=struct('ensefish',Ensenotfish,'direcfish',Direcnotfish,...
                    'ifish',Inotfish,'vfish',Vnotfish,...
                    'cfish',Cnotfish,'depthoffish',Depthofnotfish,...
                    'rolllfish',Rolllnotfish,'ptchfish',Ptchnotfish,'compassfish',Compassnotfish,...
                    'beamnumfish',Beamnumnotfish,'timefish',Timenotfish,'beamvector',Beamvectornot_rot);
        save([params.filenotfish number],'fish')
    end
end
    try
        save([params.fileout 'timeseries.mat'],'Vn','Ve','Time_series');
    catch
        print('Save to timeseries.mat did not work')
        size(Time_series)
    end
end





%% Nested function
%------------------------------------------------------------------------------------------------------
function savefishfig(params, surfacebin, time, depth, time_matlab, I, corr)
%********************************************************************************
% This function will make plot to show the fish detectability of intensity
% and correlation to facilitate fine tuning values to use for the threscorr and
% thresback parameters.
%********************************************************************************
surfacedepth = params.cellrange(surfacebin);
clf
figure(1)
ax1 = subplot(211);
imagesc(time_matlab, params.cellrange,I)
colormap('parula');
hold on
plot(time,depth,'k.', 'MarkerSize', 10)
plot(time_matlab,surfacedepth, 'k');
c = colorbar;
c.Label.String = 'Volume Backscatter [dB]';
datetick('x')
set(gca,'YDir','normal') 
title('Fish detetcted using correlation and backscatter thresholds')
if ~isempty(time_matlab)
    xlabel(sprintf('Time on %s', datestr(time_matlab(1),1)))
end
ylabel('Range [m]')

ax2 = subplot(212);
imagesc(time_matlab, params.cellrange, corr/256, [0 1])
colormap(ax2,flipud(pink));
hold on
plot(time,depth,'r.', 'MarkerSize', 10)
plot(time_matlab,surfacedepth, 'r')
c = colorbar;
c.Label.String = 'Correlation [Counts]';
datetick('x')
set(gca,'YDir','normal') 
title('Fish detetcted correlation')
if ~isempty(time_matlab)
    xlabel(sprintf('Time on %s', datestr(time_matlab(1),1)))
end
ylabel('Range [m]')
linkaxes([ax1 ax2],['x' 'y']);

%% Save
dim = [.2 .4 .5 .5];
str = sprintf('threshback:%3.0f, threshcorr:%1.2f', params.threshback, params.threshcorr/256);
annotation('textbox',dim,'String',str,'FitBoxToText','on', 'Color', 'w');
figure(1);
saveas(gcf,[params.filefigures '/' sprintf( '%s-findfish.png',datestr(time_matlab(1),'yyyy-mm-dd-HH'))]);

end

