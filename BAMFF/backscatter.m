function [] = backscatter(params)

%******************************************************
%Converts each file into backscatter values. 
%Runs each file through fix_backscatter which adjusts for spherical dispersion of signal and attenuation. 
%Uses the calibration coefficients set in params.calibrations.
%******************************************************************

%% Set up for loading in one file at a time.
step=1;
start=0;
in=0;

existingmatfile = 2;
while existingmatfile == 2
    number = sprintf('%03i',in);
    filename = [params.rawdataroot number '.mat'];
    load(filename);    

    %Extract the mean temperature of this file
    try
        Tx=nanmean(temp);
    catch
        Tx = nanmean(adcp.temperature);
    end

    %Define the size of the data
    [row,colu]=size(I1);
    
    %Define the number of bins
    nbin=row;
    
  
%% Set up and run fix_backscatter for spherical dispersion of signal
    D = params.binlength;
    B = params.depth_blanking;
    L = params.binlength / cos(20/180*pi);
    S = 32;
    C = params.instrument_cal;
    Kcd = params.calibrations;
    Pdbw = params.pdbw;

    I1=fix_backscatter(I1, Tx, D, B, L, S, C, Kcd(1),...
                  Pdbw, params.E_min(1)*ones(colu,1), nbin);
    I2=fix_backscatter(I2, Tx, D, B, L, S, C, Kcd(2),...
                  Pdbw, params.E_min(2)*ones(colu,1), nbin);
    I3=fix_backscatter(I3, Tx, D, B, L, S, C, Kcd(3),...
                  Pdbw, params.E_min(3)*ones(colu,1), nbin);          
    I4=fix_backscatter(I4, Tx, D, B, L, S, C, Kcd(4),...
                  Pdbw, params.E_min(4)*ones(colu,1), nbin);          

%% Total intensity of the four beams 
    I = (I1+I2+I3+I4)./4;

%% Save processed data
    filename = [params.fileroot number '.mat'];
    disp([filename])
    save(filename,'I','ensemble','temp','year','month','day','time','ve','vn','ver','verr','v1','v2','v3','v4','rolll','ptch','hdg','I1','I2','I3','I4','bad1','bad2','bad3','bad4','c1','c2','c3','c4')

    %Clear the data to make room for the next data set
    clear('I','ensemble','temp','year','month','day','time','ve','vn','ver','verr','v1','v2','v3','v4','rolll','ptch','hdg','I1','I2','I3','I4','bad1','bad2','bad3','bad4','c1','c2','c3','c4')

%% Check if next file exists
    existingmatfile=exist([params.rawdataroot sprintf('%03i',in+1) '.mat'],'file');
    if existingmatfile == 2
        in=in+1;
    end
    
end
end  %for each file
