function[] = RDItoMAT(params)

%******************************************************************************
% This function takes data from RDI data files and converts them into 
% a working format in matlab.  The process appears as an additional step but
% was introduced to make the RDI data compatible with a previous generations
% of software. It uses the params structure made by makeparams that
% contains the ADCP deployment setting and details.
%**************************************************************************

%%
valueensd=[];     %Define parameters that will be used later to correct the bin number
valueendu=[];

ifile = params.fname;

%Define the step between the records
start = 0;
step=4168; %UNITS?? MB??   % crazy number but matches what we've been using for other reasons.


%% Split the data into chunks
fid=fopen(ifile,'r','ieee-le');
ch=uint8(fread(fid,100000,'uint8'));
fclose(fid);
in0 = find(ch(1:end-1)==127 & ch(2:end)==127); % find the "7F7F" header start
din0=diff(in0);
ens=max(din0); % estimate of ensemble size
disp(['Estimated ensemble size: ',num2str(ens)]);
fileinfo=dir(ifile);
finish = floor(fileinfo.bytes/ens);

%% Load chunks them one at a time.
%Define the number of the file with the proper format.
infilenum = 0;
for in = start:step:finish
   
    
    % function to translate from RDI to Matlab struct.
    [rdidata, rdiset] = vrdadcp(ifile,[in in+step-1]);
    disp([ifile])
    
    %The velocity along the 4 beams
    v1 = squeeze(rdidata.vel(1,:,:))*1000;
    v2 = squeeze(rdidata.vel(2,:,:))*1000;
    v3 = squeeze(rdidata.vel(3,:,:))*1000;
    v4 = squeeze(rdidata.vel(4,:,:))*1000;

    vn = (v3 - v1)./2./sin(20/180*pi);
    ve = (v2 - v4)./2./sin(20/180*pi);
    verr = (v1 + v2 - v3 - v4)./4;
    ver = (v1 + v2 + v3 + v4)./4./cos(20/180*pi);

    
    %The correlation coefficients
    c1 = squeeze(rdidata.corr(1,:,:));
    c2 = squeeze(rdidata.corr(2,:,:));
    c3 = squeeze(rdidata.corr(3,:,:));
    c4 = squeeze(rdidata.corr(4,:,:));

    
    %The intensity of the four beams
    I1 = squeeze(rdidata.intens(1,:,:));
    I2 = squeeze(rdidata.intens(2,:,:));
    I3 = squeeze(rdidata.intens(3,:,:));
    I4 = squeeze(rdidata.intens(4,:,:));

    
    % For down facing orientation the velocities need to be flipped. (Check
    % this with down facing data (Feb 2019))
    if strcmpi(params.orientation,'down')
        for ibeam = 1:4      % loop through four adcp beams
            eval(['v' num2str(ibeam) '= flipud(v' num2str(ibeam) ');']);
            eval(['c' num2str(ibeam) '= flipud(c' num2str(ibeam) ');']);
            eval(['I' num2str(ibeam) '= flipud(I' num2str(ibeam) ');']);
        end
    end
    

    %The total intensity of the four beams 
    I = (I1+I2+I3+I4)./4;

    %In the file, a the number of bins is nbin (nbin lines of data for each measurements). Thus, for each block 
    %of nbin elements, there is a set of parameters that stay the same:

    ensemble = rdidata.ens;
    
    [year, month, day, hour, minute, seconde] = datevec(rdidata.mtime);
    time=hour+minute./60+seconde./3600;
    time = datenum(year,month,day,hour,minute,seconde);
    time = time+(time-params.zerotime)*params.timedrift;        
    [year, month, day, hour, minute, seconde] = datevec(time);

    %The water temperature
    temp = rdidata.temperature;

    %The heading (in degrees)
    hdg = rdidata.heading;

    %The pitch and the rolll motion of the ADCP (in degrees)
    rolll = rdidata.pitch;
    ptch = rdidata.roll;


    bad1 = find(v1 > 5000);
    v1(bad1) = zeros(size(bad1));

    bad2 = find(v2 > 5000);
    v2(bad2) = zeros(size(bad2));

    bad3 = find(v3 > 5000);
    v3(bad3) = zeros(size(bad3));

    bad4 = find(v4 > 5000);
    v4(bad4) = zeros(size(bad4));
    %We will save those bad array so that we know how many points failed the
    %test and this gives us an idea of how good is the data.

    
    % Save the data
    number = sprintf('%03i',infilenum);
    infilenum = infilenum + 1;

    filename3 = [params.rawdataroot number '.mat'];
    save(filename3,'I','ensemble','time','temp','year','month','day','hour','minute','seconde','ve','vn','ver','verr','v1','v2','v3','v4','rolll','ptch','hdg','I1','I2','I3','I4','bad1','bad2','bad3','bad4','c1','c2','c3','c4')

    %Clear the data to make room for the next data set
    clear('I','ensemble','temp','year','month','day','hour','minute','seconde','ve','vn','ver','verr','v1','v2','v3','v4','rolll','ptch','hdg','I1','I2','I3','I4','bad1','bad2','bad3','bad4','c1','c2','c3','c4')


end  %for each file
end

%% Last updated
% 05 Feb 2019
    
