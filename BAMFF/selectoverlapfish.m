function fish = selectoverlapfish(params, fish)
% Can be used to find fish detections that overlap with another instrument.
% Schedule described in selectoverlap.m

[~, fish.ensefish] = selectoverlap(params, fish.timefish, fish.ensefish);
[~, fish.depthoffish] = selectoverlap(params, fish.timefish, fish.depthoffish);
[~, fish.vfish] = selectoverlap(params, fish.timefish, fish.vfish);
[fish.timefish, fish.ifish] = selectoverlap(params, fish.timefish, fish.ifish);

end

    