


function [imhandle] = imagenans(xvals,yvals,data,limits,docolorbar)

minval=limits(1); 
maxval=limits(2); 

rgb = [];
colormap(jet)

Jet = jet;

[idim ilen] = size(data);

for id = 1:idim
    for il = 1:ilen
        index = floor(((data(id,il)-minval)/(maxval-minval))*64) + 1;

        if (index > 64)
            index = 64;
        end
        if (index < 1)
            index = 1;
        end
        
        if isnan(index)
            rgb(id,il,:) = [1 1 1];
        else
            rgb(id,il,:) = Jet(index,:);
        end
    end
end

if docolorbar == 1
 set(colorbar,'YTick',[1 32 64],'YTickLabel',{minval,0,maxval},'FontSize',16)
end

imhandle = image(xvals,yvals,rgb);