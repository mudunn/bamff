%**************************************************************************
%This program will analyse the velocity profile of the fish as a function
%of time and depth intervals
%**************************************************************************
 
fontsize = 12;
load ([params.fileout 'timeseries.mat'])

% Load notfish file
ifilenumber = 0;
number = sprintf('%03i',ifilenumber);
filename3 = [params.filenotfish number '.mat'];
load(filename3)
notfish = fish;  notparams = params;


%% Load binned water velocities and correct for declination
waterprofile = [params.fileresults '.\water_results.mat'];
load(waterprofile)
% Should we keep this?
% correction for declination (or any other compass nonsense) is 16 but appears it should be 0, correction is taken care of in processing software.
Vel_x_water = Vel_x_water * cosd(params.mag_dev) + Vel_y_water * sind(params.mag_dev);
Vel_y_water = -Vel_x_water * sind(params.mag_dev) + Vel_y_water * cosd(params.mag_dev);


mindepth = params.depthbin(1);
maxdepth = params.depthbin(2);
stepdepth = params.depthbin(3);
depth_all = mindepth:stepdepth:maxdepth;


%  these will be loaded later

number = sprintf('%03i',ifilenumber);
filename2 = [params.fileout number '.mat'];
load(filename2)


[ystart,mstart,dstart,hstart,mistart,sstart] = datevec(min(fish.timefish)); 
[yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish)); 
starttime_thisfile = datenum(ystart,mstart,dstart,hstart,mistart,sstart);
endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);

timestep = 20/60/24; %30 minutes


starttime_window = datenum(ystart,mstart,dstart,hstart,0,0);  % start on the hour
starttimeday = datenum(ystart,mstart,dstart,0,0,0);
endtime_day = datenum(ystart,mstart,dstart,0,0,0) + 4;
endtime_window = starttime_window + timestep;



while (endtime_window < endtime_day)
    fprintf('MATLAB is loading the data \n')
    Vel_x = [];
    Vel_y = [];
    Vel_z = [];

    Sig_vx = [];
    Sig_vy = [];
    Sig_vz = [];

    Numfish = [];
    rgb=[];
    colormap(jet);
    Jet=jet;
    minval=0;
    maxval=50;  
 
    % define which interval we are averaging over and what is the minimum number
    % of fish required to solve the equation
    %***********************************************************************
     interval=1;  %the length of the time interval in hours
    %***********************************************************************

    fprintf('MATLAB is processing the data \n')
    itimebin = 1;
    idepthbin = 1;
    sampletime = [];


    figure(1)
    %clf
    inview = find(Time_series > starttime_window & Time_series < endtime_window);
    [idim, ilen] = size(II1);
    h = pcolor((Time_series(inview)-starttimeday)*24,params.cellrange,(II1(:,inview)));
    set(h, 'EdgeColor', 'none');
    caxis([-80 -40])
    xlabel('Time (hours)','FontSize',fontsize)
    ylabel('Depth (m)','FontSize',fontsize)
    title('Backscatter Beam 1','FontSize',fontsize)  
    set(gca,'FontSize',fontsize)
    set(colorbar,'FontSize',fontsize)
    set(gca,'YDir','normal')


    figure(3)
    ts_all = (Time_series(1)) + ((1:length(Vel_x_water))-1)*(Time_series(length(Time_series))-Time_series(1))/(length(Vel_x_water)-1);
    inview_all = find(ts_all > starttime_window & ts_all < endtime_window);

    clf
    subplot(211)
    imlabelx = imagenans((ts_all(inview_all)-starttimeday)*24,depth_all,Vel_x_water(inview_all,:)'/10,[-50 50],1);
    set(gca,'FontSize',fontsize)
    title('Water velocity x')
    colorbar
    set(colorbar,'FontSize',fontsize)

    subplot(212)
    imlabely = imagenans((ts_all(inview_all)-starttimeday)*24,depth_all,Vel_y_water(inview_all,:)'/10,[-50 50],1);
    set(gca,'FontSize',fontsize)
    set(colorbar,'FontSize',fontsize)
    colorbar
    title('Water velocity y')

    xlabel('Time (hours)','FontSize',fontsize)
    ylabel('Depth (m)','FontSize',fontsize)

    figure(4)
    clf
    if (length(inview_all)>1)
        vxprofile = nanmean(Vel_x_water(inview_all,:));
        vyprofile = nanmean(Vel_y_water(inview_all,:));
        vxprofile = despike(vxprofile,1000);
        vyprofile = despike(vyprofile,1000);
    elseif (length(inview_all) == 1)
        vxprofile = Vel_x_water(inview_all,:);
        vyprofile = Vel_y_water(inview_all,:);
    elseif isempty(inview_all)
        vxprofile = zeros(1,length(depth_all));
        vyprofile = zeros(1,length(depth_all));
    end

    subplot(121)
    plot(vxprofile/10,depth_all,'LineWidth',2)
    axis([-150 150 0 30])
    title('Mean Window Profile','FontSize',fontsize)
    ylabel('Depth (m)','FontSize',fontsize)
    xlabel('East (cm/s)','FontSize',fontsize)

    subplot(122)
    plot(vyprofile/10,depth_all,'LineWidth',2)
    axis([-150 150 0 30])
    xlabel('North (cm/s)','FontSize',fontsize)


    figure(2)
    clf
    ax1 = subplot(221);
    h = pcolor((Time_series(inview)-starttimeday)*24*60,params.cellrange,(II1(:,inview)));
    set(h, 'EdgeColor', 'none');
    caxis([-80 -40]);
    title('Beam 1','FontSize',fontsize)
    set(gca,'FontSize',fontsize)

    ax2 = subplot(222);
    h = pcolor((Time_series(inview)-starttimeday)*24*60,params.cellrange,(II2(:,inview)));
    set(h, 'EdgeColor', 'none');
    caxis([-80 -40]);
    title('Beam 2','FontSize',fontsize)
    set(gca,'FontSize',fontsize)


    ax3 = subplot(223);
    h = pcolor((Time_series(inview)-starttimeday)*24*60,params.cellrange,(II3(:,inview)));
    set(h, 'EdgeColor', 'none');
    caxis([-80 -40]);
    title('Beam 3','FontSize',fontsize)
    set(gca,'FontSize',fontsize)

    ax4 = subplot(224);
    h = pcolor((Time_series(inview)-starttimeday)*24*60,params.cellrange,(II4(:,inview)));
    set(h, 'EdgeColor', 'none');
    caxis([-80 -40]);
    title('Beam 4','FontSize',fontsize)
    set(gca,'FontSize',fontsize)

    linkaxes([ax1 ax2 ax3 ax4],['x' 'y']);


    %% User selects area to analyze
    figure(1)
    disp('Click on upper left and lower right area to analyze')
    disp('Click off left side of graph to move to new image')
    [x y] = ginput(2);


    %%  Plot box around chosen area in 4 beams plot
    axref = axis;
while (x(2) > axref(1))

    tmin = starttimeday +  x(1)/24;
    tmax = starttimeday + x(2)/24;
    dmin = min(y);
    dmax = max(y);

    if (x(2) > axref(1))    %  check if user finished with image
        hold on
        plot(([tmin tmax tmax tmin tmin]-starttimeday)*24,[dmax dmax dmin dmin dmax],'k','LineWidth',2)
        hold off                           

        figure(2)
        subplot(221)
        hold on
        plot(([tmin tmax tmax tmin tmin]-starttimeday)*24*60,[dmax dmax dmin dmin dmax],'k','LineWidth',4)
        hold off
        subplot(222)
        hold on
        plot(([tmin tmax tmax tmin tmin]-starttimeday)*24*60,[dmax dmax dmin dmin dmax],'k','LineWidth',4)
        hold off
        subplot(223)
        hold on
        plot(([tmin tmax tmax tmin tmin]-starttimeday)*24*60,[dmax dmax dmin dmin dmax],'k','LineWidth',4)
        hold off
        subplot(224)
        hold on
        plot(([tmin tmax tmax tmin tmin]-starttimeday)*24*60,[dmax dmax dmin dmin dmax],'k','LineWidth',4)
        hold off


        figure(4)
        clf
        indep = find( (depth_all > dmin) &  (depth_all < dmax));
        %inview = find(Time_series > starttime_window & Time_series < endtime_window);
        inview_samp = find(ts_all > tmin & ts_all < tmax);
        if isempty(inview_samp)  % if time interval smaller than t/s 
            [idim ival] = min(abs(ts_all-(tmin+tmax)/2));
            inview_samp = ival;
            vxprofile = Vel_x_water(inview_samp,:);
            vyprofile = Vel_y_water(inview_samp,:);
        elseif (length(inview_samp)>1)
            vxprofile = nanmean(Vel_x_water(inview_samp,:));
            vyprofile = nanmean(Vel_y_water(inview_samp,:));
            vxprofile = despike(vxprofile,1000);
            vyprofile = despike(vyprofile,1000);
        else
            vxprofile = Vel_x_water(inview_samp,:);
            vyprofile = Vel_y_water(inview_samp,:);
        end
        Inview_samp = floor(mean(inview_samp));

        subplot(121)
        hold on
        plot(vxprofile/10,depth_all,'rx','LineWidth',4)
        hold off
        axis([-150 150 0 30])
        title('Mean Window Profile','FontSize',fontsize)
        ylabel('Depth (m)','FontSize',fontsize)
        xlabel('East (cm/s)','FontSize',fontsize)

        subplot(122)
        hold on
        plot(vyprofile/10,depth_all,'rx','LineWidth',4)
        hold off
        axis([-150 150 0 30])
        xlabel('North (cm/s)','FontSize',fontsize)

        instime = find(fish.timefish >= tmin & fish.timefish <= (tmax));

    else
        instime = [];
    end

%% start of branch process fish
     tim=fish.timefish(instime); %actual values between tmin & tmax
     dep=fish.depthoffish(instime);
     vel=fish.vfish(instime);
     vec=fish.beamvector(:,instime);
     ints=fish.ifish(instime);
     
     insdepth = find(dep>dmin & dep<=dmax); % finds indices

     figure(1)
     hold on
     plot((tim(insdepth)-starttimeday)*24,dep(insdepth),'k.','MarkerSize',14)%was starttime
     hold off

     if(isempty(insdepth) || length(insdepth)<=params.fishthres)

        Vel_x(itimebin,idepthbin) = NaN;
        Vel_y(itimebin,idepthbin) = NaN;
        Vel_z(itimebin,idepthbin) = NaN;

        Sig_vx(itimebin,idepthbin) = NaN;
        Sig_vy(itimebin,idepthbin) = NaN;
        Sig_vz(itimebin,idepthbin) = NaN;

        Numfish(itimebin,idepthbin) = length(insdepth);

     else
        rotmat = [cosd(params.mag_dev)   sind(params.mag_dev) 0;
                -sind(params.mag_dev)  cosd(params.mag_dev) 0;
               0                0              1];

         vecr = rotmat * vec;
         kx = vecr(1,insdepth);
         ky = vecr(2,insdepth);
         kz = vecr(3,insdepth);

         [Vel_x,Vel_y,Vel_z,sigma_vx,sigma_vy,sigma_vz,nfish] = resolve3d_sub(kx,ky,kz,-vel(insdepth));

         Sig_vx = sigma_vx;
         Sig_vy = sigma_vy;
         Sig_vz = sigma_vz;

         Numfish = nfish;
     end
     
    % Display fish velocity         
    disp('Velx  Vely  Velz Stdx Stdy Stdz')
    disp([Vel_x,Vel_y,Vel_z,Sig_vx,Sig_vy,Sig_vz])
    disp(['Number of fish: ',num2str(Numfish)])

    
    % Split-beam fish velocity  
    [vx_split, vy_split, vz_split, fishcount] = pickfish_split(params, tmin, tmax, dmin, dmax);
    disp('Velx  Vely  Velz (Split)')
    disp([vx_split vy_split vz_split])
    disp(['Number of fish in split-beam: ',num2str(Numfish)])
    % Plot fish vel
    figure(4)
    subplot(121)
    hold on
    plot([Vel_x Vel_x]/10,[dmin dmax],'g','LineWidth',4);
    plot([vx_split vx_split]/10,[dmin dmax],'--k','LineWidth',4);
    set(gca,'Box','on')
    ax = axis;
    % plot surface line
    plot([ax(1) ax(2)],[dmax dmax],'b','LineWidth',3)
    subplot(122)
    hold on
    plot([Vel_y Vel_y]/10,[dmin dmax],'g','LineWidth',4);
    plot([vy_split vy_split]/10,[dmin dmax],'--k','LineWidth',4);
    ax = axis;
    plot([ax(1) ax(2)],[dmax dmax],'b','LineWidth',3)
    set(gca,'Box','on')

    disp('E Vel'); disp(round(Vel_x)/10)
    disp('N Vel'); disp(round(Vel_y)/10)

    
    
    
%% Start of notfish
    instime = find(notfish.timefish >= tmin & notfish.timefish <= (tmax));
    tim=notfish.timefish(instime); %actual values between tmin & tmax
    dep=notfish.depthoffish(instime);
    vel=notfish.vfish(instime);
    vec=notfish.beamvector(:,instime);
    ints = notfish.ifish(instime);
    

    insdepth = find(dep>dmin & dep<=dmax); % finds indices
%     figure(1)
%     hold on
%     plot((tim(insdepth)-starttimeday)*24,dep(insdepth),'g.')%was starttime
%     hold off    
       
    if(isempty(insdepth) || length(insdepth)<=params.fishthres)

       Vel_x(itimebin,idepthbin) = NaN;
       Vel_y(itimebin,idepthbin) = NaN;
       Vel_z(itimebin,idepthbin) = NaN;
                
       Sig_vx(itimebin,idepthbin) = NaN;
       Sig_vy(itimebin,idepthbin) = NaN;
       Sig_vz(itimebin,idepthbin) = NaN;
                
       Numfish(itimebin,idepthbin) = length(insdepth);
             
    else
       rotmat = [cosd(params.mag_dev)   sind(params.mag_dev) 0;
                 -sind(params.mag_dev)  cosd(params.mag_dev) 0;
                       0                0              1];
%                      
       vecr = rotmat * vec;
                 
       kx = vecr(1,insdepth);
       ky = vecr(2,insdepth);
       kz = vecr(3,insdepth);
        
       [v_x,v_y,v_z,sigma_vx,sigma_vy,sigma_vz,nfish] = ... 
           resolve3d_sub(kx,ky,kz,-vel(insdepth));

% [not_vx,notv_y,notv_z,notsigma_vx,notsigma_vy,notsigma_vz,notnfish]
% = resolve3d_sub(kx,ky,kz,-vel(notinsdepth));
% -ve on vel because adcp velocities are +ve toward transducer and 
% therefore -ve in the beam direction

       Vel_x = v_x;
       Vel_y = v_y;
       Vel_z = v_z;
                
       Sig_vx = sigma_vx;
       Sig_vy = sigma_vy;
       Sig_vz = sigma_vz;
       
       Numfish = nfish;
%        Vel_true_east = ((Vel_x)*cosd(dec_correct) + (Vel_y)*sind(dec_correct));
%        Vel_true_north = (-(Vel_x)*sind(dec_correct) + (Vel_y)*cosd(dec_correct));
       disp('E Water Vel'); disp(round(Vel_x)/10)
       disp('N Water Vel'); disp((round(Vel_y)/10))
       pause 
    end
       
% end of notfish

    figure(1)
    inview = find(Time_series > starttime_window & Time_series < endtime_window);
    h = pcolor((Time_series(inview)-starttimeday)*24,params.cellrange,(II1(:,inview)));
    set(h, 'EdgeColor', 'none');
    caxis([-80 -40]);
    xlabel('Time (minutes)','FontSize',fontsize)
    ylabel('Depth (m)','FontSize',fontsize)
    title('Backscatter Beam 1','FontSize',fontsize)  
    set(colorbar,'FontSize',fontsize)
    set(gca,'FontSize',fontsize)

    [x, y] = ginput(2);
end

%% User is done with image, 
% move to next timestep, open new file if necessary
    starttime_window = starttime_window + timestep;
    endtime_window = starttime_window + timestep;
    if (endtime_window > endtime_thisfile) 
        endtime_window = endtime_thisfile; 
    end

    if (endtime_window > endtime_thisfile)
        endtime_window = endtime_thisfile;
    end

    if (starttime_window > endtime_thisfile)
        ifilenumber = ifilenumber + 1;

        number = sprintf('%03i',ifilenumber);
        filename2 = [params.fileout number '.mat'];
        if exist(filename2,'file')

            % load not fish data first
            filename3 = [params.filenotfish number '.mat'];
            clear fish Notfish 
            load(filename3)
            notfish = fish;  notparams = params;
            % and now data is parked in notfish and notparams

            starttime_window = endtime_thisfile;   % end of old file becomes
                                                   % start of new window
            endtime_window = starttime_window + timestep;

            load(filename2)
            [ystart,mstart,dstart,hstart,mistart,sstart] = datevec(min(fish.timefish)); 
            [yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish)); 
            starttime_thisfile = datenum(ystart,mstart,dstart,hstart,mistart,sstart);
            endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);
        end
    end
    
   

end    % endtime_window


axref = axis;
[x,y] = ginput(2);

stdthresh = 100;

close(figure(1))
close(figure(2))    
