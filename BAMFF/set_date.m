function[ask_date] = set_date(b,c)
if b == 1
    b = 'JAN';
end

if b == 2
    b = 'FEB';
end

if b == 3
    b = 'MAR';
end

if b == 4
    b = 'APR';
end
    
if b == 5
    b = 'MAY';
end

if b == 6
    b = 'JUN';
end

if b == 7
    b = 'JUL';
end

if b == 8
    b = 'AUG';
end

if b == 9
    b = 'SEPT';
end

if b == 10
    b = 'OCT';
end

if b == 11
    b = 'NOV';
end

if b == 12
    b = 'DEC';
end

if c >= 10
    c = num2str(c);
end

if c < 10
    c = num2str(c);
    c = ['0' c];
end
ask_date = [b c];
end