%------------------------------------------------------
%   Determine absorption coefficient given frequency
%
% Len Zedel ?
%------------------------------------------------------

      function  ans = alpha(f,t,s)


%   f = array of frequency
%   t = temperature
%   s = salinity


% based on equations in Clay and Medwin



%  Freshwater component

% here I assume temperature = 10C

muf = 1.3e-3; 
mufp = 3.9e-3;

w = 2. * pi * f;

alphaef = (4. * muf / 3 + mufp) / (2*1000*(1.48e3)^3) * w.^2;

%  alphaef on np/meter

alphaf = 8.686 * alphaef;   % alphaf dB/m


%
%  Magnesium Sulphate
%

frm = 21.9 * 10^(6 - 1520/(t+273))*1000;  % resonant frequency Mgso4

Ap = 2.03e-5 / 1000;

Pa = 0;            % pressure

alpham = (s * Ap * frm * f.^2) ./ (f.^2 + frm^2) * (1 - 1.23e-3*Pa);

%  alpham in dB/m

%
%   Boric Acid
%

App = 1.2e-4 / 1000;

frb = .9 * (1.5)^(t/18) * 1000;

alphab = App * f.^2 * frb ./ (frb^2 + f.^2);
% alphab in dB/m


ans = alphaf + alpham + alphab;

