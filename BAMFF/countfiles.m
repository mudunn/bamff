function[nfiles] = countfiles(fileroot)
% Finds the largest continuous file index in the folder "fileroot".
% Index starts at 0.

ind = 2;
in = 0;

while ind == 2
    number = sprintf('%03i',in);

    filename = [fileroot number '.mat'];

    ind = exist(filename);
    in = in + 1;
end

nfiles = in - 1;


if nfiles ==0
    ind = 2;
    in = 1;

    while ind == 2
        number = sprintf('%03i',in);

        filename = [fileroot number '.mat'];

        ind = exist(filename);
        in = in + 1;
    end

    nfiles = in - 2;
end