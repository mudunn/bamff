function showfish(params, surface, time, depth, time_matlab, I, corr)
% This function will make plot to show the fish detectability of intensity
% and coorelation. 
% With the goal to fine tune with values to use for the threscorr and
% thresback parameters.

timeAST = time_matlab-(4/24);
tide = t_xtide('Westport, St. Mary Bay, Nova Scotia',timeAST, 'units','meters');
depthtide = 21.6;

clf
figure(1)
ax1 = subplot(211);
imagesc(time_matlab(4500:5500), [1:size(I,1)],I(:,4500:5500))
%colormap(ax1,'parula');
colormap('parula');

hold on
plot(time,depth,'k.', 'MarkerSize', 8)
%plot(time_matlab,surface, 'k')
c = colorbar;
c.Label.String = 'Volume Backscatter [dB]';
datetick('x')
set(gca,'YDir','normal') 
title('Fish detetcted using correlation and backscatter thresholds')
xlabel(sprintf('Time on %s', datestr(time_matlab(1),1)))
ylabel('Depth bins')
%plot(time_matlab,tide+depthtide,'m')
%xlim([datenum(2018,09,22,10,24,00) datenum(2018,09,22,10,28,30)])

ax2 = subplot(212);
imagesc(time_matlab, [1:size(I,1)], corr/256)
colormap(ax2,'pink');
hold on
plot(time,depth,'r.', 'MarkerSize', 8)
plot(time_matlab,surface, 'r')
c = colorbar;
c.Label.String = 'Correlation [Counts]';
datetick('x')
set(gca,'YDir','normal') 
title('Fish detetcted correlation')
xlabel(sprintf('Time on %s', datestr(time_matlab(1),1)))
ylabel('Depth bins')
linkaxes([ax1 ax2],['x' 'y']);
hold on
plot(time_matlab,tide+depthtide,'m')



%% Modify for comparison plot

tstart = datenum(2018,09,22,07,14,25);
tend = datenum(2018,09,22,07,23,59);
tstart_index = find(time_matlab>tstart, 1, 'first');
tend_index = find(time_matlab>tend, 1, 'first');

timeschool = time_matlab(tstart_index:tend_index);
Ischool = I(1:25,tstart_index:tend_index);
corrschool = corr(1:25,tstart_index:tend_index);
surfaceschool = surface(tstart_index:tend_index);
fishindex_school = find(time>tstart & time<tend);
timefish_school = time(fishindex_school);
depthfish_school = depth(fishindex_school);


figure(2)
clf
% Intensity with fish detection
ax1 = subplot(221);
imagesc(timeschool, 1:size(Ischool,1),Ischool, [-80 0])
hold on
plot(timefish_school,depthfish_school,'k.', 'MarkerSize', 8)
%xlim([tstart tend])
colormap(ax1,'parula');
c = colorbar;
c.Label.String = 'Volume Backscatter [dB]';
datetick('x')
set(gca,'YDir','normal') 
title('A) Fish detected - all beams')
ylabel('Range [m]')



% Apply Intensity Threshold
Ischool(~(Ischool>params.threshback))=0;
Ischool(isnan(Ischool))=0;

% Intensity of fish targets
ax2 = subplot(222);
imagesc(timeschool, 1:size(Ischool,1),Ischool,'AlphaData',~Ischool==0, [-80 0])
ylim([1 30])
colormap(ax2,'parula');
c = colorbar;
c.Label.String = 'Volume Backscatter [dB]';
datetick('x')
set(gca,'YDir','normal') 
title('B) Intensity of Detected Fish Targets - Beam 4')
ylabel('Range [m]')


% Apply Correlation Threshold
corrschool(~(Ischool~=0 & corrschool>params.threshcorr))=0;
corrschool(isnan(corrschool))=0;

%Correlation of fish targets
ax3 = subplot(224);
imagesc(timeschool, 1:size(Ischool,1), corrschool,'AlphaData',~corrschool==0,[0 256])
ylim([1 30])
cmocean('amp');
c = colorbar;
c.Label.String = 'Correlation [Counts]';
datetick('x')
set(gca,'YDir','normal') 
title('D) Correlation of Detected Fish - Beam 4')
ylabel('Range [m]')
xlabel(sprintf('Time on %s', datestr(time_matlab(1),1)))
linkaxes([ax1 ax2 ax3],['x' 'y']);


% Split-beam
ax4= subplot(223);
tracks = imread('C:\Users\Muriel Dunn\OneDrive\MastersResearch\ImagesFigures\bubbles0922_0715-0724-2.png');
imagesc(tstart:1/(60*24):tend+(0/(60*24)),25:-1:0,tracks, [-66 46])
datetick('x')
set(gca,'YDir','normal')
title('C) Split-Beam Echogram')
xlabel(sprintf('Time on %s', datestr(time_matlab(1),1)))
ylabel('Range [m]')
linkaxes([ax1 ax2 ax3 ax4],['x' 'y']);
colormap('jet');
c = colorbar;
c.Label.String = 'Volume Backscatter [dB]';
% 
% ax3 = subplot(313);
% tracks = imread('C:\Users\Muriel Dunn\OneDrive\MastersResearch\ImagesFigures\fishtracks.png');
% imshow(tracks);
saveas(gcf,'C:\Users\Muriel Dunn\OneDrive\MastersResearch\ImagesFigures\fishschoolADCPsplit','epsc2');

%% Bubbles
tstart = datenum(2018,09,28,11,15,00);
tend = datenum(2018,09,28,11,19,59);
tstart_index = find(time_matlab>tstart, 1, 'first');
tend_index = find(time_matlab>tend, 1, 'first');

time_bubbles = time_matlab(tstart_index:tend_index);
I_bubbles = I(:,tstart_index:tend_index);
corr_bubbles = corr(:,tstart_index:tend_index);
surface_bubbles = surface(tstart_index:tend_index);
fishindex_bubbles = find(time>tstart & time<tend)
timefish_bubbles = time(fishindex_bubbles);
depthfish_bubbles = depth(fishindex_bubbles);


figure(3)
ax1= subplot(211);
imagesc(time_bubbles, [1:size(I_bubbles,1)],I_bubbles, 'AlphaData',~corrschool==0)
colormap(ax1,'parula');
hold on
plot(timefish_bubbles,depthfish_bubbles,'k.', 'MarkerSize', 8)
plot(time_bubbles,surface_bubbles, 'k')
%c = colorbar;
%c.Label.String = 'Volume Backscatter [dB]';
ylim([0 25])
datetick('x')
set(gca,'YDir','normal') 
title('Fish detetcted ')
xlabel(sprintf('Time on %s', datestr(time_matlab(1),1)))
ylabel('Depth bins')
grid on


ax2= subplot(212);
tracks = imread('C:\Users\Muriel Dunn\OneDrive\MastersResearch\ImagesFigures\bubbles0928_1115-1120.png');
imagesc(tstart:1/(60*24):tend+(0.9/(60*24)),25:-1:0,tracks)
datetick('x')
set(gca,'YDir','normal')
axis on

linkaxes([ax1 ax2],['x' 'y']);
%colorbar;




%% Save
dim = [.2 .4 .5 .5];
str = sprintf('threshback:%3.0f, threshcorr:%3.0f', params.threshback, params.threshcorr);
annotation('textbox',dim,'String',str,'FitBoxToText','on', 'Color', 'w');
figure(1);
saveas(gcf,sprintf('./fig/%s-findfish.png',datestr(time_matlab(1),'yyyy-mm-dd-HH')));
end