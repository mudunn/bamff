\chapter{Results}
\label{chap:detect}


A total of 37 hours of simultaneous data were collected in 10 minute intervals during the first deployment between September 21st and November 1st, 2018. The second deployment did not qualify for further analysis because it does not have enough fish detections in either instrument, less than 110 fish counts in the split-beam over the 90 h of simultaneous data. Results from the first deployment show that the detected fish targets of the ADCP and split-beam datasets generally coincide in time and space. The ADCP has a slightly lower count in the dense fish schools but seems to detect more isolated targets.\\

\section{Fish detection}

\munepsfig[width=5in]{ADCPsplitcount}{Fish counts of the simultaneous data collected averaged over 2 h time bins and 2 m depth bins. a) ADCP fish targets, b) split-beam fish track count.}


The accepted fish detections for both instruments were averaged over 2 h time bins and 2 m depth bins. The temporal bin size was chosen to have enough data points to average (20 minutes of data in 2 hours) without averaging over tidal cycles. The 2 m depth bins were chosen to have two bins to average in the ADCP data while maintaining the depth structure of the fish aggregations. Smaller depth bins fragmented the results. The resulting data summarized in Figure \ref{fig:ADCPsplitcount} shows an agreement in the intervals of higher fish detections. The split-beam generally has a couple of bins with a higher density of fish than the ADCP within each fish school. The more dense areas in the split-beam fish school detections are most noticeable with the fish schools on September 22nd and September 29th, 2018. However, fish counts for both instruments are similar in other aggregations and span the same depth range.\\
 
 \munepsfig[width=5in]{DepthIntTS}{Fish counts of the simultaneous data collected depth-integrated and averaged over 2 h time bins. a) The ADCP fish targets counts are plotted in blue and the split-beam fish tracks are plotted in orange. b) The difference between the split-beam and ADCP fish counts, positive values is when there is more fish counts in split-beam, negative values when there is more ADCP fish count.}

 
The fish detections from Figure \ref{fig:ADCPsplitcount} were depth-integrated (Figure \ref{fig:DepthIntTS}a). The peaks, representing the fish schools, occur at the same time in both the ADCP and split-beam. Notable differences  in between the datasets (Figure \ref{fig:ADCPsplitcount}b) are seen in the fish schools on September 22nd and 23rd, which show a greater number of ADCP fish detections, and the fish school on September 29th and 30th, and October 1st, 2018, where more split-beam fish targets are detected. Both of these occurrences are relatively small schools with less than 100 fish detections. \\

The depth-integrated time series was also analyzed with the fish counts on a logarithmic scale to asses any differences within periods with much lower fish detections, Figure \ref{fig:DepthIntTSlog}. Figure \ref{fig:DepthIntTSlog} shows in general good agreement between ADCP and split-beam observations across all fish concentration values.\\

 \munepsfig[width=5in]{DepthIntTSlog}{Fish counts of the simultaneous data collected depth-integrated and averaged over 2 h time bins on a semi-logarithmic plot. The ADCP fish targets counts are plotted in blue and the split-beam fish tracks are plotted in orange. The values with zero fish were set to 1 to accommodate the semi-logarithmic plot.}


\munepsfig[width=5in]{LRMbothloglog}{Linear regression analysis for comparison of overlapping ADCP fish targets dataset to split-beam fish track counts and SED within fish tracks count, both datasets were averaged in 2 h time bins and depth-integrated for the whole water column.}

 The data of the depth-integrated time series (Figure \ref{fig:DepthIntTSlog}) were used to calculate the linear regression between split-beam SED and fish tracks with ADCP fish counts. Although analysis was carried out using observed values, a scatter plot (ADCP vs. split-beam) is presented using a log-log plot in Figure \ref{fig:LRMbothloglog} in order to highlight agreement at both high and low fish concentrations. When compared against the fish targets identified by the ADCP, a slope of 1.04 $\pm$ 0.3 for the fish tracks and 7.0 $\pm$ 0.3 for the SED of the split-beam was calculated for the linear regression. The SED comparison indicates that there are on average seven single echo detections per ADCP fish target. Overall, a cross-correlation coefficient of 0.92 was calculated between the fish tracks from the split-beam and the fish counts from the ADCP. The p-value for both dataset comparisons are on the order of $10^{-40}$. The y-intercept exists in the SED linear regression (Figure \ref{fig:LRMbothloglog}) because in log space, the y-intercept, $b$, (at x=1) reflects the slope from the linear domain ($b$ = $log_{10}m$).\\
 
It is important to note that the track and ADCP detection agreement seen here is rather circumstantial. The detection volume and sampling rate are different between the two systems. In this case it is just that the system sensitivities for the targets being counted have by chance scaled the actual count values to agree quite closely. More generally, what is important is the linear relationship between the two counts.



\section{Target strength}
\label{sec:TS}
There are a number of reasons why agreement of target strength values between the two instruments would not be expected to agree, most critically, the frequency difference and the absence of a precision calibration for the ADCP. However, the results were compared for the shape of distribution. The volume backscatter strength was calculated using Equations \ref{eq:TS} and \ref{eq:sigbs}. The resulting target strength values show a bimodal distribution for the ADCP data (Figure \ref{fig:tshistogram}) which could suggest that the isolated fish targets were from two different taxa, likely fish and zooplankton. In contrast, the TS distribution of the split-beam shows only a unimodal distribution at slightly greater values.\\

\munepsfig[width=6in]{tshistogram}{Histogram of target strengths observed with both instruments, averaged over two hours and 2 m depth bins. The orange bars are ADCP, the blue bars are split-beam and the third colour is the overlap between the two instruments.}

\munepsfig[width=5in]{tstimeseries}{Fish counts for the simultaneous data collected averaged over 2h time bins and 1m depth bins. a) is the ADCP fish targets and b) is the split-beam fish track count.}

The data from Figure \ref{fig:ADCPsplitcount} was converted to average target strength (Figure \ref{fig:tstimeseries}) in order to asses any spatial structure in target strength values. The ADCP binned TS detections (orange histogram in Figure \ref{fig:tshistogram}) show the lower TS range, from -70 to -60 dB re 1 m$^{-1}$, corresponding to the isolated detections in Figure \ref{fig:tstimeseries}a where the split-beam did not have any fish detection. Comparing with the fish count time series, Figure \ref{fig:ADCPsplitcount}, it is evident that the greater target strengths are from dense aggregations. At the same time, the isolated targets identified by the ADCP correspond to the weaker peak (-70 to -60 dB re 1 m$^{-1}$). The difference in frequencies could account for the detected targets with smaller echoes present in the ADCP (600 kHz) histogram that were not in the split-beam (120 kHz) histogram. The 120 kHz instrument would be unable to measure as small of targets as an 600 kHz instrument because greater frequencies can detect targets that are smaller in size. The split-beam is the industry standard for target strength measurements because it removes the effect of the beam pattern from the backscattered signal. The ADCP data from its four single beam transducers are convolved with their beam pattern because the target can not be located within its beam. Also, the ADCP beams are slanted at a 20\degree\ angle unlike the split-beam where the beam is oriented vertically. The directivity contributed to the spread in the TS distribution of the ADCP because of the dependence of the backscattering cross-section on the tilt angle of the fish \cite{Foote1983}.\\




\section{Entrained air}

A particular challenge in strong tidal flows is the occurrence of near-surface bubble plumes \cite{Melvin2014} which are often hard to distinguish from fish (or plankton) targets. By using a bottom-mounted frame rather than a hull-mounted survey, the data in the present study extends right up to the surface. The zone just below the surface must be clear of entrained air to be analyzed with the split-beam echosounder, because the bubbles are notorious for false detections with fish detection and tracking methods. \\

\munepsfigcaption[width=5.5in]{entrainedaircompare}{Fish detections in entrained air: a) shows the split-beam fish tracks (colorful lines) below the surface exclusion line (red curvy line) and b) shows ADCP fish detections intermingled with the bubbles. The surface is at 25 m: a) the dark red horizontal line, b) the orange horizontal line. }

The surface exclusion line, red line in Figure \ref{fig:entrainedaircompare}a, determined the upper limit for the analysis area and had to be adjusted manually for this area because bubbles and fish are mixed together in the sonogram. Arguably the whole zone above 15 m depth should have been removed from the analysis, but inspection of the echosounder image suggested clear fish targets within the bubble plumes. For the data shown in Figure \ref{fig:entrainedaircompare}b, the Doppler sonar volume backscatter shows the fish targets identified below and within the bubbles. The requirement of coincident high signal correlations and high intensity was used to distinguish discrete targets from bubble clouds. In this case, the ability of the ADCP to differentiate between bubbles and through the use of the correlation values fish target is a promising result that demonstrated that ADCP fish detections could complement traditional survey methods in areas of high turbulence.


