\chapter{Literature Review}
\label{chap:LitRev}


This chapter provides background theory on sonars. It overviews the research that has shaped the current state of fish detection with ADCPs. It includes an in-depth comparison of the methods used by split-beam echosounders and their limitations. To contextualize, the application of fish detection at sites of tidal-energy extraction is explored through the knowledge gaps of the effect of tidal turbines on fish behaviour.



\section{Sonars}
Sonars are used for detection and to remotely measure objects in the ocean. Generally, active sonars use a transmitter to generate an electrical signal at a particular frequency which is converted into a sound by a transducer. The sound forms a beam of sound through the water column away from the face of the transducer. These propagating sound waves are scattered by any object with an acoustic impedance different than the medium through which the sound is travelling. The transducer receives the scattered signal that is returned towards the instrument. The instrument amplifies the received signal and converts it from analogue to digital for further processing. These basic concepts are modified for a variety of different applications, for example, water velocity extraction and fisheries sciences. The differences that distinguish the sonars used in these fields are described below. \\

Water velocity studies have applications for measuring the velocity of a ship relative to the seafloor, ocean surface speed, internal waves or currents. These applications use Doppler sonars, more specifically ADCPs. These instruments are described in more detail in Chapter \ref{LitRevADCP}.\\

	Fisheries acoustics mainly uses echosounders. They are a specific type of sonar in which the system is designed to detect and observe fish or the depth of the seabed. Acoustics is prevalent in fisheries research; in part because fish swim bladders are an effective target because the acoustic impedance of air and water differ greatly from each other and in part because fisheries acoustics provides a non-invasive survey method \cite{Trenkel2019}. In the most basic single-beam echosounders, e.g. fish finders, fish schools can be identified, but there is no way to differentiate a small fish in the middle of the beam from a large fish on the edge of the beam, therefore target strength cannot be quantified. Target strength (TS) is used by bioacousticians to quantify the size of echoes to learn as much as possible about fish populations \cite{Medwin1997}.\\

\subsection{Sonar Equation and Target Strength}

Target strength can be put into context through the log-sonar equation, Equation \ref{eq:sonar} \cite{Medwin1997}, which is used to measure the sound pressure received by the transducer, represented as sound pressure level, $SPL$.

\begin{equation}
\label{eq:sonar}
SPL = SL - 2TL + TS
\end{equation}

\begin{equation}
\label{eq:SPL}
SPL = 20 \log_{10} (\frac{P_{bs}}{P_{ref}})
\end{equation}

\begin{equation}
\label{eq:SL}
SL = 20 \log_{10} (\frac{P_{0}}{P_{ref}})
\end{equation}


The $SPL$, Equation \ref{eq:SPL}, is the sound pressure backscattered to the instrument, $P_{bs}$, in logarithmic terms. It is based on on the emitted signal level, $SL$, the sound pressure at the source, $P_{0}$, in logarithmic terms, Equation \ref{eq:SL}, transmission loss, $TL$, which is the sound lost due to range and absorption, and target strength, $TS$. All of these terms are measured in decibels. The sound level and sound pressure level are calculated from their linear pressure levels relative to a reference pressure, $P_{ref}$, 1 $\mu$Pa in the ocean.


\begin{equation}
\label{eq:TL}
TL = -20 \log_{10}( \frac{R}{R_{0}}) + \alpha R
\end{equation}.

The transmission loss term encompasses much of the complexities involved in underwater acoustic propagation. It is dependent on bathymetry, sound speed profiles, multipath arrivals, range, source frequency, divergence from the source to receiver, absorption and scattering \cite{Medwin1997}. Much of that complexity is hidden in the absorption loss term, $\alpha$, in $\frac{dB}{m}$, within Equation \ref{eq:TL}, but it is also a function of the range of the target, $R$, and the reference range, $R_{ref}$, 1 m in ocean acoustics. Here we describe transmission loss for spherical spreading, as opposed to cylindrical spreading, because it is appropriate for propagation in the case where sound does not interact with the ocean boundaries. \\

The last item in the sonar equation, Equation \ref{eq:sonar}, is target strength, $TS$. It describes the fraction of incident acoustic energy scattered back to the instrument. $TS$ is measured by converting the backscattering cross-section ($\sigma_{bs}$) of a target to a logarithmic scale:

\begin{equation}
\label{eq:TS}
TS = 10 \log_{10}(\sigma_{bs})
\end{equation}

\begin{equation}
\label{eq:sigbs}
\sigma_{bs} = V * s_{v} \\
\end{equation}
where $V$ is the sampled volume and the volume backscatter coefficient is $s_{v}$, in m$^{-1}$ \cite{Maclennan2002}. The volume backscatter coefficient is the linear measure of the logarithmic volume backscatter strength $S_{v}$:


\begin{equation}
\label{eq:VBC}
S_{v} = 10 \log_{10}(s_{v}) \\
\end{equation}


 The volume backscatter strength, $S_{v}$, is commonly used to demonstrate the received signal in dB re 1 m$^{-1}$ \cite{Maclennan2002}.\\

 The sample volume is a measure of the space that contributes echoes to the transducer for any signal \cite{Maclennan2013} it can be related to the acoustic beam pattern as:

\begin{equation}
\label{eq:V}
V = \frac{c \tau \psi R^{2}}{2} \\
\end{equation}
where the range is $R$, the pulse length is $\tau$ and the sound speed velocity is $c$. The sample volume represents a measure of the beam width \cite{Maclennan2013} and results in the direction of the target relative to the origin of the transducer. The equivalent beam angle, $\psi$, indicates the solid angle of an idealized acoustic beam:


\begin{equation}
\label{eq:psi}
\psi = \int_{\theta=0}^{\pi} \int_{\phi=0}^{2\pi} b^4(\theta, \phi) \mathrm{d}\theta \mathrm{d}\phi.\\
\end{equation}
where for an ideal cylindrical transducer, the directivity is expressed as:

\begin{equation}
\label{eq:Directivity}
b = \frac{2 J_{1}(ka\sin(\theta))}{ka\sin(\theta)}\\
\end{equation}
where the transducer radius, $a$, wavenumber, $k$ and $J_{1}$ is a Bessel function of the first kind \cite{Medwin1997}.   \\


When combined with knowledge of the study site and the expected species, target strength can provide an indicator of fish species, size and biomass estimates \cite{Maclennan2013} because it reports the size of the echo. When this type of data is used to determine the length dependence on target strength, the TS-length relationship, it can be very powerful for extrapolating existing datasets of target strength and population \cite{Foote1986}. Target classification, such as species identification based on TS-length relationship and frequency, is an active research topic \cite{Korneliussen2018} because it could decrease the need for complementary trawl surveys in areas where species composition is not well understood. However, having the equations is not enough; the instruments must be able to provide a calibrated and precise volume backscatter and range measurement of the targets. For a meaningful target strength result, echosounder technology has gone through decades of iterations to provide a calibrated target strength, for example, from narrowbeam to split-beam, in parallel, the required calibration protocols have been established. \\


\subsection{Echosounders}
 Many types of echosounders have been developed with increasing complexities to improve on some of the drawbacks of single-beam echosounders, such as to correct for the effect of the beam pattern from an acoustic echo \cite{Ehrenberg1996}.  Dual-beam and split-beam echosounders are designed to improve direct \textit{in situ} target strength measurements \cite{Maclennan2013}. Dual-beam echosounders are composed of center elements that form and widebeam and outer elements that form a narrowbeam. The received signal of the wide and narrow beam are compared to determine backscattering cross-section, range and off-axis location of targets by calculating the ratio of the intensity of their received signals. Dual-beam echosounders are more affected by additive noise compared to split-beam echosounders because they have a lower signal to noise ratio \cite{Ehrenberg1996}. For this reason alone, dual-beams systems are inferior to split-beam echosounders \cite{Foote1986}. Additionally they do not provide angular location \cite{Ehrenberg1996}. Therefore, we focus on split-beam echosounders as the industry standard for fisheries acoustics, described in Section 2.3; these instruments provide information on swimming speeds, location in the water column and the direction of travel of individual fish \cite{Ehrenberg1996}, all of which are useful to study fish behaviour. \\

 Formalizing the additional capability of the ADCP to detect fish would complement present methods used for fisheries sciences. In this thesis, we made a direct comparison of the industry standard for fisheries acoustic studies, split-beam echosounder with an ADCP for fish detection.



\section{Acoustic Doppler Current Profiler}
\label{LitRevADCP}

For most echosounders, the scattered signal will be received through the same transducer that emitted the original sound pulse. In the case of ADCPs, the standard instruments for measuring ocean current velocities, the sound pulses are emitted and received through four narrow beam transducers (about 4\degree\ to the -3 dB point on the beam) each directed 20\degree\ from the vertical. Each transducer emits a pulse of a known frequency which gets scattered by suspended particles in the water column. The energy that is scattered back to the instrument is described as being backscattered. The frequency of the backscattered signal is shifted in proportion to the velocity of the scatterer by the process of Doppler shifting. Based on the frequency shift in each beam, the ADCP can calculate the radial velocity of the scatterers. The change in frequency due to the Doppler effect, is given by:

\begin{equation}
\label{eq:doppler}
f_{r} = f_{0} - \frac{2 v_{R} f_{0}}{c}
\end{equation}

where $v_{R}$ is the radial component of the relative velocity between the source and the scatterer, $c$ is the speed of sound in water, $f_{0}$ is frequency emitted by the source, $f_{r}$ is the frequency received \cite{Maclennan2013}. The factor of $2$ is due to the moving target's velocity relative to the source shifting the frequency twice; once, when the sound is received by the moving target and a second time, when it returns to the source.\\

Each transducer provides the component of velocity along its beam axis; traditionally, the result of each beam is grouped into depth bins and is combined with the other beams to provide the velocity as a profile of three component velocity vector. This method assumes that flow is homogeneous between the beams and within the whole depth bin, as shown in Figure \ref{fig:ADCPnew}. \\

\munepsfigcaption[width=4in]{ADCPnew}{Bottom-mounted ADCP with the depth bins shown. The cube is a passive target following the water flow, such as biota or sediment.}

Even though the principle is the same for all types of Doppler instruments, they vary in the way they transmit the signal. Narrowband ADCPs send a long pulse of acoustic energy with a known frequency and calculates the frequency of the echo; the along-beam velocity is determined by comparing the difference in sent and received frequencies. The pulse must be long in order to establish a specific known frequency, the longer the pulse the less it will include other frequencies. Incidentally, a long pulse will cause the water profile to be averaged into large depth bins, resulting in poor depth resolution. A narrowband ADCP has a limited precision because the pulse length and depth bins are interrelated. Coherent systems send two pulses but wait to have received the echo of the first pulse before sending the second pulse, which can create a relatively long lag between pulses. The phase shift between the pulses is analyzed to calculate the water velocity, the long lag provides a more precise measurement. However, if the lag is too long, relative to the water velocity, the pulses won't be measuring the same parcel of water, which leads to a decorrelation of the pulses. Even if the pulses are correlated, if the velocity is too great, the phase difference becomes too large and causes a phase wrap. The phase wrap is explained through the ambiguity velocity.\\

 Ambiguity velocity arises from using the phase difference between the pulse pair to calculate velocity. As the velocity increases and the phase difference wraps past $\pm \pi$, a new velocity range is started without indication to which velocity range the phase difference corresponds. The maximum velocity in the lowest velocity domain determines the ambiguity velocity, or the maximum velocity that can be resolved. \\

Broadband systems send pulse pairs with a lag between them. The water velocity is proportional to the phase difference between the echoes of the pulse pair, which is detected through the auto-correlation of the received signal. In this thesis, the signal auto-correlation is used to differentiate fish from incoherent targets, such as entrained air. This type of Doppler system is 20 years old (1995), it is essentially a coherent system that accepts range ambiguity associated with having two pulses in the water column at the same time. It overcomes this problem by using a large bandwidth to make many independent estimates of velocity. Ambiguity velocity is a concern with broadband as well as coherent ADCPs.\\


Signals received by ADCPs contain backscatter from a variety of objects in the water column, including fish, zooplankton, sediments, bubbles and the surface or bottom. As the instrument is used for water velocity measurements, typically all signals that differ significantly in echo intensity between the four beams are rejected to reduce fish bias in velocity data \cite{Freitag1993}. The significant difference in echo intensity suggests that one beam is detecting something different from the other beams (i.e. a fish). This is the basis utilized by the rejected fish signals as a data source to study fish. \\

Vent et al. (1976) works on using Doppler measurement for determining the target strength of entire fish schools. Only the radial velocities were resolved because there was no homogeneity within each depth bin. Vent concludes that the interaction between acoustic energy and fish schools is very complex and dependent on the physical and acoustical properties of the observed school \cite{Vent1976}. \\

Holliday (1977) states, ``...the utilization of measurements involving the Doppler phenomenon can play a valuable role in the study of fishery resources"  on using the Doppler effect for his study of fish schools dynamics relative to water and to study movement within a fish school. Holliday's work is done using a narrowbeam ADCP with a long pulse length. \\

Olsen et al. (1983) compares video, echosounder and Doppler shift measurements to measure velocity estimates of herring and cod as a function of vessel speed to quantify fish reaction to a surveying vessel. The speed of the vessel and the depth of the fish are found to be important in identifying the magnitude of the velocity of the fish schools. The echosounder limitations are observed when recording groups of fish that are not sufficiently dispersed to allow for single-target echo detection. Olsen et al. (1983) suggests that studies on the behaviour of dense fish aggregations could benefit from Doppler shift measurements because it could provide velocity measurements through the fish echoes recorded.\\

Most research methods before Demer (2000) use low frequency, long pulses and narrow bandwidth. Though Doppler shift methods were promising, theoretically, for gathering information on fish behaviour, the equipment settings and data processing algorithms were not fully developed. Demer (2000) suggests that ADCPs need to achieve higher velocity and range resolution and shows that this could be accomplished by using a higher frequency, and auto-correlation for the detection of pulse shifts. Demer (2000) also identifies a need to modify data collection parameters and data processing as well as  to select survey sites carefully with fish schools matched to the size of the processing bins. \\

\subsection{Velocity measurements}

Conventional processing methods for water velocity extraction with Doppler profilers combine the radial velocity of each beam to resolve the velocity vector of the passive sediment and biota targets. This method requires all the beams to be detecting the same homogeneous flow within each depth bin. Fish targets and other targets that ``corrupt'' the signal by having an anomalous volume backscatter are rejected from the calculation.\\


Zedel and Cyr-Racine (2009) present an alternative approach to analyzing Doppler sonar data using a least-squares based algorithm which treats each acoustic beam individually to extract both fish and water velocities, even when fish are intermittently present. The main difference is that their optimization algorithm utilizes all the data collected by the Doppler profiler rather than only using data for which the assumptions of homogeneous flow over the beam sample volumes can be assured. All observations are identified as fish or water targets and are binned by time and depth intervals; these subsets are used to determine the velocity of water and fish (if present) in that bin. \\

If the measured velocity is $\vec{V} =\{ V_{x}, V_{y}, V_{z} \}$ and the unit vector that describes the orientation of the j'th component measurement is $\hat{k}_{j} = \{k_{xj}, k_{yj}, k_{zj}\}$. We can express the best choice of velocity as,

\begin{equation}
\label{eq:velocity}
v_{j} = \vec{V} \cdot \hat{k}_{j} = V_{x} k_{xj} + V_{y} k_{yj} + V_{z} k_{zj} 
\end{equation}

  If there is enough measurements within a range of orientations, a standard least-squares approach can be used to minimize the sum of the square of the residuals between the measured velocities and predicted $v_{j}$. By doing this for all observations, the optimal x, y and z component of the velocity vector will be extracted. This is beneficial because it bypasses the dependency on a homogeneous flow and accommodates mixing missed and rejected data \cite{Zedel2009}. \\

This method is more computationally involved but provides more information. The algorithm has been validated for water velocity measurements through a field test and provides the same velocity results as the standard Doppler processing method \cite{Zedel2009}. The velocity of fish relative to the water velocity differentiates a small stationary school from a large transient swimming school. Even with a suitable processing algorithm for fish velocity extraction, the challenge remains to identify the presence of fish in ADCP data accurately. \\




\subsection{Calibration}
\label{ADCPcal}
Doppler profilers, like any other acoustic instrument, must be routinely calibrated to account for the offset in intensity of the signal. Each beam of the ADCP is calibrated for sensitivity by finding the slope of the echo intensities. The echo intensity is required for volume backscatter measurements, $S_{v}$, because it describes the reflective characteristics of sound. The calibration is performed by transmitting a known frequency to the transducer of the ADCP by acoustically coupling another transducer to its face. The signal is incrementally decreased; the corresponding measured echo intensity voltage is measured and recorded \cite{McTamney2019}.  The slope, in counts/dB, of these measurements is calculated for each beam to determine the sensitivity.




\section{Fish tracking with split-beam echosounders}

Split-beam echosounders send an acoustic pulse through the water column, like a conventional (single-beam) transducer but the transducer receives the backscatter in 4 separate quadrants. If the target is on-axis, in the middle of the beam, all the quadrants will receive the signal with the same phase. As the target ventures off the center axis, the phase difference between the signal from each quadrant is used to locate the target in the beam, as shown in Figure \ref{fig:splitbeam}. When combined with the on-axis sensitivity and the beam pattern of the echosounder, the location of the target in the beam is used to determine absolute target strength. The variance of the average target strength can be significantly reduced by using the method of target tracking. Tracking provides multiple estimates of the target strength for a single fish; the reduced TS variance leads to better size estimates and species identification \cite{Ehrenberg1996}.\\


 \munepsfig[scale=0.15]{splitbeam}{A split-beam echosounder diagram with its four quadrants and the phase difference analysis used to locate the fish in the beam. An image from \cite{Maclennan2013}. Permissions by John Wiley \& Sons Company.}

Target tracking provides a better estimate of target strength and extracts fish velocity relative to the echosounder for individual fish targets. A variety of algorithms have been developed with varying complexity to identify and isolate a single fish and follow it through time and space as it swims through the beam. Algorithms differ in the way they address the challenges imposed by the survey site or the species. Common challenges are large swim speed, TS changing with fish tilt angle, entrained air and fish swimming in and out of the beam. Some algorithms have been designed to improve on certain challenges, for example, Balk's cross-filter detector minimizes signal contamination \cite{Balk2017} and Fraser's algorithm focuses on detecting fish targets in highly turbulent areas \cite{Fraser2017}. However, all of the target tracking algorithms require careful setup and in-depth knowledge of the target species \cite{Xie1997}. Adequate setup can be labour intensive because the target tracking algorithm parameters must be tuned specifically for the study site and its scatterers. \\

Target tracking requires many post-processing steps but significantly increases the amount of information provided by a split-beam echosounder. When fish aggregations are too dense, individual fish echoes cannot be isolated, and the fish tracking algorithm is unable to create individual fish tracks to extract swim speeds and averaged target strengths. The best results for fish tracking are achieved when the mean density of fish is low, and the target strength distribution width is narrow. A low fish density minimizes the chances of detecting echoes from two or more fish as a single target, resulting in data which be discarded. When multiple species are present, additional processing steps are required to identify single targets due to their respective target strength values \cite{Maclennan2013}. A narrow TS distribution allows the user to select species specific parameters. Carefully chosen parameters with narrow ranges for the minimum and maximum values facilitate fish identification and can reduce the detection of false targets.\\

 For the purpose of echo counting if fish aggregations are too dense, a method called echo integration can be used to estimate the biomass of fish in the acoustic beam regardless of the signals overlapping. Echo integration is a well-established technique for estimating biomass or target density. The total echo intensity over a prescribed sample volume is used to calculate the density of a group of targets. Echo integration relies on the linearity of the echo energy, which means that the echo energy of a school of fish is the sum of all the individual contributions of the echo energy of each fish \cite{Foote1983}. The fish counts can be estimated from the target density if the mean target density of the population is known.\\

Enzenhofer's experiment studies salmon migration in a river and compares split-beam echosounder and video recordings to determine the bias of fish numbers from split-beam fish tracking \cite{Enzenhofer1998}. They found that split-beam fish tracking in that application was limited by the signal oversaturation when migrating rates were more than 2000 fish/h, thus having a bias of underestimating fish counts through the echo estimates for high echo density \cite{Enzenhofer1998}. In situations with a high fish density, the split-beam system and fish tracking algorithms are limited by the need to detect and isolate individual fish into single target echoes. However, with the echo integration method, the fish aggregation density can be determined acoustically with enough knowledge of the size distribution of the species or the TS-length relationship \cite{Foote1983}. \\


\subsection{Calibration}
\label{Splitcal}
The split-beam echosounder is calibrated routinely to have accurate and repeatable measurements. Field calibrations should be performed before and after system deployment. This is done to ensure consistency in measurements of volume backscatter and to account for any potential drift of values over time due to wear on the the instrument's components. A tungsten carbide calibration sphere is used as a standard target. It is sphere shaped to remove the dependence of target strength on orientation and tungsten carbide is used because of it has well defined acoustic properties \cite{Miyanohana1993}. The size of the sphere, 33.2 mm, is chosen to avoid resonances that occur in the sphere at the operating frequency of 120 kHz \cite{Foote1982}. For the calibration procedure, the calibration sphere is moved in a spiral motion at 5 to 10 m away from the transducer face. The goal is to have about 500 echoes of the calibration sphere per quadrant of the transducer beam. The target strength of the 95th percentile of the targets within -3$\degree$ and +3$\degree$ off the center is used to compare with the theoretical value given by the calibration sphere datasheets. The difference between the theoretical target strength value and the target strength calculated in the calibration is used as an offset to adjust the sensitivity drift of the transducer.





\section{Application to the in-stream tidal industry}
The need to monitor fish presence and behaviour in regions of in-stream tidal energy generation motivates the evaluation and validation of fish detection and velocity extraction with ADCPs. High-energy tidal channels are inherently difficult to survey with conventional hull-mounted acoustic instruments. These difficulties affect many aspects of the surveys; in addition to the regular surveying challenges such as vessel noise contamination and weather limitations, the tidal turbulence generated backscatter (bubbles) \cite{Melvin2014} attenuates the signal. Sites of tidal energy generation projects are being targeted for the same reasons that make them difficult to survey, they require high-energy, fast tidal flows. Tidal energy industry converts power from the tidal currents into electricity by installing turbines with blades that rotate similar to wind turbines. Since in-stream turbine technologies do not force migrating fish to pass through dams or enclosures, it is possible that they would have a smaller impact on fish migrations. However, the impact of tidal turbines on fish migratory patterns and stock is not yet understood and requires more research \cite{Shen2016}.\\

Melvin and Cochrane (2014) set a goal to monitor fish distribution to understand the behaviour (or reaction) of fish when an underwater structure is encountered. They completed nine transects over a full tidal cycle in Minas Passage, a 12 km long and 5 km wide tidal channel, at a location proposed for in-stream tidal power energy conversion devices. Melvin and Cochrane (2014) report that fish in the middle water column are likely to interact with turbines even though the overall density of fish is low, because there is a high density of transient fish in Minas Passage. Melvin and Cochrane (2014) make qualitative recommendations about instrumentation and deployments, such as replacing or complementing conventional boat surveys with stationary or autonomous bottom-mounted echosounders to minimize vessel noise avoidance. Incidentally, these bottom-mounted deployments could lower cost and increase the survey time series length by remaining underwater for several months with minimal boat costs. A long-term assessment of an area could provide the complete dataset required to evaluate the impact of tidal power development on the behaviour and mortality of fish \cite{Melvin2014}. Though the recommendations are insightful, they were unable to make quantified conclusions about fish behaviour in relation to in-stream tidal turbines.\\

While research on the effect of in-stream tidal turbines on fish behaviour is limited, Viehman et al. (2015), and Viehman and Zydlewski (2017) provide a comprehensive study of the use of the water column by fish and fish abundance in Cobscook Bay, Maine, USA, an area with tidal currents strong enough for tidal power generation. The difficulty of sampling a full tidal cycle in these strong tidal currents conditions had formed a gap in the literature. The research focused on the vertical distribution of fish before the installation of a proposed in-stream tidal turbine to find which populations would be most affected and eventually to compare with vertical densities after the installation \cite{Viehman2015}. After the removal of the tidal power conversion device, Viehman and Zydlewski (2017) used two years of bottom-mounted moored echosounder data to study temporal variations in fish abundance by completing a wavelet analysis at the location of a removed tidal turbine. Current direction is available for part of the dataset through ADCP measurements, but when this data is unavailable, fish direction is used as an proxy for tidal current, therefore tidal stage. The wavelet transform analysis was successful in finding temporal patterns (modes) in the fish abundance data. However, this study only included individual fish. Fish schools were omitted due to the difficulty in distinguishing them from entrained air and identifying individuals within a school. The importance of continuous long-term sampling in these areas with highly variable fish presence is highlighted \cite{Viehman2017}.\\


ADCPs are essential in any in-stream tidal current analysis due to the need to measure the highly variable current velocities. These instruments are designed to perform long-term deployments, which is a key factor missing in the literature described above on fish avoidance analysis at tidal energy sites. With the validation of the algorithm proposed by Zedel and Cyr-Racine (2009), fish behaviour studies at various types of research sites could be consistently measured over multiple days, tidal cycles and seasons. In the last five years, there has been some research efforts focused on understanding fish behaviour changes in the presence of tidal turbines. However, research in the field of fish velocity extraction using ADCP instruments does not seem to have kept up, there is a gap in knowledge that could be filled by using the recent tidal energy site research and ADCP's capabilities to extract fish and water velocities.\\
