\chapter{Experimental Methodology}
\label{chap:expmethods}
 
 To evaluate ADCP performance for fish detection, we designed an experiment that allowed a direct comparison between an ADCP and a split-beam echosounder. This section details the experimental methods. The computational methods are described in Section \ref{chap:compmethods}. The experimental methods include the deployment site description, the instruments description, collection settings and calibration as well as an account of the battery power considerations when comparing instruments with widely different battery requirements. 
 
 
\section{Experimental Site}
 
The Bay of Fundy is home to some of the largest tides in the world. Grand Passage, Nova Scotia, located on the southeastern side of the Bay of Fundy, has a 5 m tidal range and currents up to 1.5 m/s. Due to these strong tidal currents, it is identified as having the potential for in-stream tidal developments. Prior studies of fish presence in high-energy tidal channels have identified a common survey challenge to be acoustic scattering from near-surface bubbles \cite{Melvin2014, Viehman2015}. The study by Melvin and Cochrane (2014) recommends deploying ``an autonomous, stationary, bottom-mounted echo-sounder” to overcome these challenges. As an experimental site, Grand Passage, NS, further motivates the recommendations because it has frequent boat and ferry traffic. A long-term bottom-deployment allows for inter-survey data which can validate the ``snapshot” measurements from conventional surveys. Additionally, diurnal and tidally induced fish behaviour can be separated with a long-term deployment. Acoustic measurements for a comparison of split-beam sonar and ADCP were collected from a bottom-mounted self-contained frame deployed at 25 m depth and positioned as shown in Figures \ref{fig:GPdeploymentlocation} and \ref{fig:ADAAM_GP_land}; note the location was selected to avoid interference from the ferry traffic. 

\munepsfigcaption[width=5.5in]{GPdeploymentlocation}{Location of the frame in Nova Scotia, Canada. }

\munepsfigcaption[width=4in]{ADAAM_GP_land}{Location of the frame ($\star$) in Grand Passage, (extent of the ferry route between \textit{Freeport} and \textit{Westport} is the chequered red area). The green area is land and the isolines are depths at 5 m intervals in the tidal passage, as shown in the legend.}




\section{Deployment}

A bottom-mounted frame was equipped with an RD Instruments 600 kHz acoustic Doppler current profiler and a BioSonicsDTX Submersible 120 kHz split-beam echosounder, as shown in Figure \ref{fig:GPframe}. The 37-day deployment took place from September 21st, 2018 14:30 to October 29th, 2018 13:54. The second deployment was deployed for 28 days from June 11th, 2010, 12:05 to July 9th, 2019, 10:09. The acoustic instruments were installed as close as possible to facilitate direct comparison between both datasets. \\

\munepsfigcaption[width=5in]{GPframe}{Left: A picture of the frame before the deployment with the ADCP in the middle. The split-beam echosounder system includes the echosounder on the shelf, the black horizontal cylinder on the left is the DT-X Submersible and the battery pack is the blue horizontal cylinder on the right. Right: Sketch of the frame designed by Richard Cheel. The frame is 3 by 4 feet.}
 
 
 \subsection{Duty Cycle}
 
A common problem when operating two nearby acoustic instruments is crosstalk. We chose widely separated frequencies to mitigate this problem (120 kHz for split-beam and 600 kHz for the ADCP). Nevertheless, contamination in the split-beam sonar data was observed during lab tests. Therefore, in addition to the separate frequencies, the duty cycle was staggered to ensure the collection of individual uncontaminated data from both instruments as well as simultaneous measurements. To achieve an offset in the timing of the instruments, the split-beam data acquisition cycle of was started 10 minutes into the ADCP duty cycle.\\

The ADCP sampling duty cycle was 20 minutes on and 20 minutes off. The Workhorse ADCP time commands from the RDI configuration software were used to set the duty cycle, the chosen settings are summarized in Table \ref{tab:ADCPconfig}. The time per burst command, TB, determines the length of one whole duty cycle. The ensemble per burst command, TC, dictates the number of ensembles within the burst time. The split-beam sampling regime was 20 minutes on and 40 minutes off. \\

Upon inspection, the first deployment displayed little contamination between instruments and crosstalk was found to be inconsequential for fish detection. Given that crosstalk did not cause difficulty, the duty cycle was adjusted for the second deployment to maintain the 10 minutes of simultaneous data collection while extending the battery life of the split-beam system. The ADCP was configured to collect data for 30 minutes and be off for 30 minutes while the split-beam was on only for the middle 10 minutes of the ADCP on cycle. \\


\subsection{Data Collection Settings}

The ADCP was set to collect at one ping per second with 1 m bins with no averaging of the profile data. It is crucial for the fish detection algorithm that the pings are not averaged. If multiple pings are averaged together the discrete targets, such as fish, will be either averaged out or combined. The time per ensemble, TE, dictates how much time will be averaged in each ensemble and time between pings, TP, sets the ping rate, see Table \ref{tab:ADCPconfig} for RDI configuration software commands. Both the time per ensemble and the time between pings must be the same value to have one ping per ensemble, hence no averaging in the time domain. The split-beam sonar was configured to transmit four pings per seconds with a 0.1 ms pulse duration. \\


\begin{muntab}{|c|l|r|r|}{ADCPconfig}{ADCP RDI Workhorse Time and Water Profiling Commands for both the 2018 and 2019 deployments.}
\hline
Command &  Description & Deployment 2018 & Deployment 2019 \\ 
\hline
\hline
 TB & Time per burst & 00:40:00.00 & 01:00:00.00\\
\hline
  TC & Ensemble per burst & 1200 & 1800 \\
\hline
 TE & Time per ensemble & 00:00:01.00 & 00:00:01.00\\
\hline
 TP & Time between pings & 00:00:01.00 & 00:00:01.00\\
\hline
\hline
 WE & Error Velocity (mm/s) & 0 & 5000 \\
\hline
  WC & Correlation Threshold (counts) & 0 & 0 \\
 \hline
 WN & Depth Cells & 30 & 60\\
\hline
 WS & Depth Cell Size (cm) & 50 & 100\\
 \hline
 WV & Ambiguity Velocity (cm/s) & 175 & 175\\
 \hline
\hline
\end{muntab}

\subsection{Calibration}
There are three critical components of the instrumentation that need to be calibrated: the ADCP compass, the ADCP backscatter and split-beam backscatter. The ADCP compass was calibrated the day before both deployments by rotating the mounted frame in a constant magnetic field environment \cite{RDI2001}. The whole frame must be included in the compass calibration to account for any compass offset that may be present due to the magnetic materials of the instrument frame, such as the batteries and any iron material. \\

The ADCP backscatter calibration coefficients were taken from a January 2018 laboratory calibration. The backscatter calibration was performed following the details described in Section \ref{ADCPcal}. The split-beam was calibrated using the method described in Section \ref{Splitcal}, in the days immediately following the retrieval of the frame. A 33.2 mm diameter tungsten carbide sphere and a 0.4 ms pulse length were used to perform the calibration. The calculated offset of 1.1 dB was applied to the first dataset. The offset from the second deployment was not calculated because the data did not contain fish and was not used for further analysis.



\section{Battery Considerations}

Split-beam echosounders consume significantly more power than ADCPs because they are not designed for long-term autonomous deployments. An accurate battery consumption calculator based on sampling parameters is important for long-term deployments of split-beam echosounders to ensure the collection of enough data over a known period of time. The first step of the deployment design was to calculate the battery consumption of the split-beam system based on collection parameters such as ping rate, pulse duration, duty cycle and range. Initially, the BioSonics DT-X Quick Start Guide \cite{Biosonics2016} was used to anticipate the power consumption of the BioSonics Submersible DT-X with one transducer connected. It was quickly established that this would not provide a sufficiently accurate estimate of collection time because it relies on approximations and suggests taking measurements if power draw is of concern. \\

A series of nine lab tests were performed. Seven tests with unique combinations of data collection parameters as well as a two repeat measurements to develop an algorithm that estimates the battery life. The battery life estimate was based on collection parameters and its variance between repeat measurements. The choice of data collection parameter combinations for the tests was driven by expected deployment need and finding what parameters drove the power consumption. \\

The algorithm was designed to account for the power draw of the sleep and transmit states and the fraction of time the instrument spends in each state per duty cycle:

\begin{equation}
\label{eq:BattEst}
E_{cycle} = P_{sleep} * \frac{t_{sleep}}{t_{cycle}} + P_{transmit} * \frac{t_{transmit}}{t_{cycle}}
\end{equation}
where $P$ is the power draw for the $sleep$ or $transmit$ state, $t$ is the amount of time spent in the $sleep$ or $transmit$ state, determined by the duty cycle and $E_{cycle}$ is the energy used for one full cycle.\\

The total operating life was then calculated from the ratio of the total energy available and the energy used for one cycle. 

\begin{equation}
\label{eq:BattEsttotal}
t_{total} = \frac{E_{available}}{E_{cycle}}
\end{equation}

where $E_{available}$ is the total amount of energy available (Wh) in the battery pack.\\

In determining the values for the power consumption terms in Equation \ref{eq:BattEsttotal}, the goodness of fit was evaluated using the percentage difference between the the lab tests and the calculated operating life. The test initial results, taken with a profiling range of 100 m, were within 2\% of the observed total sampling time. When the profiling range was set to 30 m, which was the case for the deployment, the difference between Equation \ref{eq:BattEsttotal} and the observed duration was 30\%. This unexpected result had shown that a shorter profiling range uses more power. To account for this increased power draw, Equation \ref{eq:BattEsttotal} was modified with an adjustment term that accounts for changes in operating range:

\begin{equation}
\label{eq:BattEsttotalR}
t_{total} = \frac{E_{available}}{E_{cycle}} - \frac{(100-R)}{10}
\end{equation}

where $R$ is range in meters. With the additional term, an average of 7\% error was achieved.\\

The standard battery pack for the BioSonics Submersible system is a DeepSea Power \& Light rechargable battery, rated at 960 Wh, but that system didn't provide the total power required for the deployment we had planned. We instead selected an OceanSonics Subsea battery pack that provides 4500 Wh with lithium primary batteries. We used the battery consumption function to select a set of parameters (ping rate at 4 pps, pulse length at 0.1 ms, duty cycle set to 20 mins on and 40 mins off and a range of 30 m) that would provide a 24 $\pm$ 3 days of operating life given the power available (see line 1 of Table \ref{tab:BattTimes}). However, only 8.6 days of data collection was realized. The discrepancy in collections days was partially explained by the strain of high current draw (200 mA) on the lithium-ion primary D-cells which caused them to derate significantly. Discharge curves in the technical data sheet were used to quantify the derating of each battery cell due to the high current draw \cite{SaftLS33600}. Using the corrected available power, 2500 Wh rather than 4500 Wh, the battery consumption function determined the battery pack should have had 14 $\pm$ 1 days of operating life, which was closer but did not account for all the missing days (see line 2 of Table \ref{tab:BattTimes}). \\

While preparing for the second deployment, a short on the circuit board was found to have been causing an additional constant power draw of 100mA, limited only by the current limiting components in the battery pack. However, this short still reduced the effective power capacity of the battery, which accounts for the additional missing days of data collection (see line 3 of Table \ref{tab:BattTimes}). The second deployment was designed with a duty cycle of 10 minutes on per hour rather than 20 minutes on per hour to increase the expected collection time to 26 $\pm$ 3 days (see line 4 of Table \ref{tab:BattTimes}). The second deployment yielded 24 days of data collection, which is within the expected value and demonstrates the utility in the battery consumption function (Equation \ref{eq:BattEsttotalR}).

\begin{muntab}{|c||r|r|r|r|}{BattTimes}{Total collection time calculated by the battery consumption algorithm based on different collection parameters and available power. Nominal is the ideal conditions ratings (3.6V, 17Ah). Derated is the rating estimated +10$\degree$C with 200mA current draw (3.2V, 11Ah). }
\hline
 72 Lithium Batteries &  Voltage & Energy & Collection & Error \\ 
\hline
\hline
  Nominal &	21.6 V & 4406 Wh  & 24 days & $\pm$ 3 days\\
\hline
  Derated & 19.2 V & 2534 Wh & 14 days & $\pm$ 2 days\\
\hline
  Derated + Short in Circuit & 19.2 V & 2534 Wh & 9 days & $\pm$ 1 day\\
\hline
  Derated + New Duty Cycle & 19.2 V & 2534 Wh & 26 days & $\pm$ 3 days\\
\hline
\end{muntab}














