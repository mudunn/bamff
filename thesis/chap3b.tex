\chapter{Computational Methodology}
\label{chap:compmethods}

This chapter on computational methodology provides an overview of BAMFF, the suite of custom MATLAB scripts used to batch process the ADCP data for fish detections. Sonar5-Pro, the software used for post-processing and fish tracking the split-beam echosounder data is described. Also, we outline how fish are identified and isolated with the use of thresholds for both systems.
 

\section{Broadband Acoustic Monitoring For Fish (BAMFF)}

The Broadband Acoustic Monitoring For Fish (BAMFF) package was used to process the ADCP data. It is a MATLAB toolbox that processes raw ADCP data into depth and time-averaged fish and water velocities. The toolbox converts the RDI files into user-modifiable MATLAB structures, then it calibrates and corrects for spherical spreading and absorption loss using the procedure outlined by Deines (1999) \cite{Mullison2017}. Using the combination of correlation and volume backscatter thresholds, it determines whether signals are from fish or water targets in each beam individually. The targets for all the beams are sorted by time and depth to calculate the average fish velocity and count. It also bins the non-fish target in the same way to extract the water velocity profiles.

\subsection{Thresholds}

As with any echosounder system, the presence of fish in the Doppler sonar data can be initially assessed by volume backscatter levels exceeding a specified threshold. In addition, properties unique to the broadband Doppler system allow discrimination of discrete targets as opposed to volume backscatter. The correlation processing inherent to broadband sonar provides an alternative way to detect discrete targets. In contrast to correlation coefficient values of 0.5 or 128 counts, a characteristic expected from a cloud of bubbles, more extensive school of fish or an absence of discrete targets, discrete targets create peaks in the autocorrelation. A perfect single discrete target without noise would return a correlation value of 1 or 256 counts, as recorded by the RDI systems \cite{Tollefsen2003}. The correlation threshold aims to isolate the high peaks in the autocorrelation signal that is indicative of a discrete target, such as a single fish target when it coincides with high volume backscatter signal. \\

The threshold values were determined by starting with low values and slowly increasing them while assessing their accuracy on a characteristic single fish school and a bubble plume. The thresholds were increased until the bubbles plume was not considered to include discrete targets, but the fish in the aggregation were still detected. The balance between the intensity and correlation thresholds was adjusted further so that in general other bubble plumes and fish schools were correctly identified.\\

\munepsfig[width=6in]{thresholdhist}{Histograms of the a) correlation and b) volume backscatter values in beam 1 for the 2018 deployment.}

 Figure \ref{fig:thresholdhist}a shows the correlation has a sharp upper limit where no targets are detected with a threshold above 159 counts (or 0.62). For the second deployment, this limit increases to 190 counts because of the decreased size of the depth bins. The smaller depth bins, 0.5 m for the second deployment compared to 1 m for the first, increase the likelihood of a target to be discrete within its bin because it is averaged over a smaller sample volume. The intensity counts sharply decrease and taper off at volume backscatter values above -65 dB (Figure \ref{fig:thresholdhist}b).\\

\munepsfig[width=7in]{FishschoolADCPsplit}{a) A volume backscatter sonogram for a fish school from the ADCP with the identified fish targets (black dots, $\cdot$) from all four beams b) identifies the remaining data after the backscatter threshold (-45 dB)  is applied, c) the corresponding split-beam signal and d) identifies the remaining data after the backscatter and correlation thresholds (135 counts).}

The volume backscatter and correlation thresholds are applied to each beam individually for fish detection. An example of the effect of the thresholding is shown in Figure \ref{fig:FishschoolADCPsplit}. The top left panel, Figure \ref{fig:FishschoolADCPsplit}a is the sonogram image based on the calibrated volume backscatter, $S_{v}$ from beam 1. Figure \ref{fig:FishschoolADCPsplit}b is the calibrated volume backscatter, $S_{v}$, with a threshold of -45 dB applied to isolate all possible fish target candidates. It results in bubbles, surface and fish targets remaining. The correlation threshold of 153 counts (or 0.60) is applied to finalize the fish detection by removing the low quality, incoherent targets. In addition to the thresholds, the near-field and the surface must be removed from the analysis. \\

The near-field is the area of non-uniform phase structure at the face of the transducer. Near the transducer face, the signal is complex because it has areas of constructive and destructive interference \cite{Medwin1997}. Past a critical range, determined as $ R = \pi \frac{a^{2}}{\lambda} $, the pressure is safely far-field and the phase is uniform across the beam. With a transducer radius, $a$, of 0.04 m and a wavelength, $\lambda$, of  0.0025 m, the near-field range is calculated to be 2.0 m. Only the far-field signal was kept by excluding first 2 m from the analysis. As for the surface, the exclusion line range was selected with a running mean of the maximum volume backscatter. This exclusion line removes the strong surface backscatter and anything beyond the surface from the analysis. The final targets remaining after the volume backscatter threshold, the correlation threshold, and the surface and near-field exclusion lines are accepted as fish target; these are shown in Figure \ref{fig:FishschoolADCPsplit}d and as well, these remaining detected targets are indicated by black dots in Figure \ref{fig:FishschoolADCPsplit}a. Note in particular the clear agreement in backscatter structure when comparing the Doppler sonar data (Fig. \ref{fig:FishschoolADCPsplit}a) with the split-beam data (Fig. \ref{fig:FishschoolADCPsplit}c). Conversely; water targets are identified as signals below either the intensity or correlation thresholds; and these water target data are used to calculate the water velocity.

\subsection{Refactoring}
To make the BAMFF toolbox more accessible to potential users, some tactical refactoring was undertaken to make the code run easily for any data collection parameters and surveying or deployment configurations. Tactical refactoring with regards to programming means to improve and redesign existing code with an end goal that is beyond the requirements of a current task. The goal of the present refactoring was to make fish detection, and velocity extraction as well as water velocity extraction easily accessible for a user with a raw ADCP data file through this suite of functions. The refactoring started with renaming functions from their corresponding name in the RDI software package to descriptions of what is accomplished by running the function. Naming the functions based on their task removed the dependence of having to understand the RDI software package to use this package. For example, the code component that converts RDI raw data into variables in the MATLAB environment is called RDItoMAT.m. \\

Because ADCP processing is often associated with large datasets, the focus was then put towards reducing the amount of coded loops in the software. The loops were replaced with logical indexing when possible to increase speed and the readability of the code. The speed of the code was also improved by reducing the amount of times data files were opened and closed because this file activity takes up the most amount of time in this package. The code documentation was reviewed; comments that helped follow the code when the purpose of certain lines might not be obvious were kept to improve readability. Comments regarding debugging checkpoints and questions were addressed and removed. During the refactoring, a structure containing all of the parameters that can be saved with the raw data was formalized as a function. This parameter structure maintains a summary of the collection and post-processing parameters used for the survey and the analysis. The structure format facilitates switching between datasets with different post-processing requirements and instruments with different calibration coefficients. Aditionally it allows the user to revisit the analysis if the need arises. \\

Visualizations tools are the clear next step required in the package. However, optimal data visualizations vary depending on the purpose of the post-processing.

\subsection{Documentation}
The BAMFF code is published in a repository and is maintained under version control. Documentation for the toolbox is on a Read the Docs webpage, www.bamff.readthedocs.io. The landing page of the documentation webpage is included in Appendix \ref{apdx:bamffrtd}. The webpage includes a description of the package and a Literature Review for background information that might be required by a user. The algorithm documentation section describes all the entries in the parameters structure with some suggested values for starting off. It also contains a brief summary of the main functions required to get from raw RDI data files to fish targets. There is also a Quick Start guide that provides a generic run-through of the package. Lastly, reference are provided for additional information and in case the package should be referenced for another project.\\




\section{Split-beam with Sonar5-Pro}

Sonar5-Pro \cite{Balk2017}, is a sonar post-processing program developed to improve single echo detection methods for fish target detection in sonar data. Balk and Lindem (2002) created a split-beam sonar target tracking algorithm to improve ``traditional'' methods that would frequently miss echoes from fish and create tracks solely from noise. This program, like many others of its kind, requires the user to establish the zones that will not be evaluated for fish detection, such as the near-field and the surface or bottom. Double the near-field range is commonly used for the exclusion zone in front of the transducer, for the Biosonics 120kHz split-beam echosounder the near-field is 0.73 m; the exclusion zone is set to 1.46 m. In our case, the ocean surface also needs to be excluded by a surface exclusion line. This problem is analogous to difficulties near the bottom for conventional hull-mounted (downward looking) system operations. In the present application it is possible to use the bottom detection algorithm to identify the location of the surface. Generally, the bottom detection algorithm requires manual adjustment when fish are near the surface and when the fish and bubbles are mixed. For this purpose, the software provides facility to adjust the exclusion lines. The manual adjustment process is a visual judgment of the sonogram image to delineate the extent of the spreading bubble plume. Manual adjustments can introduce bias by either keeping too much of the bubble plume which results in false targets from bubbles or by eliminating too much which leads to an inaccurate fish count estimate. In addition, it is time-consuming for the user and results in blind spots for the system.\\

The Cross-Filter Detector algorithm, developed by Balk and Lindem (2002), was used to identify the SED and to combine them into fish tracks. There are four components to the Cross-filter Detector for tracking: the detector, the evaluator, the SED and the tracking. The parameters for each component were tuned by carefully following the guidelines described in the Sonar5-Pro manual \cite{Balk2017}. The same fish school used for tuning the ADCP parameters was used to choose the settings for fish detection and tracking.\\

 The first component of the Cross-Filter Detector algorithm, the detector, is composed of two filters: a foreground filter that smooths over the stronger signal with a running mean and a background filter that averages over the ping-to-ping intensity fluctuations without modifying the echoes from fish. The background filter is a key component in the algorithm as it suppresses the noise that causes missing fish targets and false noise targets. The combination of the foreground and background filters creates an adaptive threshold used to isolate the fish targets from the rest of the signal \cite{Balk2002}. The filter values were changed until the background filter removed the background noise and the foreground filter made the targets in the fish school stand out without causing the targets to overlap.\\


When the Cross-Filter Detector foreground and background filters have been completed, the evaluator is used to remove unwanted detections by considering the image and fish tracks as a whole. The fish school was used to make a training set for the evaluator parameters by identifying targets that are wanted. Only one setting was turned on at a time, the auto-detect option provided by Sonar5-Pro was used to provide estimates of appropriate values for each setting. Using the most effective settings from the the auto-detect testing, the criteria that worked best for removing unwanted detections in the present dataset were the track length, mean intensity and standard deviation parameters, as shown in Table \ref{tab:ParametersS5}. \\

 
The SED component of the Cross-Filter Detector algorithm produces a SED echogram without missing detections and without noise detections. The echo separation parameter was used to determine the maximum allowed gap in time between detections, determined in pulse lengths. It was determined to be 3 pulse lengths based on visual inspection of the SED results. \\

In the tracking component of the Cross-Filter Detector algorithm, the SEDs identified in the previous step are combined into fish tracks using a simple multiple target tracker (MTT) algorithm. The simple MTT is based on a proximity threshold, which is determined by the gating parameter, set in meters. The gating parameter defines a conceptual gate that is formed around a track and determines if a single observation is within the gate of the track and not within the gate of any other track \cite{Blackman1986}. The tracks are accepted or rejected depending on length, speed and path which are determined based on the site, species and data quality. The track parameters used are reported in Table \ref{tab:ParametersS5}, they were determined by visual inspection of the detected fish tracks in the sonogram of the characteristic fish school.\\


\begin{muntab}{|c||c|c|r|r|}{ParametersS5}{Parameters for each component of Cross-Filter Detector for tracking algorithm Sonar5-Pro.}
\hline
Component & \multicolumn{2}{c|}{Parameter} & Deployment 2018 & Deployment 2019 \\ 
\hline
\hline
Detector & Foreground Filter & Height & 15 bins & 17 bins\\ 
\cline{3-5}
	&	& Width  & 5 pings & 7 pings\\
\cline{2-5}
	& Background Filter & Method & Mean filter & Mean filter  \\
\cline{3-5}
	&	& Height & 53 bins & 39 bins \\
\cline{3-5}
	&	& Width  & 5 pings & 5 pings \\ 
\cline{3-5}
	&	& Offset  & 10 dB & 10dB \\
\hline
Evatuator & Track Length & Min. & 4 pings & / \\ 
\cline{3-5}
	&	& Max. & 32 pings & / \\
\cline{2-5}
	& Mean Intensity & Min & -50 dB & -70dB \\
\cline{3-5}
	&	 & Max & -28 dB & -28dB \\
\cline{2-5}
	& Std. dev. Alo & Min. & 0.97 & / \\
\cline{3-5}
	&	& Max.  & 5.87 & /  \\ 
\cline{2-5}
	& Std. dev. Ath & Min. & 0.51 & / \\
\cline{3-5}
	&	& Max.  & 6.73 & / \\
\hline
SED	&  \multicolumn{2}{c|}{Max gap between detections} &  3 pings &  3 pings \\
\cline{2-5}
	&  \multicolumn{2}{c|}{Range detection}  &  Center of gravity &  Center of gravity\\
\hline 
Tracking & \multicolumn{2}{c|}{Min. Track Length} & 5 pings & 2 pings\\ 
\cline{2-5}
	& \multicolumn{2}{c|}{Max. Ping Gap} & 2 pings  & 5 pings\\ 
\cline{2-5}
	& \multicolumn{2}{c|}{Gating Range} & 0.05 m & 0.02 m\\ 
\hline

\end{muntab}


The parameters of the Cross-Filter Detector for tracking chosen for the first deployment were expected to be adequate for the second deployment. However, only 110 fish were detected when using these setting over the 24 days of the second deployment. The detector filters were modified to favour the possible fish targets, and many settings had to be turned off in the evaluator to keep as many detections as possible as reported in the deployment 2019 column of Table \ref{tab:ParametersS5}. The need to reduce the parameters to accept almost anything to detect a few fish, suggests that the dataset has too few fish targets to determine settings appropriately. False targets were being accepted in order to detect fish targets within the few small schools that were present. As a result of this low rate of fish occurences, no comparisons between ADCP and split-beam were possible for data from the second deployment.\\
