%**************************************************************************
%This program will analyse the velocity profile of the fish as a function
%of time and depth intervals
%**************************************************************************
 
clear
ikeep = 0;      % used to control clearing of figures 
fontsize = 12;
debug = 0;
dec_correct = -20;    % correction for declination (or any other compass
                    % nonsense) was 17 but appears it should be 0, correction 
                    % is taken care of in processing software.
                    
% if (dec_correct ~= 0)
%     disp(['check dec_correct, in some code I assume it is 0'])
%     % a problem is very likely in the figure 4 plot of fish velocity
%     % on top of water velocity
%     dec_correct = dec_correct / 0;
% end
                    
% ask_date = 'sn16714_';
ask_date = '';
%ask_date = input('MONTHXX: ','s'); %FORMAT: JUN28, JUL01, etc
subsetparams
load ([fileout 'timeseries.mat'])
load ('data/fish_results_2days.mat')

%load (['/zedeldata/jlahey/polagye/venusdata/venus2012DDL/detected_fish/' ask_date '/' ask_date 'timeseries.mat']);

%load (['e:/venus2010/detected_fish/' ask_date '/' ask_date 'timeseries.mat']);

[idim ilen] = size(II1);
depth = depth_instrument - depth_blanking -  ...
        ((binlength) / 2) - binlength * (1:idim) - binlength/4;

[ystart,mstart,dstart,hstart,mistart,sstart] = datevec(Time_series(1)); 
starttimeday = datenum(ystart,mstart,dstart,0,0,0);

run = 1:(length(Time_series)-400);    
I = (II1+II2+II3+II4)./4;
I(imag(I) ~= 0) = 0;

 figure(21)
 clf
 imagesc((Time_series(run)-Time_series(1)),depth,I(:,run))
 xlabel(['Time (days since ' datestr(Time_series(1))])
 ylabel('Depth (m)')
 title('Backscatter (dB)')
 colorbar
 
 figure(22)
 clf
 subplot(211)
 imagesc((Time_series(run)-Time_series(1)),depth,Ve(:,run)/1000,[-1 1])
 ylabel('Depth (m)')
 title('Raw Ve (m/s)')
 colorbar
 set(gca,'XTickLabel',[])
 subplot(212)
 imagesc((Time_series(run)-Time_series(1)),depth,Vn(:,run)/1000,[-1 1])
 xlabel(['Time (days since ' datestr(starttimeday)])
 ylabel('Depth (m)')
 title('Raw Vn (m/s)')
 colorbar
    
    
    

ifilenumber = 6;
% load water parameters ... will park these until needed

fileout3 = ['E:/fortune201617/fortune1/detected_notfish/' ask_date];
%fileout3 = ['e:/venus2010/detected_notfish/' ask_date '/' ask_date];
filename_rootnot = fileout3;
 number = sprintf('%04i',ifilenumber);
 number = sprintf('%01i',ifilenumber);
filename3 = [filename_rootnot number '.mat'];
load(filename3)

%
%

Notfish = fish;  notparameters = parameters;


%
%c:\users\zedel\research\tidal\rock_channel\detected_notfish\
waterprofile = ['E:/fortune201617/fortune1/water_results/water_results.mat'];
%waterprofile = ['e:/venus2010/water_results/water_results_' ask_date '.mat'];
load(waterprofile)
%depth = depth_instrument - depth_blanking -  ((binlength) / 2) - binlength * (1:idim) - binlength/4;

Vel_x_water = Vel_x_water * cosd(dec_correct) + Vel_y_water * sind(dec_correct);
Vel_y_water = -Vel_x_water * sind(dec_correct) + Vel_y_water * cosd(dec_correct);


%depths = [4 98 4];  % 4 170 4 for sog east
%depths = [2 30 2];

mindepth = depths(1);
maxdepth = depths(2);
stepdepth = depths(3);
depth_all = mindepth:stepdepth:maxdepth;



%  these will be loaded later



fileout = ['E:/fortune201617/fortune1/detected_fish/' ask_date];
%fileout = ['e:/venus2010/detected_fish/' ask_date '/' ask_date];
filename_root = fileout;


number = sprintf('%01i',ifilenumber);
filename2 = [filename_root number '.mat'];
load(filename2)


[ystart,mstart,dstart,hstart,mistart,sstart] = datevec(min(fish.timefish)); 
[yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish)); 
starttime_thisfile = datenum(ystart,mstart,dstart,hstart,mistart,sstart);
endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);

% timestep = 10/60/24; %10 minutes
% timestep = 240/60/24; %4.0 hours
% timestep = 43200/60/24;  % 30 days
timestep = 1440/60/24; % 24 hours


starttime_window = datenum(ystart,mstart,dstart,hstart,0,0);  % start on the hour
starttimeday = datenum(ystart,mstart,dstart,0,0,0);
endtime_day = datenum(ystart,mstart,dstart,0,0,0) + 4;
endtime_window = starttime_window + timestep;

endtime_all = Time_series(length(Time_series));



%for ifilenumber = 1:11;
%while exist(filename2,'file') 
while (endtime_window < endtime_all)
fprintf('MATLAB is loading the data \n')


     Vel_x = [];
     Vel_y = [];
     Vel_z = [];
               
     Sig_vx = [];
     Sig_vy = [];
     Sig_vz = [];
                
     Numfish = [];
     rgb=[];
     colormap(jet);
     Jet=jet;
     minval=0;
     maxval=50;  
     

 
  
% define which interval we are averaging over and what is the minimum number
% of fish required to solve the equation
%***********************************************************************
 interval=1;  %the length of the time interval in hours
 thres=10;    %the minimum number of fish necessary to find a meaningful velocity 
%***********************************************************************
 
fprintf('MATLAB is processing the data \n')
itimebin = 1;
idepthbin = 1;
sampletime = [];

 
figure(1)
clf
inview = find(Time_series > starttime_window & Time_series < endtime_window);
[idim ilen] = size(I);
depth = depth_instrument - depth_blanking -  ...
        ((binlength) / 2) - binlength * (1:idim) - binlength/4;
%imagesc((Time_series(inview)-starttimeday)*24,depth,(II1(:,inview)),[-80 -40])
imagesc((Time_series(inview)-Time_series(1))*24,depth,flipud(II1(:,inview)),[-85 -30])  % flipud here has been needed and then not needed depending on deployment!
 axx = axis;
 %axis([axx(1) axx(2) (depth_instrument-surface(Inview_samp)-5) axx(4)]);  % for grand passage needed to track surface 
axis([axx(1) axx(2) -5 axx(4)]); 
xlabel('Time (hours)','FontSize',fontsize)
ylabel('Depth (m)','FontSize',fontsize)
title('Backscatter Beam 1','FontSize',fontsize)  
set(gca,'FontSize',fontsize)
set(colorbar,'FontSize',fontsize)

figure(3)
ts_all = (Time_series(1)) + ((1:length(Vel_x_water))-1)*(Time_series(length(Time_series))-Time_series(1))/(length(Vel_x_water)-1);
%ts_all = results_time;
disp(['deleted line 188'])
inview_all = find(ts_all > starttime_window & ts_all < endtime_window);


clf
subplot(211)
imlabelx = imagenans((ts_all(inview_all)-Time_series(1))*24,depth_all,Vel_x_water(inview_all,:)'/10,[-50 50],1);
set(gca,'FontSize',fontsize)
%set(colorbar,'FontSize',fontsize)

subplot(212)
imlabely = imagenans((ts_all(inview_all)-Time_series(1))*24,depth_all,Vel_y_water(inview_all,:)'/10,[-50 50],1);
set(gca,'FontSize',fontsize)
%set(colorbar,'FontSize',fontsize)

xlabel('Time (hours)','FontSize',fontsize)
ylabel('Depth (m)','FontSize',fontsize)

% figure(4)
% clf
% if (length(inview_all)>1)
%     vxprofile = nanmean(Vel_x_water(inview_all,:));
%     vyprofile = nanmean(Vel_y_water(inview_all,:));
%     vxprofile = despike(vxprofile,1000);
%     vyprofile = despike(vyprofile,1000);
% elseif (length(inview_all) == 1)
%     vxprofile = Vel_x_water(inview_all,:);
%     vyprofile = Vel_y_water(inview_all,:);
% elseif (length(inview_all) == 0)
%     vxprofile = zeros(1,length(depth_all)-1);
%     vyprofile = zeros(1,length(depth_all)-1);
% end
% 
% subplot(121)
% plot(vxprofile/10,-depth_all(1:(length(depth_all)-1)),'LineWidth',2)
% axis([-50 50 -50 0])
% %axis([ax(1) ax(2) ax(3) -(depth_instrument-surface(Inview_samp)-5)]);  % needed in grand passage 
% 
% title('Mean Window Profile','FontSize',fontsize)
% ylabel('Depth (m)','FontSize',fontsize)
% xlabel('East (cm/s)','FontSize',fontsize)
% 
% subplot(122)
% plot(vyprofile/10,-depth_all(1:(length(depth_all)-1)),'LineWidth',2)
% axis([-50 50 -30 0])
% xlabel('North (cm/s)','FontSize',fontsize)

 
figure(2)
clf
subplot(221)
imagesc((Time_series(inview)-Time_series(1))*24,depth,flipud(II1(:,inview)))
title('Beam 1','FontSize',fontsize)
set(gca,'FontSize',fontsize)
subplot(222)
imagesc((Time_series(inview)-Time_series(1))*24,depth,flipud(II2(:,inview)))
title('Beam 2','FontSize',fontsize)
set(gca,'FontSize',fontsize)
subplot(223)
imagesc((Time_series(inview)-Time_series(1))*24,depth,flipud(II3(:,inview)))
title('Beam 3','FontSize',fontsize)
set(gca,'FontSize',fontsize)
subplot(224)
imagesc((Time_series(inview)-Time_series(1))*24,depth,flipud(II4(:,inview)))
title('Beam 4','FontSize',fontsize)
set(gca,'FontSize',fontsize)

 

% for tt=starttime:timestep:(endtime-timestep)
 %     instime = find(fish.timefish >= tt & fish.timefish <= (tt+timestep));
% rect = input('tmin, tmax, dmin, dmax')

figure(1)
disp('Click on upper left and lower right area to analyze')
disp('Click off left side of graph to move to new image')

if (debug == 1)
   x = [7.7774;
      78.1661];

   y = [15.1257;
      57.3772];
else
    [x y] = ginput(2);
end



axref = axis;
while (x(2) > axref(1))

tmin = starttimeday +  x(1)/24;
tmax = starttimeday + x(2)/24;
tmin = Time_series(1) +  x(1)/24;
tmax = Time_series(1) + x(2)/24;
dmin = y(1);
dmax = y(2);

if (x(2) > axref(1))    %  check if user finished with image

hold on
plot(([tmin tmax tmax tmin tmin]-Time_series(1))*24,[dmax dmax dmin dmin dmax],'k','LineWidth',2)
plot(([tmin tmax tmax tmin tmin]-Time_series(1))*24,[dmax dmax dmin dmin dmax],'k','LineWidth',2)
hold off                           

figure(2)
subplot(221)
hold on
plot(([tmin tmax tmax tmin tmin]-Time_series(1))*24*60,[dmax dmax dmin dmin dmax],'k','LineWidth',4)
hold off
subplot(222)
hold on
plot(([tmin tmax tmax tmin tmin]-Time_series(1))*24*60,[dmax dmax dmin dmin dmax],'k','LineWidth',4)
hold off
subplot(223)
hold on
plot(([tmin tmax tmax tmin tmin]-Time_series(1))*24*60,[dmax dmax dmin dmin dmax],'k','LineWidth',4)
hold off
subplot(224)
hold on
plot(([tmin tmax tmax tmin tmin]-Time_series(1))*24*60,[dmax dmax dmin dmin dmax],'k','LineWidth',4)
hold off


if (ikeep == 0)
    figure(4)
    clf
    indep = find( (depth_all > dmin) &  (depth_all < dmax));
    %inview = find(Time_series > starttime_window & Time_series < endtime_window);
    inview_samp = find(ts_all > tmin & ts_all < tmax);
    if isempty(inview_samp)  % if time interval smaller than t/s 
        [idim ival] = min(abs(ts_all - (tmin+tmax)/2));
        inview_samp = ival;
    end

    if (length(inview_samp)>1)
        vxprofile = nanmean(Vel_x_water(inview_samp,:));
        vyprofile = nanmean(Vel_y_water(inview_samp,:));
        vxprofile = despike(vxprofile,1000);
        vyprofile = despike(vyprofile,1000);
    else
        vxprofile = Vel_x_water(inview_samp,:);
        vyprofile = Vel_y_water(inview_samp,:);
    end
    Inview_samp = floor(mean(inview_samp));

    subplot(121)
    hold on
    plot(vxprofile/10,-depth_all(1:(length(depth_all)-1)),'rx','LineWidth',4)
    hold off
    axis([-50 50 -50 5])
    title('Mean Window Profile','FontSize',fontsize)
    ylabel('Depth (m)','FontSize',fontsize)
    xlabel('East (cm/s)','FontSize',fontsize)

    subplot(122)
    hold on
    plot(vyprofile/10,-depth_all(1:(length(depth_all)-1)),'rx','LineWidth',4)
    axis([-50 50 -50 5])
    xlabel('North (cm/s)','FontSize',fontsize)
end



    instime = find(fish.timefish >= tmin & fish.timefish <= (tmax));

else
    instime = [];
end

% start of branch process fish
 
     tim=fish.timefish(instime); %actual values between tmin & tmax
     dep=fish.depthoffish(instime);
     vel=fish.vfish(instime);
     vec=fish.beamvector(:,instime);
      ints = fish.ifish(instime);
      TS = fish.TSfish(instime);
    
     
     insdepth = find(dep>dmin & dep<=dmax); % finds indices
        
     
             figure(1)
             hold on
%             plot((tim(insdepth)-starttimeday)*24,dep(insdepth),'k.','MarkerSize',14)%was starttime
             plot((tim(insdepth)-Time_series(1))*24,dep(insdepth),'k.','MarkerSize',14)%was starttime
             hold off
             axx = axis;
             %axis([axx(1) axx(2) (depth_instrument-surface(Inview_samp)-5) axx(4)]);  % for grand passage needed to track surface 
             axis([axx(1) axx(2) -5 axx(4)]); 
                                       
             if(isempty(insdepth) || length(insdepth)<=thres)

                Vel_x(itimebin,idepthbin) = NaN;
                Vel_y(itimebin,idepthbin) = NaN;
                Vel_z(itimebin,idepthbin) = NaN;
                
                Sig_vx(itimebin,idepthbin) = NaN;
                Sig_vy(itimebin,idepthbin) = NaN;
                Sig_vz(itimebin,idepthbin) = NaN;
                
                Numfish(itimebin,idepthbin) = length(insdepth);
                TS_mean = mean(TS(insdepth));
                TS_min = min(TS(insdepth));
                TS_max = max(TS(insdepth));
             
             else
              rotmat = [cosd(dec_correct)   sind(dec_correct) 0;
                 -sind(dec_correct)  cosd(dec_correct) 0;
                       0                0              1];
%                      
       vecr = rotmat * vec;
                 
       kx = vecr(1,insdepth);
       ky = vecr(2,insdepth);
       kz = vecr(3,insdepth);

               
               [v_x,v_y,v_z,sigma_vx,sigma_vy,sigma_vz,nfish] = resolve3d_sub(kx,ky,kz,-vel(insdepth));
               %[not_vx,notv_y,notv_z,notsigma_vx,notsigma_vy,notsigma_vz,notnfish] = resolve3d_sub(kx,ky,kz,-vel(notinsdepth));
% -ve on vel because adcp velocities are +ve toward transducer and therefore -ve in the beam direction
                Vel_x = v_x;
                Vel_y = v_y;
                Vel_z = v_z;
                
                Sig_vx = sigma_vx;
                Sig_vy = sigma_vy;
                Sig_vz = sigma_vz;
                
                Numfish = nfish;
                TS_mean = mean(TS(insdepth));
                TS_min = min(TS(insdepth));
                TS_max = max(TS(insdepth));
             end
             
    disp('Velx  Vely  Velz Stdx Stdy Stdz')
    disp([Vel_x,Vel_y,Vel_z,Sig_vx,Sig_vy,Sig_vz])
    disp(['Number of fish: ',num2str(Numfish)])
    disp(['Target Strength (min, mean, max): ',num2str(TS_min),', ',num2str(TS_mean),', ',num2str(TS_max)])

    figure(4)
    subplot(121)
    hold on
    plot([Vel_x Vel_x]/10,[-dmin -dmax],'g','LineWidth',4);
    set(gca,'Box','on')
    ax = axis;
%    plot([ax(1) ax(2)],[-(depth_instrument-surface(Inview_samp)-binlength) -(depth_instrument-surface(Inview_samp)-binlength)],'b','LineWidth',3)
    plot([ax(1) ax(2)],[0 0],'b','LineWidth',3)
    %axis([ax(1) ax(2) ax(3) -(depth_instrument-surface(Inview_samp)-5)]);  % needed in grand passage 
    axis([ax(1) ax(2) ax(3) 5]); 

    subplot(122)
    hold on
    plot([Vel_y Vel_y]/10,[-dmin -dmax],'g','LineWidth',4);
    ax = axis;
%    plot([ax(1) ax(2)],[-(depth_instrument-surface(Inview_samp)-binlength) -(depth_instrument-surface(Inview_samp)-binlength)],'b','LineWidth',3)
    plot([ax(1) ax(2)],[0 0],'b','LineWidth',3)

    %axis([ax(1) ax(2) ax(3) -(depth_instrument-surface(Inview_samp)-5)]);  % needed in grand passage 
    axis([ax(1) ax(2) ax(3) 5]); 

    set(gca,'Box','on')
    
    
    
    
% x-direction magnetic-to-true-north conversion******************
% icaret [E(m)cosXX + N(m)sinXX] (degrees)      E(m) = Vel_x
% jcaret [-E(m)coxXX + N(m)sinXX] (degrees)     & N(m) = Vel_y
% http://geomag.nrcan.gc.ca/apps/mdcal-eng.php?Year=2009&Month=9&Day=24&Lat
% =49&Min=15&LatSign=1&Long=123&Min2=7&LongSign=-1&Submit=Calculate+magnetic+declination&CityIndex=83
% ***************************************************************

% Vel_true_east = ((Vel_x)*cosd(dec_correct) + (Vel_y)*sind(dec_correct));
% Vel_true_north = (-(Vel_x)*sind(dec_correct) + (Vel_y)*cosd(dec_correct));

    disp('E Vel'); disp(round(Vel_x)/10)
    disp('N Vel'); disp(round(Vel_y)/10)

% start of notfish

Fish = fish;

fish=Notfish;

% fileout3 = ['/research/FishAcoustics/venus2010/detected_notfish/' ask_date '/' ask_date];
% filename_rootnot = fileout3;
% filename3 = [filename_rootnot num2str(ifilenumber) '.mat'];
% load(filename3)
     

instime = find(fish.timefish >= tmin & fish.timefish <= (tmax));
    tim=fish.timefish(instime); %actual values between tmin & tmax
    dep=fish.depthoffish(instime);
    vel=fish.vfish(instime);
    vec=fish.beamvector(:,instime);
    ints = fish.ifish(instime);
    

    insdepth = find(dep>dmin & dep<=dmax); % finds indices
%     figure(1)
%     hold on
%     plot((tim(insdepth)-starttimeday)*24,dep(insdepth),'g.')%was starttime
%     hold off    
       
    if(isempty(insdepth) || length(insdepth)<=thres)

       Vel_x(itimebin,idepthbin) = NaN;
       Vel_y(itimebin,idepthbin) = NaN;
       Vel_z(itimebin,idepthbin) = NaN;
                
       Sig_vx(itimebin,idepthbin) = NaN;
       Sig_vy(itimebin,idepthbin) = NaN;
       Sig_vz(itimebin,idepthbin) = NaN;
                
       Numfish(itimebin,idepthbin) = length(insdepth);
             
    else

              rotmat = [cosd(dec_correct)   sind(dec_correct) 0;
                 -sind(dec_correct)  cosd(dec_correct) 0;
                       0                0              1];
%                      
       vecr = rotmat * vec;
                 
       kx = vecr(1,insdepth);
       ky = vecr(2,insdepth);
       kz = vecr(3,insdepth);
        
       [v_x,v_y,v_z,sigma_vx,sigma_vy,sigma_vz,nfish] = ... 
           resolve3d_sub(kx,ky,kz,-vel(insdepth));

% [not_vx,notv_y,notv_z,notsigma_vx,notsigma_vy,notsigma_vz,notnfish]
% = resolve3d_sub(kx,ky,kz,-vel(notinsdepth));
% -ve on vel because adcp velocities are +ve toward transducer and 
% therefore -ve in the beam direction


       Vel_x = v_x;
       Vel_y = v_y;
       Vel_z = v_z;
                
       Sig_vx = sigma_vx;
       Sig_vy = sigma_vy;
       Sig_vz = sigma_vz;
       
       Numfish = nfish;
%        Vel_true_east = ((Vel_x)*cosd(dec_correct) + (Vel_y)*sind(dec_correct));
%        Vel_true_north = (-(Vel_x)*sind(dec_correct) + (Vel_y)*cosd(dec_correct));
       disp('E Water Vel'); disp(round(Vel_x)/10)
       disp('N Water Vel'); disp((round(Vel_y)/10))
%        figure(4)
%         subplot(121)
%         hold on
%         plot([Vel_x Vel_x]/10,[-dmin -dmax],'k','LineWidth',4);
%         subplot(122)
%         hold on
%         plot([Vel_y Vel_y]/10,[-dmin -dmax],'k','LineWidth',4);
       pause 
    end
       
 
 
% end of notfish

     
 
% fishdata (again)

%setparamsDDL  % controls which file (day) to work from
subsetparams 


% fileout4 = ['/research/FishAcoustics/venus2010/detected_fish/' ask_date '/' ask_date];
% filename_root = fileout4;
% filename4 = [filename_root num2str(ifilenumber) '.mat'];
% load(filename4)
%     

fish = Fish;

figure(1)

ikeep = 0;

if (ikeep == 0)
    inview = find(Time_series > starttime_window & Time_series < endtime_window);
    [idim ilen] = size(II1);
    depth = depth_instrument - depth_blanking -  ((binlength) / 2) - binlength * (1:idim) - binlength/4;
%    imagesc((Time_series(inview)-starttimeday)*24,depth,(II1(:,inview)),[-80 -40])
    imagesc(24*(Time_series(inview)-Time_series(1)),depth,flipud(I(:,inview)),[-80 -40])
    xlabel('Time (hours)','FontSize',fontsize)
    ylabel('Depth (m)','FontSize',fontsize)
    title('Backscatter Beam 1','FontSize',fontsize)  
    set(colorbar,'FontSize',fontsize)
    set(gca,'FontSize',fontsize)
end

if (debug == 1)
   x = [7.7774;
      78.1661];

   y = [15.1257;
      57.3772];
else
    [x y] = ginput(2);
end


end

starttime_window = starttime_window + timestep;
endtime_window = starttime_window + timestep;
if (endtime_window > endtime_thisfile) 
    endtime_window = endtime_thisfile; 
end

if (endtime_window > endtime_thisfile)
    endtime_window = endtime_thisfile;
end

if (starttime_window > endtime_thisfile)
    ifilenumber = ifilenumber + 1;
    
    number = sprintf('%01i',ifilenumber);
    filename2 = [filename_root number '.mat'];

    
    
  %  filename2 = [filename_root num2str(ifilenumber) '.mat'];
    if exist(filename2,'file')
        
        % load not fish data first
 
        filename3 = [filename_rootnot number '.mat'];
%        filename3 = [filename_rootnot num2str(ifilenumber) '.mat'];
        clear fish Notfish 
        load(filename3)
        Notfish = fish;  notparameters = parameters;
        % and now data is parked in notfish and notparameters

        starttime_window = endtime_thisfile;   % end of old file becomes
                                               % start of new window
        endtime_window = starttime_window + timestep;
        
        load(filename2)
        [ystart,mstart,dstart,hstart,mistart,sstart] = datevec(min(fish.timefish)); 
        [yend,mend,dend,hend,miend,send] = datevec(max(fish.timefish)); 
        starttime_thisfile = datenum(ystart,mstart,dstart,hstart,mistart,sstart);
        endtime_thisfile = datenum(yend,mend,dend,hend,miend,send);
        
    else
        aaa = 1;  % RELAX ... I think the fact that the file is not renewed
        % will be taken care of with the loop getting to the end of the
        % day
    end
end
    
   

end    % endtime_window


axref = axis;
    
    if (debug == 1)
       x = [7.7774;
            78.1661];

       y = [15.1257;
            57.3772];
        
       x(2) = axref(1) - .1;  
       clear
    else
        [x,y] = ginput(2);
    end
 



             stdthresh = 100;
             
             close(figure(1))
             close(figure(2))
             
