%% Battery Types Options
types=10;
name{1} = 'Lead Acid battery';
voltage(1) = 12;
power(1) = voltage(1)*80; % V*Ah=Wh
cost(1) = 0;

name{2} = 'Lead Acid Batteryx2';
voltage(2) = 12;
power(2) = voltage(2)*160; % W
cost(2) = 6.5;

name{3}='OceanSonics Lithium (Nominal)';
voltage(3) = 3.6*6;
power(3) = voltage(3)*17*18; % W
cost(3) = NaN;

name{4} = 'OceanSonics Lithium (derated LS33600)';
voltage(4) = 3.2*6;
power(4) = 3.2*11*72; % W
cost(4) = 2.3;

name{5} = 'OceanSonics Lithiumx2 (derated LS33600)';
voltage(5) = 3.3*6;
power(5) = 3.3*14*72*2; % W
cost(5) = 10;

name{6} = 'OceanSonics Lithium(derated LS33600)';
voltage(6) = 3.2*6;
power(6) = 3.2*7*72; % W
cost(6) = 2.3;

name{7} = 'Oceansonics Lithium (derated LSH 20)';
voltage(7) = 3.5*6;
power(7) = voltage(5)*11*18;
cost(7) = 2.3;

name{8} = 'Oceansonics Lithiumx2 (derated LSH 20)';
voltage(8) = 3.5*6;
power(8) = voltage(8)*11.5*18*2;
cost(8) = 10;

name{9} = 'OceanSonics Alkaline';
voltage(9) = 1.5*18;
power(9) = 1.5*12*72;
cost(9) = 0.2;

name{10} = 'Oceana (3xLi LS33600)';
voltage(10) = 3.6*6;
power(10) = 3.6*16*72*3;
cost(10) = 0;
%Lead


%% Settings 
ping_rate = 4; %pps
ping_dur = 100; %us
ping_time = 20; %mins
off_time = 40; %mins
range = 25; %m

% % Oceana settings
% ping_rate = 0.5; %pps
% ping_dur = 100; %us
% ping_time = 12; %mins
% off_time = 108; %mins
% range = 200; %m

for k = 1:types
    collection_hours(k) = powerusage(ping_rate, ping_dur, ping_time, off_time, range,voltage(k),power(k));
end

power
collection_days = collection_hours/24

% Error est.

SplitBeamPower;
error_est = (collection_days* error(7)./100)


%% Put into a table
Results20 = table(voltage', power', cost',collection_days', error_est','VariableNames', {'Voltage','Power','Costk','CollectionDays', 'ErrorDays'}, 'RowNames', name)

Results20 = table(voltage', power', cost',collection_days', error_est','VariableNames', {'Voltage','Power','Costk','CollectionDays', 'ErrorDays'}, 'RowNames', name)