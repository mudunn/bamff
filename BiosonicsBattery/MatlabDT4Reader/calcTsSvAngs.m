function [ts,sv,wts,wsv,xang,yang]=calcTsSvAngs(dtx)
% [ts,sv,wts,wsv,xang,yang]=calcTsSvAngs(dtx): calculates volume
% backscattering strength (sv) and target strength
% (ts) in dB using equations found in Biosonics' DT4
% Data file format specification document 
% (sec. 6.3.1, document dated Dec. 2004).  Also
% computes split-beam angles in degrees.
%
% Input: dtx is the data structure output by the rddtx function.
%
% Example:
%   dtx=rddtx('foo.dt4',1,1);
%   [ts,sv,wts,wsv,xang,yang]=calcTsSvAngs(dtx);
%
% For single-beam data, only ts and sv are computed.
% For dual-beam data, only ts, sv, wts, and wsv are computed.
% For split-beam data, only ts, sv, xang, and yang are computed.
%
% $Revision: 4844 $ $Date: 2010-12-15 08:31:02 -0800 (Wed, 15 Dec 2010) $
% A.Stevens @ USGS 01/25/2007
% astevens@usgs.gov


%calculate absorption coefficient assuming depth=0
%and pH=8. Temp and sal from values in DT4 file.
absC=calcAlpha(dtx.env.salinity,dtx.env.temperature,...
    dtx.snd.rxee.frequency/1000,0,8)/1000;

%calculate 2-way beam angle
psi=(dtx.snd.rxee.bwx/20)*(dtx.snd.rxee.bwy/20)*10.^(-3.16);
lctp=(10*log10(dtx.env.sv*(dtx.snd.pulselen/1000)*(psi/2)));
if (strcmp(dtx.opmode,'Dual')),
  wpsi=(dtx.snd.rxee.bwwx/20)*(dtx.snd.rxee.bwwy/20)*10.^(-3.16);
  wlctp=(10*log10(dtx.env.sv*(dtx.snd.pulselen/1000)*(wpsi/2)));
end

% value to use for below threshold TS and Sv data.
negInf = -1000;

% set this to 1 to have data below threshold set to the threshold, rather than negInf.
% can be useful for comparing results against results from other programs, because
% roundoff differences will not cause large differences in the results.
limitToThreshold = 0;

[m,n]=size(dtx.vals);
ts=zeros(m,n);
sv=zeros(m,n);
if (strcmp(dtx.opmode,'Single')),
  wts=[];
  wsv=[];
  xang=[];
  yang=[];
elseif (strcmp(dtx.opmode,'Dual')),
  wts=zeros(m,n);
  wsv=zeros(m,n);
  xang=[];
  yang=[];
elseif (strcmp(dtx.opmode,'Split')),
  wts=[];
  wsv=[];
  xang=zeros(m,n);
  yang=zeros(m,n);
end

for i=1:m
    k = find(dtx.vals(i,1:n) == 0);

    %equation 4a in Biosonics document
    ts(i,1:n)=(20.*log10(dtx.vals(i,1:n)))- ...
        (dtx.snd.rxee.sl+dtx.snd.rxee.rs+(dtx.env.power*10))./10+...
        (40.*log10(dtx.range(i)))+...
        (2*absC*dtx.range(i))+...
        (dtx.snd.ccor./100);
    kts = find(ts(i,1:n) < dtx.snd.threshold);
    if (limitToThreshold),
      tsNegInf = dtx.snd.threshold;
    else
      tsNegInf = negInf;
    end
    ts(i,kts) = tsNegInf;
    ts(i,k) = tsNegInf;
    
    %equation 4b in Biosonics document 
    sv(i,1:n)=(20.*log10(dtx.vals(i,1:n)))- ... 
        (dtx.snd.rxee.sl+dtx.snd.rxee.rs+(dtx.env.power*10))./10+...
        (20.*log10(dtx.range(i)))+...
        (2*absC*dtx.range(i))-...
        lctp+...
        (dtx.snd.ccor./100);
    ksv = find(sv(i,1:n) < dtx.snd.threshold-lctp);
    if (limitToThreshold),
      svNegInf = dtx.snd.threshold-lctp;
    else
      svNegInf = negInf;
    end
    sv(i,ksv) = svNegInf;
    sv(i,k) = svNegInf;

    if (strcmp(dtx.opmode,'Dual')),
      kw = find(dtx.wvals(i,1:n) == 0);
      
      %equation 4a in Biosonics document
      wts(i,1:n)=(20.*log10(dtx.wvals(i,1:n)))- ...
          (dtx.snd.rxee.sl+dtx.snd.rxee.rsw+(dtx.env.power*10))./10+...
          (40.*log10(dtx.range(i)))+...
          (2*absC*dtx.range(i))+...
          (dtx.snd.ccor./100);
      kwts = find(wts(i,1:n) < dtx.snd.threshold);
      if (limitToThreshold),
        wtsNegInf = dtx.snd.threshold;
      else
        wtsNegInf = negInf;
      end
      wts(i,kw) = wtsNegInf;
      wts(i,kwts) = wtsNegInf;
      
      %equation 4b in Biosonics document 
      wsv(i,1:n)=(20.*log10(dtx.wvals(i,1:n)))- ... 
          (dtx.snd.rxee.sl+dtx.snd.rxee.rsw+(dtx.env.power*10))./10+...
          (20.*log10(dtx.range(i)))+...
          (2*absC*dtx.range(i))-...
          wlctp+...
          (dtx.snd.ccor./100);
      kwsv = find(wsv(i,1:n) < dtx.snd.threshold-wlctp);
      if (limitToThreshold),
        wsvNegInf = dtx.snd.threshold-wlctp;
      else
        wsvNegInf = negInf;
      end
      wsv(i,kwsv) = wsvNegInf;
      wsv(i,kw) = wsvNegInf;
    elseif (strcmp(dtx.opmode,'Split')),
      kx = find(isnan(dtx.xangs(i,1:n)));

      phx = dtx.snd.rxee.phx/10.0;
      aox = dtx.snd.rxee.aox/100.0;
      pdx = dtx.snd.rxee.pdx/100000.0;

      xang(i,1:n)=(dtx.xangs(i,1:n)*phx)/127.0;
      if (dtx.snd.rxee.phpx == -1),
        xang(i,1:n)=-xang(i,1:n);
      end
      xang(i,1:n)=xang(i,1:n)+aox;
      k1 = find(xang(i,1:n) > phx);
      k2 = find(xang(i,1:n) < -phx);
      xang(i,k1) = xang(i,k1)-2*phx;
      xang(i,k2) = xang(i,k2)+2*phx;
      xang(i,1:n) = xang(i,1:n)*pi/180;
      if (dtx.snd.rxee.pdpx ~= 0),
        pdx = -pdx;
      end
      tmp = pdx*cos(xang(i,1:n))/dtx.range(i);
      k1 = find(tmp > 1);
      k2 = find(tmp < -1);
      tmp(k1) = 1;
      tmp(k2) = -1;
      xang(i,1:n) = xang(i,1:n)-asin(tmp);
      xang(i,1:n) = xang(i,1:n)*180/pi;
      xang(i,kx) = 0;
      
      ky = find(isnan(dtx.yangs(i,1:n)));

      phy = dtx.snd.rxee.phy/10.0;
      aoy = dtx.snd.rxee.aoy/100.0;
      pdy = dtx.snd.rxee.pdy/100000.0;

      yang(i,1:n)=(dtx.yangs(i,1:n)*phy)/127.0;
      if (dtx.snd.rxee.phpy == -1),
        yang(i,1:n)=-yang(i,1:n);
      end
      yang(i,1:n)=yang(i,1:n)+aoy;
      k1 = find(yang(i,1:n) > phy);
      k2 = find(yang(i,1:n) < -phy);
      yang(i,k1) = yang(i,k1)-2*phy;
      yang(i,k2) = yang(i,k2)+2*phy;
      yang(i,1:n) = yang(i,1:n)*pi/180;
      if (dtx.snd.rxee.pdpy ~= 0),
        pdy = -pdy;
      end
      tmp = pdy*cos(yang(i,1:n))/dtx.range(i);
      k1 = find(tmp > 1);
      k2 = find(tmp < -1);
      tmp(k1) = 1;
      tmp(k2) = -1;
      yang(i,1:n) = yang(i,1:n)-asin(tmp);
      yang(i,1:n) = yang(i,1:n)*180/pi;
      yang(i,ky) = 0;
    end
end
