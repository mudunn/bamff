This folder contains Matlab code to read BioSonics DT4 data files.

Use the rddtx function to read a DT4 file and convert it into an object containing the raw data from the file.

Use the calcTsSvAngs function to convert the raw data into TS, Sv, or mechanical angle data.
