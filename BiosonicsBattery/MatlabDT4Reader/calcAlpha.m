function alpha=calcAlpha(sal,temp,freq,depth,pH)
% alpha=calcAlpha(sal,temp,freq,depth,pH): calculates
% sound absorption coefficient in dB/km based on the 
% equation of Francois and Garrison,
% 1982, J. Acoust. Soc. Am 17 (6) pp. 1879-????.
%
% Inputs:  sal:   salinity, psu
%         temp:   temperature, degC
%         freq:   transducer frequency, kHz
%        depth:   depth, m
%           pH:   pH, don't know units 
%
% $Revision: 3556 $ $Date: 2009-09-10 13:54:08 -0700 (Thu, 10 Sep 2009) $
% A.Stevens @ USGS 01/25/2007
% astevens@usgs.gov

phi=temp+273;
svel=sw_svel(sal,temp,depth); %sound velocity, uses subfunction from 
                              %seawater tookit

%boric acid contribution
a1=(8.86/svel)*10^((0.78*pH)-5);
p1=1;
f1=(2.8.*(sal/35)).^0.5*10.^(4-(1245/phi));

%magnesium sulfate contribution
a2=21.44*(sal/svel)*(1+(0.025*temp));
p2=(1-(1.37e-4*depth))+(6.2e-9.*depth.^2);
f2=(8.17.*10.^(8-(1990/phi)))./(1+(0.0018*(sal-35)));

%pure water contribution
if temp<20
    a3=4.937e-4-(2.59e-5.*temp)+(9.11e-7.*temp.^2)-...
        (1.50e-8.*temp.^3);
else
    a3=3.964e-4-(1.146e-5.*temp)+(1.45e-7.*temp.^2)-...
        (6.5e-10.*temp.^3);
end

p3=(1-(3.83e-5.*depth))+(4.9e-10.*depth.^2);

alpha=(((a1*p1*f1*freq.^2)./(freq.^2+f1.^2)) + ...
    ((a2*p2*f2*freq.^2)/(freq.^2+f2.^2)) + (a3*p3*freq.^2));
