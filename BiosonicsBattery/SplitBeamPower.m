%% Determining battery usage of BioSonic DT-X SUB
% Using three trials with different parameters determine the battery usage
% function of the instrument. Total 80A = 960W at 12V

%% Information
% Note: overlap between trial 2 end and trial 3, can they overlap?

% Book Ex
ping_rate_ex = 20; %pps
ping_dur_ex = 1000; %us
ping_time_ex = 60; %min
range_ex = 100; %m
trial_ex_start = datenum(2018,02,08,15,40,00);
trial_ex_end = datenum(2018,02,18,14,43,00);
power_available = 960; %W
trial_collect_ex = power_available/27;


% Settings for tests.
ping_rate =  [4,   4,   4,   4,   20,   20,  4,    4,   4]; %pps
ping_dur = [400, 100, 100, 100, 1000, 1000, 100, 100, 100] ; %us
ping_time = [10,  10,  20,  10,   58,   58,  20,  20,  20]; %min
off_time =  [50,  50,  50,  50,    2,    2,  50,  40,  40]; %min
range =    [100, 100, 100,  20,   20,   20,  20,  20, 100]; %m

% First Trial
trial_1_start = datenum(2018,02,08,15,40,55);
trial_1_end = datenum(2018,02,18,14,43,15);
trial_collect(1) = (trial_1_end - trial_1_start)*24;


% Second trial
trial_2_start = datenum(2018,02,28,09,33,33);
trial_2_end = datenum(2018,03,11,08,36,26);
trial_collect(2) = (trial_2_end - trial_2_start)*24;


% Third trial
trial_3_start = datenum(2018,03,20,09,41,00);
trial_3_end = datenum(2018,03,27,06,12,42);
trial_collect(3) = (trial_3_end - trial_3_start)*24;

% Fourth trial
trial_4_start = datenum(2018,04,24,19,15,25);
trial_4_end = datenum(2018,05,05,09,17,47);
trial_collect(4) = (trial_4_end - trial_4_start)*24;

% Fifth trial
trial_5_start = datenum(2018,05,23,13,23,41);
trial_5_end = datenum(2018,05,24,08,34,50);
trial_collect(5) = (trial_5_end - trial_5_start)*24;

% Sixth trial
trial_6_start = datenum(2018,05,29,12,39,24);
trial_6_end = datenum(2018,05,30,07,39,41);
trial_collect(6) = (trial_6_end - trial_6_start)*24;

% Seventh trial
trial_7_start = datenum(2018,06,04,14,52,32);
trial_7_end = datenum(2018,06,10,15,34,10);
trial_collect(7) = (trial_7_end - trial_7_start)*24;

% Eigth trial
trial_8_start = datenum(2018,06,29,16,22,44);
trial_8_end = datenum(2018,07,05,02,24,40);
trial_collect(8) = (trial_8_end - trial_8_start)*24;

% Nine trial
trial_9_start = datenum(2018,07,06,16,14,06);
trial_9_end = datenum(2018,07,11,12,15,47);
trial_collect(9) = (trial_9_end - trial_9_start)*24;



Ntrials = length(trial_collect);

%% Variables
% Three types of power usage: Standby, transmit and listening
% Four parameters: ping duration (us), ping time (min), off time (min), ping rate (pps)
% and depth range.

% From literarure: pinging more often (ping_rate, pps) , longer transmit
% pulse length (pind_dur, us) increase power draw.
% Worst case scenario: 20 pps, 1ms, system is transmit 2% of the time.

% Fine tune new function.
V=12;
for test = 1:Ntrials
    collecttime(test) = powerusage(ping_rate(test), ping_dur(test), ping_time(test), off_time(test), range(test), V, 960);
end


%% Biosonics estimator for collection time
% Biji's spreadsheet equations
biosonics_W = 24*(30*ping_time + 0.3*off_time)./(ping_time+off_time);
biosonics_collecttime = 24*960./biosonics_W;



%% Error
% The error in the estimated sample time.
error = (abs(collecttime-trial_collect)./trial_collect)*100;



%% Plot Results
figure(1)
plot(1:Ntrials, collecttime, 'kx', 1:Ntrials, trial_collect,'-', 1:Ntrials, biosonics_collecttime,'ro')
legend('PowerUsage Estimate (12V)','Experimental Results', 'BioSonics Estimate', 'location', 'southwest');
ylabel('Collection time (hours)')
xlabel('Trial #')
title('Power Consumtion Estimates')


% 
% figure(2)
% subplot(2,1,1)
% plot(ping_dur(1:2), collecttime(1:2), 'o',  ping_dur(1:2), trial_collect(1:2))
% title('Changed variable: Ping Duration')
% legend('PowerUsage Results', 'Experimental Results', 'location', 'SouthWest');
% ylabel('Collection time (hours)')
% xlabel('Ping Duration (us)')
% 
% 
% subplot(2,1,2)
% plot(ping_time(2:3), collecttime(2:3), 'o',  ping_time(2:3), trial_collect(2:3))
% title('Changed variable: Ping Time')
% legend('PowerUsage Results', 'Experimental Results');
% ylabel('Collection time (hours)')
% xlabel('Ping Time (min)')
% xlim([9,21])
