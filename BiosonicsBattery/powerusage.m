function collectiontime = powerusage(rate, duration, on_time, off_time, range, V,power_available)
% This function is used to extimate how much data collection time will the
% BioSonics DT-X allow for given the parameters input
%
% Input:
%   Rate: Ping rate, in pings per seconds.
%   Duration: Length of the ping, in microseconds.
%   Time: Amount of time the system spends pinging, in minutes.
%   Range: The range of sampling, in meters.
%   V : Voltage of the power source
%   power_available: The total amount of power available.
% 
% Output:
%   collectiontime: Amount of time in hours of data collection.
%
%
% May 17th 2018


%% Method
% Find out how much Amp/hours each type of power usage uses and the
% percentage of time it spends in each type.

% Three types of battery usage modes.
% Values below from Quick start guide, will have to be adjusted.
powerdraw_sleep = 0.5; %W

powerdraw_transmit = 19.6 + rate/2; %W


%% Duty Cycle Times.
cycle_length = on_time + off_time;
on_time_percent = on_time/cycle_length;

% The amount of time transmitting is the length of the ping* the amount of
% pings per second * the amount of a cycle that is spend in "cycle on"
% mode.
transmit_percent = on_time/cycle_length ;%rate *  on_time_percent *((duration*1e-6)+0.5);
% The amount of time receiving is the rest of the "cycle on" time

sleep_percent = off_time/cycle_length ;






%% Calaculate available data collection time
power_per_cycle = (powerdraw_sleep* sleep_percent) + (powerdraw_transmit*transmit_percent) ;

collectiontime = power_available/power_per_cycle - (100-range)/10;


end

